echo "Setup Started..."
. "./.environment.sh"
if [ "$FrontendPort" != "" ];
then
    docker stop incareview-frontend
    docker rm -f incareview-frontend
    sudo cp .environment.sh .env
    #docker-compose build
    sudo docker-compose run -d -p $FrontendPort:$FrontendPort --name incareview-frontend web
    echo "Setup Finished...."   
    docker ps
    #sudo docker run -it -p ${FrontendPort}:${FrontendPort} --name incareview-frontend

    # sudo yum install net-tools
    # resp=`netstat -tunl | grep ":$FrontendPort "` 
    # if [ -z "$resp" ]; then
        # docker-compose build
    #     echo "Setup Finished...."   
    #     docker ps
    # else
    #     echo "$FrontendPort Port Already in use. Please use another port..."
    # fi
else
    echo "One or More environment field not found. Please Update your environment file data"
    echo "Setup Aborted...."
fi

