# don't send the nginx version number in error pages and Server header
server_tokens off;

# config to don't allow the browser to render the page inside an frame or iframe
# and avoid clickjacking http://en.wikipedia.org/wiki/Clickjacking
# if you need to allow [i]frames, you can use SAMEORIGIN or even set an uri with ALLOW-FROM uri
# https://developer.mozilla.org/en-US/docs/HTTP/X-Frame-Options
add_header X-Frame-Options SAMEORIGIN;

# when serving user-supplied content, include a X-Content-Type-Options: nosniff header along with the Content-Type: header,
# to disable content-type sniffing on some browsers.
# https://www.owasp.org/index.php/List_of_useful_HTTP_headers
# currently suppoorted in IE > 8 http://blogs.msdn.com/b/ie/archive/2008/09/02/ie8-security-part-vi-beta-2-update.aspx
# http://msdn.microsoft.com/en-us/library/ie/gg622941(v=vs.85).aspx
# 'soon' on Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=471020
add_header X-Content-Type-Options nosniff;

# This header enables the Cross-site scripting (XSS) filter built into most recent web browsers.
# It's usually enabled by default anyway, so the role of this header is to re-enable the filter for
# this particular website if it was disabled by the user.
# https://www.owasp.org/index.php/List_of_useful_HTTP_headers
add_header X-XSS-Protection "1; mode=block";

upstream dynamic {
    server incareview.com:9007;
    server incareview.com:9008;
}

server {
    listen       9003;
    # listen 443;
    # server_name  localhost;

    # charset koi8-r;
    # access_log  /var/log/nginx/log/host.access.log  main;
    # ssl on;
    # ssl_certificate /etc/nginx/ssl/incareview_public.crt;
    # ssl_certificate_key /etc/nginx/ssl/incareview_com.key;
    # ssl_protocols TLSv1 TLSv1.1 TLSv1.2; 
    # ssl_prefer_server_ciphers on;
    # ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

   
    index  index.html index.htm;

    location / {
        root /home/incareview-frontend/incare/files;
    }

    location /privacy {
        root /home/incareview-frontend/incare/files;
    }

    location /app_download {
        root /home/incareview-frontend/incare/files;
    }

    location /app {
        alias  /home/incareview-frontend/incare/files/app;
        try_files $uri $uri/ /app/index.html;
    }

    location /socket.io {
        proxy_pass https://incareview.com:9006;
    }
    
    location /flashcard/ {
        try_files   $uri $uri/ index.html;
        root /home/incareview-frontend/incare/;
    }

    location /api/v1/gateway {
        proxy_pass https://dynamic;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # Prevent access to hidden files
    location ~ /\. {
        deny all;
    }
}
