FROM node:10.19.0 as builder

# create app directory in container
RUN mkdir /app

# set /app directory as default working directory
WORKDIR /app

# Temporarily commented
# ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install

RUN npm install -g @angular/cli@8.1.0

#add app
COPY . /app

RUN ng build --prod

FROM nginx:1.11.12-alpine

RUN mkdir -p /etc/nginx/ssl
ADD nginx/ssl/ /etc/nginx/ssl/

RUN rm /etc/nginx/conf.d/default.conf
ADD nginx/conf.d/ /etc/nginx/conf.d/

# COPY --from=builder /app/dist/ /home/incareview-frontend

RUN mkdir -p /home/incareview-frontend/incare

RUN mkdir -p /home/incareview-frontend/incare/files

RUN mkdir -p /home/incareview-frontend/incare/flashcard

COPY --from=builder /app/dist/incare/ /home/incareview-frontend/incare/files

RUN mkdir -p /home/incareview-frontend/incare/files/app

COPY --from=builder /app/dist/incare/index.html /home/incareview-frontend/incare/files/app

ADD Incareview-Website/files /home/incareview-frontend/incare/files

ADD flashcard /home/incareview-frontend/incare/flashcard

ADD ngsw-worker.js /home/incareview-frontend/incare/files

EXPOSE ${FrontendPort}
