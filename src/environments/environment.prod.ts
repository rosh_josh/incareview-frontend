export const environment = {
  production: true,
  AWS: {
    // Test environment
    // IdentityPoolId: 'eu-west-1:d3f9fa72-0d08-4024-88c9-29afecf31225',
    // Region: 'eu-west-1',
    // Bucket: 'incareview-dev',
    // CloudFrontUrl: 'https://d2tzzzwpy23qee.cloudfront.net/'

    // Production Environment
    IdentityPoolId: 'eu-west-1:00a66e80-98e2-421a-a484-40637992464d',
    Region: 'eu-west-1',
    Bucket: 'incareview',
    CloudFrontUrl: 'https://d2gydfppkc2r1k.cloudfront.net/'
  }
};
