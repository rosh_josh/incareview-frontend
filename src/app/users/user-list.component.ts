import { Component, OnInit } from '@angular/core';
import { UserService } from '../services';
import { Router } from '@angular/router';
import { format } from 'date-fns'
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {

  userType: any = localStorage.getItem('userType');
  search: '';
  users: any = [];

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    if (!localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/login']);
    } else {
      if (Number(this.userType) === 1) {
        this.getUserList();
      } else {
        this.router.navigate(['/app/menu']);
      }
    }
  }

  getUserList() {
    this.userService.getUsersList().subscribe((list) => {
      this.users = list.result.userList;

      this.users = list.result.userList.map((user) => {
        user.expiry = format(new Date(user.expiry), 'dd/MM/yyyy')
        user.createdAt = format(new Date(user.created_at), 'dd/MM/yyyy')
        return user;
      })
    });
  }
}
