import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaVideofeedsComponent } from './media-videofeeds.component';

describe('MediaVideofeedsComponent', () => {
  let component: MediaVideofeedsComponent;
  let fixture: ComponentFixture<MediaVideofeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaVideofeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaVideofeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
