import { Component, OnInit } from '@angular/core';
import { MediaService } from '../services';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-media-videofeeds',
  templateUrl: './media-videofeeds.component.html',
  styleUrls: ['./media-videofeeds.component.scss']
})
export class MediaVideofeedsComponent implements OnInit {

  mediaList: any = [];

  constructor(
    private router: Router,
    private mediaService: MediaService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getMediaList();
  }

  getMediaList() {
    this.mediaService.getMedia().subscribe((media) => {

      this.mediaList = media.result;
      console.log(this.mediaList);
    });
  }

  deleteMedia(media) {
    console.log(media.id);
    this.mediaService.deleteMedia(media.id).subscribe((data) => {
      console.log(data);
      this.toastr.success(data.result.message);
      this.getMediaList();
    });
  }

  updateMedia(media) {
    console.log(media.id);
    this.router.navigate(['/app/media-edit', media.id]);
  

  }

  onShown() {

  }

  onHidden() {

  }

  isOpenChange() {

  }

}
