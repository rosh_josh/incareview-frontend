import { Component, OnInit, ViewChild, Input, NgZone, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaService, S3UploadService, MyOrgService } from '../services';
import { AppConfig } from './../app.config';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
  @ViewChild('videoplayer') videoPlayer: any;
  @ViewChild('canvas') canvas: any;
  @Output() saveDone: EventEmitter<any> = new EventEmitter<any>();
  context: any;
  @Input() width = 320;
  @Input() height = 240;
  @Input() tagID = 0;
  @Input() summaryId;
  @Input() patientId;
  @Input() mediaType;
  redirectUrl;
  tagId;
  tabs;
  isMobileview = false;
  type;
  isUploadErr = '';
  isFlashCardEnabled: boolean = Number(localStorage.getItem('userType')) === 3;

  constructor(private router: Router,
    private mediaService: MediaService, private s3upload: S3UploadService,
    private route: ActivatedRoute, private ngZone: NgZone,
    private myOrgService: MyOrgService, private config: AppConfig, private toastr: ToastrService) {

    window.onresize = (e) => {
      this.ngZone.run(() => {
        this.isCheckMobile();
      });
    };
    this.isCheckMobile();
  }

  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.tagId = this.route.snapshot.queryParamMap.get('tagId');
    this.route.snapshot.queryParamMap.get('summaryId')
      ? this.summaryId = this.route.snapshot.queryParamMap.get('summaryId') : -1;
    this.route.snapshot.queryParamMap.get('patientID')
      ? this.patientId = this.route.snapshot.queryParamMap.get('patientID') : -1;
    this.route.snapshot.queryParamMap.get('tabs')
      ? this.tabs = this.route.snapshot.queryParamMap.get('tabs') : -1;
  }

  isCheckMobile() {
    if (window.innerWidth < 673) {
      this.isMobileview = false;
    } else {
      this.isMobileview = true;
    }
  }

  upload(files: File[]) {
    this.isUploadErr = '';
    this.type = '';
    this.uploadAndProgress(files);
  }

  openFlashCard() {
    const socketDt = {
      type: this.tagID > 0 ? 6 : 1,
      mediaType: 'flashCard'
    }
    this.myOrgService.mediaFormEvent.emit(socketDt);
  }

  async uploadAndProgress(files: File[]) {
    if (files.length === 1) {
      const size = await this.formatBytes(files[0].size);
      const file: any = {};
      file.size = size;
      file.type = files[0].type;
      file.type = file.type.slice(0, file.type.indexOf('/'));
      if (file.type === 'application') {
        file.type = 'document';
      }
      let filename = files[0].name;
      file.format = filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
      let supportedFormat = this.config.supported_Media && this.config.supported_Media[file.type];
      let index = -1;
      if (supportedFormat) {
        index = supportedFormat.findIndex(obj => obj === file.format.toLowerCase())
      }
      if (index > -1) {
        this.mediaService.fileData = file;
        let filesForUpload = { files, file, filename };
        this.mediaService.setFilesForUpload(filesForUpload);

        if (!this.isMobileview) {
          this.router.navigateByUrl(
            this.router.createUrlTree(
              ['/app/media-form'], {
              queryParams: {
                tagId: this.tagId, redirectUrl: this.redirectUrl, summaryId: this.summaryId,
                patientID: this.patientId, tabs: this.tabs
              }
            }
            ));
        } else {
          if (this.patientId) {
            this.saveDone.emit();
          }
          if (this.tagID > 0) {
            this.myOrgService.mediaFormEvent.emit({ type: 6 });
          } else {
            this.myOrgService.mediaFormEvent.emit({ type: 1 });
          }
        }
      } else {
        this.type = file.type;
        this.isUploadErr = supportedFormat && supportedFormat.length > 0 ? supportedFormat.toString() : '';
        this.toastr.error('Invalid File Formats');
      }
    }

  }
  async formatBytes(bytes, decimals = 2) {
    if (bytes < 1073741824) {
      return (bytes / 1048576).toFixed(3) + ' MB';
    } else {
      return (bytes / 1073741824).toFixed(3) + ' GB';
    }
  }

  changeRedirect() {
    this.router.navigateByUrl(
      this.router.createUrlTree(
        [this.redirectUrl], {
        queryParams: {
          patientId: this.patientId,
          summaryId: this.summaryId,
          tabs: this.tabs
        }
      })
    );
  }

  redirectPage() {
    if (this.tagId) {
      this.router.navigate([this.redirectUrl]);
    } else if (this.patientId) {
      this.changeRedirect();
    } else {
      this.router.navigate(['/app/media-slides']);
    }
  }
}
