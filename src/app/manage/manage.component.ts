import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {

  constructor(private router: Router, private notificationService: NotificationService) { }

  gotomenu() {
    this.notificationService.changemenuEvent.emit(true);
    this.router.navigate(['/app/menu']);
  }

  ngOnInit() {
  }

}
