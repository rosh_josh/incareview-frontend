import { Component, OnInit } from '@angular/core';
import { MediaService } from '../services';
@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss']
})
export class MediaListComponent implements OnInit {

  constructor(private mediaService: MediaService) { }

  ngOnInit() {
    this.mediaService.getMedia().subscribe((media) => {
      console.log(media);
    });
  }

}
