import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MediaService, MyOrgService } from '../services';
import { Location } from '@angular/common';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-medication',
  templateUrl: './add-medication.component.html',
  styleUrls: ['./add-medication.component.scss']
})
export class AddMedicationComponent implements OnInit, OnChanges {

  @Input() currentTagID = 0;
  @Input() editMediaID = 0;
  @Input() summaryId;
  @Input() patientId;

  medicationForm: FormGroup;
  groupName: FormControl;
  genericName: FormControl;
  amountOfMg: FormControl;
  medicationUse: FormControl;
  comments: FormControl;
  medTime: FormControl;
  medFoodTime: FormControl;
  otherMedTime: FormControl;
  otherFoodTime: FormControl;
  orgDetail: FormControl;
  submitted = false;
  showOthertime = false;
  showOtherfood = false;
  midtime: any = [];
  foodtime: any = [];
  editId;
  editData;
  title = 'Add';
  careList: any = [];
  nodeID;
  uId;
  tagId;
  tabs;
  doseError=false;

  medTimeLists = [
    { value: 'Morning', checked: false },
    { value: 'Afternoon', checked: false },
    { value: 'Night', checked: false },
    { value: 'Others', checked: false },
  ];

  footTimeLists = [
    { value: 'Before Food', key: '1', checked: false },
    { value: 'After Food', key: '2', checked: false },
    { value: 'Others', key: '3', checked: false }
  ];

  redirectUrl;
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private mediaService: MediaService,
    private myOrgService: MyOrgService,
    private location: Location,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getMyorg();
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.route.snapshot.queryParamMap.get('tagId')
    ? this.tagId = this.route.snapshot.queryParamMap.get('tagId') : -1;
    this.route.snapshot.queryParamMap.get('summaryId')
    ? this.summaryId = this.route.snapshot.queryParamMap.get('summaryId') : -1;
    this.route.snapshot.queryParamMap.get('patientID')
    ? this.patientId = this.route.snapshot.queryParamMap.get('patientID') : -1;
    this.route.snapshot.queryParamMap.get('tabs')
    ? this.tabs = this.route.snapshot.queryParamMap.get('tabs') : -1;
    this.route.snapshot.queryParamMap.get('medId')
    ? this.editMediaID = parseInt(this.route.snapshot.queryParamMap.get('medId')) : -1;
    if(this.editMediaID > 0){
      this.editId = this.editMediaID;
      this.viewMedByID();
    }
    console.log(this.redirectUrl)
    console.log(this.tagId)
    console.log(this.summaryId)
    console.log(this.patientId)
    console.log(this.editId)
    console.log(this.editMediaID)

    
    this.createFormControls();
    this.createForm();
    console.log((this.editId > 0))

  }

  ngOnChanges() {
    if (this.currentTagID > 0) {
      this.tagId = this.currentTagID;
    }
    if(this.editMediaID > 0){
      this.editId = this.editMediaID;
      this.viewMedByID();
    }
  }

  createFormControls() {

    this.groupName = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.genericName = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.amountOfMg = new FormControl('', [Validators.required]);
    this.medicationUse = new FormControl('', [Validators.required,
    Validators.minLength(3)]);
    this.comments = new FormControl('', [Validators.required]);
    this.medTime = new FormControl('', [Validators.required]);
    this.medFoodTime = new FormControl('', [Validators.required]);
    this.otherMedTime = new FormControl('');
    this.otherFoodTime = new FormControl('');
    this.orgDetail = new FormControl('');





  }

  createForm() {
    this.medicationForm = new FormGroup({
      groupName: this.groupName,
      genericName: this.genericName,
      amountOfMg: this.amountOfMg,
      medicationUse: this.medicationUse,
      comments: this.comments,
      medTime: this.medTime,
      medFoodTime: this.medFoodTime,
      otherMedTime: this.otherMedTime,
      otherFoodTime: this.otherFoodTime,
      orgDetail: this.orgDetail
    });

    const currentUrl = this.location.path();
    console.log(currentUrl);
    if (currentUrl) {
      console.log(currentUrl.slice(0, 4));
      if (currentUrl.slice(0, 4) !== '/tag') {
        this.route.params.subscribe((data) => {
          if (data.id && !this.patientId) {
            // console.log(data.id);
            this.editId = data.id;
            this.viewMedByID();
          }

        });
      }
    }




  }

  getMyorg() {

    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      console.log(nodes);


      if (nodes.result.length > 0) {
        this.careList = nodes.result;
        this.careList = _.uniqBy(this.careList, 'nodeId');
        if (this.uId && this.nodeID) {
          const index = this.careList.findIndex(element => (element.nodeId === this.nodeID && element.uId === this.uId));
          this.medicationForm.controls['orgDetail'].setValue(this.careList[index].id);
        }

      }
    });

  }

  onChangeUser(event) {
    const index = this.careList.findIndex(element => element.id.toString() === event);
    if (index >= 0) {
      this.nodeID = this.careList[index].nodeId;
      this.uId = this.careList[index].uId;
    } else {
      this.medicationForm.controls['orgDetail'].setErrors({ required: true });
    }



  }

  viewMedByID() {

    this.mediaService.viewMedByID(this.editId).subscribe((medData: any) => {
      this.editData = medData.result.meds;
      console.log(this.editData);
      if (this.editData) {
        this.medicationForm.controls['groupName'].setValue(this.editData.groupName);
        this.medicationForm.controls['genericName'].setValue(this.editData.genericName);
        this.medicationForm.controls['amountOfMg'].setValue(this.editData.amountOfMg);
        this.medicationForm.controls['medicationUse'].setValue(this.editData.medicationUse);
        this.medicationForm.controls['comments'].setValue(this.editData.comments);
        this.nodeID = this.editData.nodeId;
        this.uId = this.editData.uId;
        this.getMyorg();
        if (this.editData.medTime.Morning) {
          this.medTimeLists[0].checked = this.editData.medTime.Morning;
          this.medTimeChange(this.editData.medTime.Morning, 'Morning');
        }
        if (this.editData.medTime.Afternoon) {
          this.medTimeLists[1].checked = this.editData.medTime.Afternoon;
          this.medTimeChange(this.editData.medTime.Afternoon, 'Afternoon');
        }
        if (this.editData.medTime.Night) {
          this.medTimeLists[2].checked = this.editData.medTime.Night;
          this.medTimeChange(this.editData.medTime.Night, 'Night');
        }
        if (this.editData.medTime.Others) {
          this.medTimeLists[3].checked = this.editData.medTime.Others;
          this.medTimeChange(this.editData.medTime.Others, 'Others');
          this.medicationForm.controls['otherMedTime'].setValue(this.editData.medTimeOthers);
        }
        if (this.editData.medFoodTime.Before) {
          this.footTimeLists[0].checked = this.editData.medFoodTime.Before;
          this.medFoodChange(this.editData.medFoodTime.Before, 'Before Food');
        }
        if (this.editData.medFoodTime.After) {
          this.footTimeLists[1].checked = this.editData.medFoodTime.After;
          this.medFoodChange(this.editData.medFoodTime.After, 'After Food');
        }
        if (this.editData.medFoodTime.Others) {
          this.footTimeLists[2].checked = this.editData.medFoodTime.Others;
          this.medFoodChange(this.editData.medFoodTime.Others, 'Others');
          this.medicationForm.controls['otherFoodTime'].setValue(this.editData.medFoodTimeOthers);
        }
        this.title = 'Edit';
      }
    });

  }


  get f() { return this.medicationForm.controls; }

  medTimeChange(type, value) {

    if (type) {
      if (value === 'Others') {
        this.showOthertime = true;
        // this.medicationForm.controls['otherMedTime'].setValidators([Validators.required]);
      }
      const found = this.midtime.find(data => data === value);
      if (!found) {
        this.midtime.push(value);
        this.medicationForm.controls['medTime'].setValue(this.midtime);
      }
    } else {
      if (value === 'Others') {
        this.showOthertime = false;
        // this.medicationForm.controls['otherMedTime'].clearValidators();
      }
      const index = this.midtime.indexOf(value);
      if (index !== -1) {
        this.midtime.splice(index, 1);
        this.medicationForm.controls['medTime'].setValue(this.midtime);
        if (this.midtime.length === 0) {
          this.medicationForm.controls['medTime'].setValue('');
        }
      }
    }

  }


  medFoodChange(type, value) {

    if (type) {
      if (value === 'Others') {
        this.showOtherfood = true;
        // this.medicationForm.controls['otherFoodTime'].setValidators([Validators.required]);
      }
      const found = this.foodtime.find(data => data === value);
      if (!found) {
        this.foodtime.push(value);
        this.medicationForm.controls['medFoodTime'].setValue(this.foodtime);
      }
    } else {
      if (value === 'Others') {
        this.showOtherfood = false;
        // this.medicationForm.controls['otherFoodTime'].clearValidators();
      }
      const index = this.foodtime.indexOf(value);
      if (index !== -1) {
        this.foodtime.splice(index, 1);
        this.medicationForm.controls['medFoodTime'].setValue(this.foodtime);
        if (this.foodtime.length === 0) {
          this.medicationForm.controls['medFoodTime'].setValue('');
        }
      }
    }

  }

  onSubmit() {

    console.log(this.medicationForm.value);
    console.log(this.tagId);
    console.log(this.currentTagID);
    this.submitted = true;

    if (this.medicationForm.invalid) {
      return;
    }

    let params: any = {};
    params = this.medicationForm.value;
    if(params.amountOfMg === '0') {
      this.doseError = true;
      return;
    }
    params.amountOfMg = params.amountOfMg.toString();
    params.otherMedTime ? (params.medTimeOthers = params.otherMedTime, delete params.otherMedTime) : delete params.otherMedTime;
    params.otherFoodTime ? (params.medFoodTimeOthers = params.otherFoodTime, delete params.otherFoodTime) : delete params.otherFoodTime;
    params.nodeId = this.nodeID.toString();
    params.uId = this.uId.toString();
    params.tagId = this.editData && this.editData['tagId'] && this.editData['tagId'].length > 0 ? this.editData['tagId'].split(',') : (this.tagId) ? [this.tagId] : [];
    

    delete params.orgDetail;

    if (this.editId > 0) {
      this.mediaService.updateMeds(this.editId, params).subscribe((meds: any) => {
        if (meds.result.meds) {
          this.toastr.success('Medicine Updated Successfully');
          if(this.redirectUrl){
            if(this.patientId) {
              this.router.navigateByUrl(
                this.router.createUrlTree(
                  [this.redirectUrl], {
                    queryParams: {
                      patientId:this.patientId,
                      summaryId:this.summaryId,
                      tabs: this.tabs
                    }
                  }
                )
              );
            } else {
              this.router.navigate([this.redirectUrl]);
            }
            
          } else {
            this.myOrgService.mediaFormEvent.emit({type: 5});
          }
        } else if (meds.result.err) {
          this.toastr.error(meds.result.err);
        }
      }, (err) => {

        this.toastr.error(err.error.err.message);
      });
    } else {
      if(this.summaryId) params.summaryId = this.summaryId;
      if(this.patientId) params.createdFor = this.patientId;
      this.mediaService.createMeds(params).subscribe((meds: any) => {
        console.log(meds);
        if (meds.result.meds) {
          this.toastr.success('Medicine Added Successfully');
          if(this.redirectUrl){
            if(this.patientId) {
              this.router.navigateByUrl(
                this.router.createUrlTree(
                  [this.redirectUrl], {
                    queryParams: {
                      patientId:this.patientId,
                      summaryId:this.summaryId,
                      tabs: this.tabs
                    }
                  }
                )
              );
            } else {
              this.router.navigate([this.redirectUrl]);
            }
          } else {
            this.myOrgService.mediaFormEvent.emit({type: 5});
          }
         
        }
      }, (err) => {
        this.toastr.error(err.error.err.message);
      });
    }




  }

  gotoBack() {
    this.router.navigate([this.redirectUrl]);
  }

  allowAlphaNumeric(event) {   
       let keyCode:any = event.charCode;
       let value = event.target.value;
       if(!value && parseInt(event.key) === 0) {
         this.doseError = true;
         return false
       } else {
         this.doseError = false;
         return ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) ||(keyCode >= 97 && keyCode <= 122) || keyCode == 8 || keyCode == 32 || keyCode == 190)
       }       
    }

}
