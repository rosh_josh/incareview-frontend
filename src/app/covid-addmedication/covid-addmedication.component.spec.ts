import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidAddmedicationComponent } from './covid-addmedication.component';

describe('CovidAddmedicationComponent', () => {
  let component: CovidAddmedicationComponent;
  let fixture: ComponentFixture<CovidAddmedicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidAddmedicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidAddmedicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
