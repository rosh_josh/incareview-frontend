import { Component, OnInit } from '@angular/core';
import { NotificationService, UserService, MyOrgService } from '../services';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  userId;
  userInfo : any = {};
  showAdvanced = false;

  constructor(private router: Router, private route: ActivatedRoute, private location: Location, 
    private notificationService: NotificationService, private userService: UserService, 
    private toastr: ToastrService, private myOrgService : MyOrgService) { }

  gotomenu() {
    this.notificationService.changemenuEvent.emit(true);
    this.router.navigate(['/app/menu']);
  }

  ngOnInit() {
    this.userId = localStorage.getItem('userid');

    if(localStorage.getItem('currentusertoken')) {
      this.getUser(this.userId);
      this.getOrgs();
    } else{ 
      this.router.navigate(['/app/login'])
    }
  }

  getUser(id) {
    this.userService.getUserById(id).subscribe(userInfo => {
      this.userInfo = userInfo.result;
    })
  }

  gotochangePassword() {
    let redirectUrl = this.location.path();
    this.router.navigateByUrl(
      this.router.createUrlTree(['/app/change-password'], { queryParams: { redirectUrl } }));
  }

  handleChange(isChecked, type) {
    if(type === 'workMode') {
      this.userInfo['is_workmode'] = isChecked;
    }    
    if(type === 'noti') {
      this.userInfo['is_notify'] = isChecked;
    }
  }

  updateUser() {
    let updateData = {is_notify: this.userInfo['is_notify'], is_workmode: this.userInfo['is_workmode'],
    email: this.userInfo['email'], id: this.userInfo['id'], firstName: this.userInfo['firstName'],
    lastName: this.userInfo['lastName']};
    
    this.userService.updateProfile(updateData).subscribe(data => {
      this.toastr.success('User-Info Updated Successfully');
    }, (err) => {
      if (err.error.message) {
        this.toastr.error(err.error.message);
      } else {
        this.toastr.error(err.error.err.message);

      }
    })
  }

  getOrgs() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
       if(permissionList && permissionList.length > 0) {
         let mypermission = permissionList[0];
         let myActions = mypermission['myActions'];
        /* this.showAdvanced = (myActions['isCreate'] === true ||
          myActions['isCreate'] === 'true') ? true : false;*/
          this.showAdvanced = true;
       }
    });
  }

}
