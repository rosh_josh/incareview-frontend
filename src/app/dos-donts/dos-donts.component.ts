import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, UserService, MyOrgService, MediaService } from '../services';
import * as _ from 'lodash';


@Component({
  selector: 'app-dos-donts',
  templateUrl: './dos-donts.component.html',
  styleUrls: ['./dos-donts.component.scss']
})
export class DosDontsComponent implements OnInit {

  navigateFlag = 1;
  dosIds;
  dosDontsInfo:any = [];
  dosData:any = [];

 constructor(private router: Router, private myOrgService: MyOrgService,
    private notificationService: NotificationService, private userService: UserService, private mediaService: MediaService) { }


  ngOnInit() {
    this.dosIds = this.mediaService.getPatientData();
    if(localStorage.getItem('userid')) {
      if(this.dosIds) {
        this.getDosDonts();
      } else {
        this.router.navigate(['/app/menu']);
      }
    } else {
      this.router.navigate(['/app/login']);
    }
  }


  navigateList(navigateto, dosData) {
    this.navigateFlag = navigateto;
    dosData.forEach(item => {
      let firstName = item['careInfo'] && item['careInfo']['firstName'];
      let lastName = item['careInfo'] && item['careInfo']['lastName'];
      item['docterName'] = firstName +' '+ lastName; 
    })
    this.dosData =  _.chain(dosData).groupBy('docterName').map((value, key) => ({docterName: key, data: value})).value();
  }

  goToInstruction(data) {
    this.mediaService.setMedData(data);
    this.router.navigate(['/app/instructions']);
  }

  changeDateTime() {
    this.dosDontsInfo.forEach(item => {
       item['postDate'] = new Date(item['postDate']).toISOString();
    })

  }

  getDosDonts() {
    this.mediaService.getPatientDosDonts({dosDontsId:this.dosIds}).subscribe(data => {
      if(data.result && data.result.dosDontsList && data.result.dosDontsList.length > 0) {
        data.result.dosDontsList.forEach(item => {
            let firstName = item['careInfo'] && item['careInfo']['firstName'];
            let lastName = item['careInfo'] && item['careInfo']['lastName'];
            item['docterName'] = firstName +' '+ lastName; 
            item['postDate'] = item['created_at'].split('T')[0];
            item['careTeamName'] = item['unitInfo'] && item['unitInfo']['name'];
        })
        this.dosDontsInfo = _.chain(data.result.dosDontsList).groupBy('careTeamName').map((value, key) => ({careTeamName: key, data: value})).value();
        //this.changeDateTime();
      }
    })
  }


}
