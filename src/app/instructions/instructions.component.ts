import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, UserService, MyOrgService, MediaService } from '../services';
import * as _ from 'lodash';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})

export class InstructionsComponent implements OnInit {
	 dosDontsInfo:any = [];
	 dos:any = [];
	 donts:any = [];
	 dosIds;

 constructor(private router: Router, private myOrgService: MyOrgService,
    private notificationService: NotificationService, private userService: UserService, private mediaService: MediaService) { }


  ngOnInit() {
  	if(localStorage.getItem('userid')) {
  		this.dosIds = this.mediaService.getPatientData();
	  	if(this.dosIds) {	  		
	  		this.getDosDonts();
	  	} else {
	  		this.router.navigate(['/app/dosdonts'])
	  	}
	  } else {
	  	this.router.navigate(['/app/login'])
	  }
  }

  grouppingDosDonts(dosdont) {
  	dosdont.forEach(item => {
		let postDate = _.chain(item['data']).groupBy('postDate').map((value, key) => ({docterName: item['docterName'] ,postDate: key, data: value})).value();
		postDate.forEach(key => {
			this.donts = [];
			this.dos = [];
			key['data'].forEach(i => {
				if(i['type'] === 'Dos') this.dos.push(i);
				if(i['type'] === 'Donts') this.donts.push(i);
			})
			let dataobj = {docterName: key['docterName'] ,postDate: key['postDate'],Dos:this.dos, Donts:this.donts }
			this.dosDontsInfo.push(dataobj);
			this.dosDontsInfo = _.orderBy(this.dosDontsInfo, 'postDate', 'desc').reverse();
			this.donts = [];
			this.dos = [];
		})
	})
  }

  getDosDonts() {
    this.mediaService.getPatientDosDonts({dosDontsId:this.dosIds}).subscribe(data => {
      if(data.result && data.result.dosDontsList && data.result.dosDontsList.length > 0) {
        data.result.dosDontsList.forEach(item => {
            let firstName = item['careInfo'] && item['careInfo']['firstName'];
            let lastName = item['careInfo'] && item['careInfo']['lastName'];
            item['docterName'] = firstName +' '+ lastName; 
            item['postDate'] = item['created_at'].split('T')[0];
            item['careTeamName'] = item['unitInfo'] && item['unitInfo']['name'];
        })
        let dosdont = _.chain(data.result.dosDontsList).groupBy('docterName').map((value, key) => ({docterName: key, data: value})).value();
        if(dosdont && dosdont.length > 0) this.grouppingDosDonts(dosdont);
      }
    })
  }

}
