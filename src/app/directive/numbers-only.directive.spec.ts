import { NumbersOnlyDirective } from './numbers-only.directive';
const elRefMock = {
  nativeElement: document.createElement('div')
};
describe('NumbersOnlyDirective', () => {
  it('should create an instance', () => {
    const directive = new NumbersOnlyDirective(elRefMock);
    expect(directive).toBeTruthy();
  });
});
