import { Component, OnInit } from '@angular/core';
import { UserService, NotificationService, AuthenticationService } from '../services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import _ from 'lodash';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {

  phoneMaxLength = 9;
  editProfileForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  secondary_email: FormControl;
  phoneNumber: FormControl;
  submitted = false;
  countryCodes = [];
  countryCode: String;
  defaultCountry: String;
  isValidPhoneError = false;
  mobilePattern: any = /^\d{10,}$/;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private notificationService: NotificationService,
    private userService: UserService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.defaultCountry = '+353';
    this.countryCode = 'IE';
    this.getCountryCodes();
    this.createFormControls();
    this.createForm();
  }

  setFlagByCountryCode(countryCode) {
    this.countryCodes.forEach((countryObj) => {
      if (countryObj.dial_code.toString() === countryCode.toString()) {
        this.countryCode = countryObj.code;
      }
    });
  }

  getCountryCodes() {
    this.authenticationService.getCountryCodes().subscribe(data => {
      this.countryCodes = data;
      this.countryCodes.forEach((countryObj) => {
        if (countryObj.dial_code === '+353' || countryObj.dial_code === '+91') {
          countryObj.priority = countryObj.dial_code === '+353' ? 1 : 2;
        } else {
          countryObj.priority = 3;
        }
      });

      this.countryCodes = _.orderBy(this.countryCodes, ['priority'], ['asc']);
      this.getUser();
    })
  }

  updatePhoneValidation() {
    this.phoneMaxLength = this.defaultCountry === '+353' ? 9 : 10;
    let phoneNumberField = this.editProfileForm.controls['phoneNumber'].value;
    phoneNumberField = phoneNumberField.substring(0, this.phoneMaxLength);

    if (phoneNumberField) {
      this.editProfileForm.controls['phoneNumber'].setValidators([
        Validators.required,
        Validators.maxLength(this.phoneMaxLength)
      ]);
      this.editProfileForm.controls['phoneNumber'].setValue(phoneNumberField);
    }
  }

  setCountryInfo(flag) {
    this.countryCode = flag.code;
    this.defaultCountry = flag.dial_code;
    this.updatePhoneValidation();
  }


  createFormControls() {

    this.firstName = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.lastName = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    // this.email = new FormControl('', [
    //   Validators.required,
    //   Validators.email
    // ]);
    this.secondary_email = new FormControl('', [
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.phoneNumber = new FormControl('', [
      Validators.required,
      Validators.maxLength(this.phoneMaxLength)
    ]);
  }

  createForm() {
    this.editProfileForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      secondary_email: this.secondary_email,
      phoneNumber: this.phoneNumber,

    });
  }

  get f() { return this.editProfileForm.controls; }

  getUser() {
    this.userService.getUser().subscribe((user) => {

      if (user.result) {
        this.editProfileForm.controls['firstName'].setValue(user.result.firstName);
        this.editProfileForm.controls['lastName'].setValue(user.result.lastName);
        this.editProfileForm.controls['email'].setValue(user.result.email);
        this.editProfileForm.controls['secondary_email'].setValue(user.result.secondary_email);
        this.editProfileForm.controls['phoneNumber'].setValue(user.result.phoneNumber);
        if (user.result.country_Code) {
          if (this.countryCodes && this.countryCodes.length > 0) {
            this.setFlagByCountryCode(user.result.country_Code);
          }

          this.defaultCountry = user.result.country_Code;
          this.updatePhoneValidation();
        }

      }
    });
  }

  removePrefixZeros(removedStatus = false) {
    if (removedStatus) {
      return this.editProfileForm.controls['phoneNumber'].value;
    } else {
      let phoneNumberValue = this.editProfileForm.controls['phoneNumber'].value;

      if (phoneNumberValue && phoneNumberValue.charAt(0).toString() === '0') {
        this.editProfileForm.controls['phoneNumber'].setValue(phoneNumberValue.substring(1, phoneNumberValue.length));
        phoneNumberValue = this.editProfileForm.controls['phoneNumber'].value;
        if (phoneNumberValue && phoneNumberValue.charAt(0).toString() === '0') {
          this.removePrefixZeros();
        } else {
          this.removePrefixZeros(true);
        }
      } else {
        this.removePrefixZeros(true);
      }
    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.editProfileForm.controls['phoneNumber'].value && this.editProfileForm.controls['phoneNumber'].value.length > 0) {
      this.removePrefixZeros();
    }

    this.isValidPhoneError = false;

    if (this.editProfileForm.invalid) {
      return;
    }

    if (this.editProfileForm.value['phoneNumber']) {
      let isValidPhone = parseInt(this.editProfileForm.value['phoneNumber']);
      this.isValidPhoneError = (isValidPhone != 0) ? false : true;
      this.mobilePattern = this.defaultCountry === '+353' ? /^\d{9,}$/ : /^\d{10,}$/;

      if (!this.mobilePattern.test(this.editProfileForm.controls['phoneNumber'].value)) {
        this.isValidPhoneError = true;
      }

      if (this.isValidPhoneError) {
        return;
      }
    }

    if (this.editProfileForm.value['secondary_email'] === null || this.editProfileForm.value['secondary_email'] === undefined) {
      delete this.editProfileForm.value['secondary_email'];
    }
    let params = this.editProfileForm.value;

    params.country_Code = this.defaultCountry;
    this.userService.updateProfile(params).subscribe((user) => {
      if (user.result.status === 200) {
        this.toastr.success(user.result.message);
        this.router.navigate(['/app/menu']);
      } else {
        this.toastr.error(user.result.message);
      }

    }, (err) => {
      console.log(err);
      this.toastr.error(err.error.err.message);
    });



  }

  gotomenu() {
    this.notificationService.changemenuEvent.emit(true);
    this.router.navigate(['/app/menu']);

  }

}
