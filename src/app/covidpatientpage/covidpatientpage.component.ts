import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserService, MyOrgService, MediaService } from '../services';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { CovidService } from '../services/covid.service';

@Component({
  selector: 'app-covidpatientpage',
  templateUrl: './covidpatientpage.component.html',
  styleUrls: ['./covidpatientpage.component.scss']
})
export class CovidpatientpageComponent implements OnInit {
  permissionList: any = [];
  patientList: any = [];
  search = '';
  modalRef: BsModalRef;
  showCreate = false;
  getname="";

  constructor(private userService: UserService,
    private router: Router,
    private mediaService: MediaService,
    private myOrgService: MyOrgService, private modalService: BsModalService,private covidService: CovidService,) { }

  ngOnInit() {
    if (localStorage.getItem('currentusertoken')) {

      this.myOrgService.getUserPermission().subscribe((data) => {
        this.permissionList = data.result.permissionList;

        if (this.permissionList.length > 0) {
          let actions = this.permissionList[0].myActions;
          if (actions['isCreate'] === true || actions['isCreate'] === 'true') {
            this.showCreate = true;
          }
          this.patientLists();
        } else {
          this.router.navigate(['/app/menu']);
        }
      });

      this.myOrgService.mediaFormEvent.subscribe((data) => {
        if (data.type === 3) {
          if (this.modalRef) {
            this.modalRef.hide();
            this.patientLists();
          }
        }
      });
    } else {
      this.router.navigate(['/app/login']);
    }  
    this.getaccordianname(); 
    
  }
  getaccordianname(){
    this.covidService.getdynamicname().subscribe((data) => {
      console.log(data)
      if(data.result){
        this.getname = data.result.categoryList[0].name;
        console.log(this.getname)
      }
    })
    }
  

  createPatientModal(addNewPatient: TemplateRef<any>) {
    localStorage.setItem('isSpecialPatientType', 'true');
    this.modalRef = this.modalService.show(addNewPatient, { class: 'modal-lg incareview-model org-window' });
  }

  searchPatient(key) {
    this.search = key;
    this.patientLists();
  }

  patientLists() {
    this.userService.patientList(this.search).subscribe((list) => {
      this.patientList = list.result.userList;
    });
  }
  gotoPatients(patient) {
    // this.mediaService.viewSummary(patient.id).subscribe(summary => {
    //   if(summary.result.status === 400){
    //   console.log('Invalid');
    //   } else {
    //     console.log(summary.result.summaryList);
    //   }
    // });
    this.router.navigate(['/app/covidpatientprofile', patient.id]);
  }



}
