import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidpatientpageComponent } from './covidpatientpage.component';

describe('CovidpatientpageComponent', () => {
  let component: CovidpatientpageComponent;
  let fixture: ComponentFixture<CovidpatientpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidpatientpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidpatientpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
