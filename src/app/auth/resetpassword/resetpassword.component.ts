import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { AuthenticationService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  resetpassworddata:any ={};
  code:any;
  socialUser:any;
  today = new Date();

  constructor(private toastr: ToastrService, private authenticationService:AuthenticationService,   private router:Router, private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.code = params['code'];
    });
  }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1}
  };

  verifyToken(){
    this.authenticationService.verifyToken(this.code).subscribe(res =>{
      if(res.result.status === 200) {
        this.socialUser = res.result.isSocialUser;
      } else {
        this.toastr.error(res.result.err);
        this.router.navigate(['/app/login'])
      }
    })
  }

  resetPasswordSubmit(){
    let resetPwData = {
      "isSocialUser":this.socialUser,
      "code":this.code,
      "dob":this.resetpassworddata.dob.formatted,
      "new_password":this.resetpassworddata.new_password,
      "confirmPassword":this.resetpassworddata.confirmPassword
    }
    // if(this.resetpassworddata.new_password != this.resetpassworddata.confirmPassword){
    //   this.toastr.error("Password and Confirm password must be same");
    // }
    this.authenticationService.resetPassword(resetPwData).subscribe(res => {
      console.log(res);     
      if(res.result.status != 422){
        this.toastr.success(res.result.message);
        this.router.navigate(['/app/login'])
      }else{
        this.toastr.error(res.result.err);
      }

    },
      err => {
        if (err.status != 200) {
          this.toastr.error(err.error.message);
        }
      }
    )
  }


  ngOnInit() {
    if(localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/menu'])
    } else {
      if(this.code){
        this.verifyToken();
      } else {
        this.router.navigate(['/app/login'])
      }
    } 
  }
}
