import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-valid-email',
  templateUrl: './valid-email.component.html',
  styleUrls: ['./valid-email.component.scss']
})
export class ValidEmailComponent implements OnInit {
  
  validEmail:any ={};
  isSubmit:boolean = false;

  constructor(private toastr: ToastrService, 
    private authenticationService:AuthenticationService, private router: Router) { }

   //Email Valid
   emailValid(){
    this.authenticationService.validEmail(this.validEmail.email).subscribe(res =>{
      if(res.result.status != 422){
        this.toastr.success("Email Activation Link sent to your email");
        this.isSubmit = false;
        this.router.navigate(['/app/login']);
      }else{
        this.toastr.error(res.result.err);
      }
    },err => {
    })
  }


  ngOnInit() {
    /*if(localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/myorg'])
    } */
  }

}
