import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptloginComponent } from './optlogin.component';

describe('OptloginComponent', () => {
  let component: OptloginComponent;
  let fixture: ComponentFixture<OptloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
