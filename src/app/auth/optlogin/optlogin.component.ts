import { Component, OnInit } from '@angular/core';
import { AuthenticationService, MyOrgService } from '../../services/index';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-optlogin',
  templateUrl: './optlogin.component.html',
  styleUrls: ['./optlogin.component.scss']
})
export class OptloginComponent implements OnInit {
  logindata: any = {};
  constructor(private toastr: ToastrService, private authenticationService: AuthenticationService,
    private router: Router, private myOrgService: MyOrgService) { }

  loginSubmit() {
    localStorage.clear();
    this.authenticationService.loginOtp(this.logindata).subscribe(res => {
      if (res) {
        let modelData = {
          "code": res.result.code,
          "grant_type": "authorization_code"
        }
        this.authenticationService.loginAuth(modelData).subscribe(res => {
          localStorage.setItem("currentusertoken", res.access_token.token.token);
          localStorage.setItem("userid", res.access_token.token.user_id);
          localStorage.setItem("userType", JSON.stringify(res.access_token.userInfo['userType']));
          localStorage.setItem("userInfo", JSON.stringify(res.access_token.userInfo));
          if (res.access_token.userInfo.isForce === 1) {
            localStorage.setItem('oldPass', 'ignore-pwd-check');
            this.router.navigate(['/app/change-password']);
          } else {
            if (res.access_token.userInfo['userType'] === 1 || res.access_token.userInfo['userType'] === '1') {
              this.router.navigate(['/app/ceo-management']);
              this.toastr.success("Login successful");
            } else {
              //this.router.navigate(['/app/menu']);
              this.getPermission();
            }
          }

        })
      }
    },
      err => {
        if (err) {
          this.toastr.error("This OTP code is invalid or expired");
        }
      })
  }


  ngOnInit() {
    if (localStorage.getItem('currentusertoken')) {
      let userInfo: any;
      localStorage.getItem('userInfo') ? userInfo = JSON.parse(localStorage.getItem('userInfo')) : -1;
      if (userInfo.isForce === 1) {
        this.router.navigate(['/app/change-password'])
      } else if (userInfo.isForce === 0) {
        this.router.navigate(['/app/menu']);
      }
    }

  }

  getPermission() {
    if (window.innerWidth < 673) {
      this.router.navigate(['/app/menu']);
      this.toastr.success("Login successful");
    } else {
      this.myOrgService.getUserPermission().subscribe((data) => {
        if (data.result.permissionList.length > 0) {
          this.router.navigate(['/app/patients']);
        } else {
          this.router.navigate(['/app/menu']);
        }
        this.toastr.success("Login successful");
      });
    }
  }

}
