import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import _ from 'lodash';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  phoneMaxLength = 9;
  forgotpassworddata: any = {};
  isSubmit: boolean = false;
  emailPattern: any = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  mobilePattern: any = /^\d{10,}$/;
  emailRequired: Boolean = false;
  emailPatterError: Boolean = false;
  isEmail: Boolean = true;
  countryCodes = [];
  countryCode: String;
  defaultCountry: String;

  constructor(private toastr: ToastrService,
    private authenticationService: AuthenticationService, private router: Router) { }

  forgotPasswordSubmit() {
    if (!this.isEmail) {
      this.forgotpassworddata.email = this.forgotpassworddata.phone;
    }

    this.authenticationService.forgotPassword(this.forgotpassworddata.email, this.defaultCountry).subscribe(res => {
      if (res.result.status != 422) {
        this.toastr.success(res.result.message);
        this.forgotpassworddata = {};
        this.isSubmit = false;
        this.router.navigate(['/app/login']);
      } else {
        if (this.isEmail) {
          this.toastr.error("Invalid Email");
        } else {
          this.toastr.error("Invalid Phone No");
        }
      }
    },
      err => {
        if (this.isEmail) {
          this.toastr.error("Invalid Email");
        } else {
          this.toastr.error("Invalid Phone No");
        }
      })
  }

  getCountryCodes() {
    this.authenticationService.getCountryCodes().subscribe(data => {
      this.countryCodes = data;
      this.countryCodes.forEach((countryObj, index) => {
        if (countryObj.dial_code === '+353' || countryObj.dial_code === '+91') {
          countryObj.priority = countryObj.dial_code === '+353' ? 1 : 2;
        } else {
          countryObj.priority = 3;
        }
      });

      this.countryCodes = _.orderBy(this.countryCodes, ['priority'], ['asc']);
    })
  }

  setCountryInfo(flag) {
    this.countryCode = flag.code;
    this.defaultCountry = flag.dial_code;
    this.phoneMaxLength = this.defaultCountry === '+353' ? 9 : 10;
    if (this.forgotpassworddata.phone && this.forgotpassworddata.phone.length > 0) {
      this.forgotpassworddata.phone = this.forgotpassworddata.phone.substring(0, this.phoneMaxLength);
    }
  }

  removePrefixZeros(removedStatus = false) {
    if (removedStatus) {
      return this.forgotpassworddata.phone;
    } else {
      if (this.forgotpassworddata.phone.charAt(0).toString() === '0') {
        this.forgotpassworddata.phone = this.forgotpassworddata.phone.substring(1, this.forgotpassworddata.phone.length);
        if (this.forgotpassworddata.phone.charAt(0).toString() === '0') {
          this.removePrefixZeros();
        } else {
          this.removePrefixZeros(true);
        }
      } else {
        this.removePrefixZeros(true);
      }
    }
  }

  validateForgotPwdInfo() {
    this.isSubmit = true;

    if (this.forgotpassworddata.phone && this.forgotpassworddata.phone.length > 0) {
      this.removePrefixZeros();
    }

    this.mobilePattern = this.defaultCountry === '+353' ? /^\d{9,}$/ : /^\d{10,}$/;

    if (!this.forgotpassworddata.email && !this.forgotpassworddata.phone) {
      this.emailRequired = true;
      this.emailPatterError = false;
    } else if ((this.isEmail && !this.emailPattern.test(this.forgotpassworddata.email)) || (!this.isEmail && !this.mobilePattern.test(this.forgotpassworddata.phone))) {
      this.emailPatterError = true;
      this.emailRequired = false;
    } else {
      if (!this.isEmail) {
        let isValidPhone = parseInt(this.forgotpassworddata.phone);
        this.emailPatterError = (isValidPhone != 0) ? false : true;
        if (!this.emailPatterError) {
          this.emailRequired = false;
          this.emailPatterError = false;
          this.forgotPasswordSubmit();
        }
      } else {
        this.emailRequired = false;
        this.emailPatterError = false;
        this.forgotPasswordSubmit();
      }

    }
  }

  ngOnInit() {
    if (localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/myorg'])
    }

    this.defaultCountry = '+353';
    this.countryCode = 'IE';
    this.getCountryCodes();
  }

  changeType() {
    this.isEmail = !this.isEmail;
    this.isSubmit = false;
    this.emailRequired = false;
    this.emailPatterError = false;
    this.forgotpassworddata = {};
  }


}
