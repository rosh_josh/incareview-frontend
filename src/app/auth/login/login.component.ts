import { Component, OnInit } from '@angular/core';
import { AuthenticationService, MyOrgService } from '../../services/index';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import _ from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  phoneMaxLength = 9;
  logindata: any = {};
  emailPattern: any = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  mobilePattern: any = /^\d{10,}$/;
  emailRequired: Boolean = false;
  emailPatterError: Boolean = false;
  submitted: Boolean = false;
  countryCodes = [];
  countryCode: String;
  defaultCountry: String;
  isEmail: Boolean = true;
  isValidPhoneError: Boolean = false;
  isRemembered: Boolean = false;

  constructor(private toastr: ToastrService, private authenticationService: AuthenticationService,
    private router: Router, private authService: AuthService, private myOrgService: MyOrgService) { }

  setRemeberMe(event) {
    this.isRemembered = event.target.checked;
  }

  loginSubmit() {
    localStorage.clear();
    if (!this.isEmail) {
      this.logindata.email = this.logindata.phone;
    }

    this.logindata['country_Code'] = this.defaultCountry;
    this.authenticationService.loginCode(this.logindata).subscribe(res => {
      if (res) {
        let modelData = {
          "code": res.result.code,
          "grant_type": "authorization_code"
        }
        this.authenticationService.loginAuth(modelData).subscribe(res => {
          if (this.isRemembered) {
            localStorage.setItem('loginInfo', JSON.stringify(this.logindata));
          } else {
            localStorage.removeItem('loginInfo');
          }
          localStorage.setItem("currentusertoken", res.access_token.token.token);
          localStorage.setItem("userid", res.access_token.token.user_id);
          localStorage.setItem("userType", JSON.stringify(res.access_token.userInfo['userType']));
          localStorage.setItem("userInfo", JSON.stringify(res.access_token.userInfo));
          if (res.access_token.userInfo.isForce === 1) {
            localStorage.setItem('oldPass', this.logindata['password']);
            this.router.navigate(['/app/change-password']);
          } else {
            if (res.access_token.userInfo['userType'] === 1 || res.access_token.userInfo['userType'] === '1') {
              this.router.navigate(['/app/ceo-management']);
              this.toastr.success("Login successful");
            } else {
              //this.router.navigate(['/app/menu']);
              this.getPermission();
            }
          }

        })
      }
    },
      err => {
        if (err && err.message) {
          console.log(err)
          if (err.error.err.message == 'Invalid Password') {
            this.toastr.error("Invalid Password");
          } else {
            if (this.isEmail) {
              this.toastr.error("Invalid Email");
            } else {
              this.toastr.error("Invalid Phone No");
            }
          }

        }
      })
  }

  removePrefixZeros(removedStatus = false) {
    if (removedStatus) {
      return this.logindata.phone;
    } else {
      if (this.logindata.phone.charAt(0).toString() === '0') {
        this.logindata.phone = this.logindata.phone.substring(1, this.logindata.phone.length);
        if (this.logindata.phone.charAt(0).toString() === '0') {
          this.removePrefixZeros();
        } else {
          this.removePrefixZeros(true);
        }
      } else {
        this.removePrefixZeros(true);
      }
    }
  }

  validateLoginInfo() {
    this.submitted = true;
    this.isValidPhoneError = false;

    if (this.logindata.phone && this.logindata.phone.length > 0) {
      this.removePrefixZeros();
    }

    this.mobilePattern = this.defaultCountry === '+353' ? /^\d{9,}$/ : /^\d{10,}$/;
    if (!this.logindata.email && !this.logindata.phone) {
      this.emailRequired = true;
      this.emailPatterError = false;
    } else if ((this.isEmail && !this.emailPattern.test(this.logindata.email)) || (!this.isEmail && !this.mobilePattern.test(this.logindata.phone))) {
      this.emailPatterError = true;
      this.emailRequired = false;
    } else if (this.logindata.password) {
      if (!this.isEmail) {
        let isValidPhone = parseInt(this.logindata.phone);
        this.emailPatterError = (isValidPhone != 0) ? false : true;
        if (!this.emailPatterError) {
          this.emailRequired = false;
          this.emailPatterError = false;
          this.loginSubmit();
        }
      } else {
        this.emailRequired = false;
        this.emailPatterError = false;
        this.loginSubmit();
      }

    }
  }

  getCountryCodes() {
    this.authenticationService.getCountryCodes().subscribe(data => {
      this.countryCodes = data;
      this.countryCodes.forEach((countryObj, index) => {
        if (countryObj.dial_code === '+353' || countryObj.dial_code === '+91') {
          countryObj.priority = countryObj.dial_code === '+353' ? 1 : 2;
        } else {
          countryObj.priority = 3;
        }
      });

      this.countryCodes = _.orderBy(this.countryCodes, ['priority'], ['asc']);
    })
  }

  setCountryInfo(flag) {
    this.countryCode = flag.code;
    this.defaultCountry = flag.dial_code;
    this.phoneMaxLength = this.defaultCountry === '+353' ? 9 : 10;
    this.logindata.phone = this.logindata.phone.substring(0, this.phoneMaxLength);
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    // this.authenticationService.socialLogin(FacebookLoginProvider.PROVIDER_ID).subscribe(res => {
    //   console.log(res)    
    // })
  }

  ngOnInit() {
    if (localStorage.getItem('loginInfo')) {
      let parsingSuccess = false;
      let rememberedInfo = {};

      try {
        if (localStorage.getItem('loginInfo')) {
          rememberedInfo = JSON.parse(localStorage.getItem('loginInfo'));
          parsingSuccess = true;
        }
      } catch (e) {
        rememberedInfo = {};
      }

      if (rememberedInfo && parsingSuccess) {
        this.isRemembered = true;
        this.logindata = rememberedInfo;
        if (this.logindata.phone && this.logindata.phone.length > 0) {
          this.isEmail = false;
        }
      }
    }
    // localStorage.getItem('currentusertoken') ?  this.router.navigate(['/myorg']) :-1;

    if (localStorage.getItem('currentusertoken')) {
      let userInfo: any;
      localStorage.getItem('userInfo') ? userInfo = JSON.parse(localStorage.getItem('userInfo')) : -1;
      if (userInfo.isForce === 1) {
        this.router.navigate(['/app/change-password'])
      } else if (userInfo.isForce === 0) {
        this.router.navigate(['/app/menu']);
      }
    }

    this.defaultCountry = '+353';
    this.countryCode = 'IE';
    this.getCountryCodes();

  }

  getPermission() {
    if (window.innerWidth < 673) {
      this.router.navigate(['/app/menu']);
      this.toastr.success("Login successful");
    } else {
      this.myOrgService.getUserPermission().subscribe((data) => {
        if (data.result.permissionList.length > 0) {
          this.router.navigate(['/app/covid']);
        } else {
          this.router.navigate(['/app/menu']);
        }
        this.toastr.success("Login successful");
      });
    }
  }

  changeType() {
    this.isEmail = !this.isEmail;
    this.submitted = false;
    this.isValidPhoneError = false;
    this.emailRequired = false;
    this.emailPatterError = false;
    this.logindata = {};
  }

}
