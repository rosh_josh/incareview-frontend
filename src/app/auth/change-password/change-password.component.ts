import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { AuthenticationService, UserService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changepassworddata: any = {};
  code: any;
  socialUser: any;
  userInfo: any;
  oldPass;
  redirectUrl;

  constructor(private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) {

  }





  resetPasswordSubmit() {
    const changePwData = {
      'old_password': this.changepassworddata.old_password,
      'new_password': this.changepassworddata.new_password,
      'confirmPassword': this.changepassworddata.confirmPassword
    };

    if (this.changepassworddata.new_password !== this.changepassworddata.confirmPassword) {
      this.toastr.error('New Password and Confirm password must be same');
    } else {
      if (this.oldPass === 'ignore-pwd-check') {
        changePwData['ignorePwdCheck'] = true;
      }

      this.userService.changePassword(changePwData).subscribe((user) => {
        if (user && user.result && user.result.err) {
          this.toastr.error(user.result.err);
        } else {
          this.toastr.success(user.result.message);
          if (this.userInfo && this.userInfo['isForce'] === 1) {
            this.userInfo['isForce'] = 0;
            localStorage.setItem("userInfo", JSON.stringify(this.userInfo));
            localStorage.removeItem('oldPass');
          }
          if (this.redirectUrl) {
            this.router.navigate([this.redirectUrl]);
          } else {
            if (this.userInfo['userType'] === 1 || this.userInfo['userType'] === '1') {
              this.router.navigate(['/app/ceo-management']);
            } else {
              this.router.navigate(['/app/menu']);
            }
          }
        }
      }, err => {
        this.toastr.error(err.error.message);
      });
    }


  }


  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    if (localStorage.getItem('currentusertoken')) {
      this.userInfo = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : -1;
      this.oldPass = localStorage.getItem('oldPass') ? localStorage.getItem('oldPass') : false;
      if (this.oldPass) {
        this.changepassworddata['old_password'] = this.oldPass;
      }
    } else {
      this.router.navigate(['/app/login']);
    }
  }

}
