import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkviaComponent } from './linkvia.component';

describe('LinkviaComponent', () => {
  let component: LinkviaComponent;
  let fixture: ComponentFixture<LinkviaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkviaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
