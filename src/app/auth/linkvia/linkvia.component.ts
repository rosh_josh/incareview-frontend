import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

@Component({
  selector: 'app-linkvia',
  templateUrl: './linkvia.component.html',
  styleUrls: ['./linkvia.component.scss']
})
export class LinkviaComponent implements OnInit {
	list;
	logindata:any = {};

  constructor(private toastr: ToastrService, private authenticationService:AuthenticationService, private router:Router, private route: ActivatedRoute, private authService: AuthService) { }


  loginSubmit(){
    localStorage.clear();
    this.authenticationService.loginCode(this.logindata).subscribe(res => {
      if(res){
        let modelData = {
          "code": res.result.code,
          "grant_type":"authorization_code"
        }
        this.authenticationService.loginAuth(modelData).subscribe(res =>{
          localStorage.setItem("currentusertoken", res.access_token.token.token); 
          localStorage.setItem("userid", res.access_token.token.user_id);   
          localStorage.setItem("userInfo", JSON.stringify(res.access_token.userInfo)); 
          this.toastr.success("Login successfully");
        })
      }
    },
    err => {
      if (err) {
        if(err.status === 409) {
          this.toastr.error(err.error.err.message);
        } else {
          this.toastr.error("Invalid Email/Password");
        }
        
      }
    })
  }


  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
 
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    // this.authenticationService.socialLogin(FacebookLoginProvider.PROVIDER_ID).subscribe(res => {
    //   console.log(res)    
    // })
  }

  ngOnInit() {
  	this.list = this.authenticationService.getData();
    if(localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/myorg'])
    } else{ 
      if(this.list && this.list['permissionId']) {
        this.logindata['secondary_email'] = this.list['email'];
        this.logindata['permissionId'] = this.list['permissionId'];
      } else {
        this.router.navigate(['/app/login']);
      }
    }
  }

}
