import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/index';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  
  registrationdata:any ={};
  isSubmit:boolean = false;
  id:any;
  permissionId:any
  userId:any;
  userEmail:any;
  activeInfo:any={};
  today = new Date();
  isDobError = false;

  constructor(private toastr: ToastrService, private authenticationService:AuthenticationService, private router:Router, private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this.permissionId = params['permissionId'];
    });
  }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    disableSince: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1}
  };


  activateUser(){
    this.authenticationService.activateUser(this.id,this.permissionId).subscribe(res => {
      if(res.result.status != 422 ){
        this.activeInfo = res.result.userInfo;
        this.userId = res.result.userInfo.id;
        this.userEmail = res.result.userInfo.email;
        this.registrationdata.firstName = this.activeInfo.firstName;
        this.registrationdata.lastName = this.activeInfo.lastName;
      }
      else{
        this.toastr.error(res.result.err);
        this.router.navigate(['/app/login'])
      }
    })
  }

  redirectLinkvia() {
    if(this.activeInfo && this.activeInfo['permissionId']) {
      this.authenticationService.setData(this.activeInfo);
      this.router.navigate(['/app/linkvia']);
    }
  }
 
  registrationSubmit(){

    let minYear = this.today.getFullYear()-18;
    let minMonth = this.today.getMonth()+1;
    let minDate = this.today.getDate();
    let dob = this.registrationdata['dob']['date'];
    this.isDobError = (minYear >= dob.year) ? false : true;

    if(minYear === dob.year) this.isDobError = (minMonth >= dob.month) ? false : true;
    if(minYear === dob.year && minMonth === dob.month) this.isDobError = (minDate >=dob.day) ? false : true;
    if(this.isDobError) return;


    let registerData = {
      "firstName":this.registrationdata.firstName,
      "lastName":this.registrationdata.lastName,
      "dob":this.registrationdata.dob.formatted,
      "email":this.userEmail,
      // "password":this.registrationdata.password,
      // "confirmPassword":this.registrationdata.confirmPassword,
    }
    // if( this.registrationdata.password != this.registrationdata.confirmPassword){
    //   this.toastr.error("Password and Confirm password must be same");
    // }
    if(this.activeInfo && this.activeInfo['permissionId'] && this.activeInfo['permissionId'] !== null) {
      registerData['permissionId'] = this.activeInfo['permissionId'];
    } else {
       registerData['id']=this.userId;
    }
    
    this.authenticationService.signup(registerData).subscribe(res => {
      // this.toastr.success("User created successfully");
      this.toastr.success('Username and Password sent to your email');
      this.router.navigate(['/app/login'])
    },err => {
    })
  }

  ngOnInit() {
    this.authenticationService.setData(undefined);
    if(localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/myorg'])
    } else {
      if(this.id){  
       this.activateUser();      
      } else {
       this.router.navigate(['/app/login'])
      }
    }
  }

}
