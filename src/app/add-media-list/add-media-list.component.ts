import { Component, OnInit } from '@angular/core';
import { MediaService } from '../services';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-media-list',
  templateUrl: './add-media-list.component.html',
  styleUrls: ['./add-media-list.component.scss']
})
export class AddMediaListComponent implements OnInit {
  redirectUrl = '';
  mediaList: any = [];
  mediaIDs: any = [];
  tagID = 0;
  constructor(
    private mediaService: MediaService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    console.log(this.redirectUrl);
    const URLlist = this.redirectUrl.split('/');
    console.log(URLlist.length);
    if (URLlist.length === 3) {
      this.tagID = +URLlist[2];
      console.log(this.tagID[1]);
      this.mediaService.viewTagByID(this.tagID).subscribe((tag) => {
        this.mediaList = tag.result.tagInfoList.mediaInfo;
        console.log(this.mediaList);
      });
    }
   
    // this.mediaService.getPatientMedia().subscribe((list)=>{
    //   console.log(list);
    // });
  }

  mediaChange(type, mediaID) {
    const found = this.mediaIDs.findIndex(element => element === mediaID);
    console.log(found);
    if (type) {
      if (found < 0) {
        this.mediaIDs.push(mediaID);
      }

    } else {
      if (found >= 0) {
        this.mediaIDs.splice(found, 1);
      }
    }
    console.log(this.mediaIDs);
    // console.log(mediaID);
  }

  addMedia() {
    console.log(this.mediaIDs);
    console.log(this.tagID);
    if (this.mediaIDs.length > 0) {
      if (this.tagID > 0) {

      }

    } else {
      this.toastr.error('Please Select Any One Media!');
    }
  }

  gotoBack() {
    this.router.navigate([this.redirectUrl]);
  }

}
