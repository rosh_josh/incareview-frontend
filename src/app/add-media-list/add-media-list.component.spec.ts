import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMediaListComponent } from './add-media-list.component';

describe('AddMediaListComponent', () => {
  let component: AddMediaListComponent;
  let fixture: ComponentFixture<AddMediaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMediaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMediaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
