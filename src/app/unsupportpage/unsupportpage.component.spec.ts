import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsupportpageComponent } from './unsupportpage.component';

describe('UnsupportpageComponent', () => {
  let component: UnsupportpageComponent;
  let fixture: ComponentFixture<UnsupportpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsupportpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsupportpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
