import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { UserService } from '../services';


@Component({
  selector: 'app-unsupportpage',
  templateUrl: './unsupportpage.component.html',
  styleUrls: ['./unsupportpage.component.scss']
})
export class UnsupportpageComponent implements OnInit {

  modalRef: BsModalRef;
  @ViewChild('unsupport') public unsupport: TemplateRef<any>;

  constructor(private modalService: BsModalService, private router: Router, private userService: UserService) { }

  ngOnInit() {
    let browser = localStorage.getItem('browser');
    if (browser === 'Safari') {
      this.modalRef = this.modalService.show(this.unsupport, { backdrop: 'static', keyboard: false, class: 'modal-lg incareview-model incare-window' });
    } else {
      this.router.navigate(['/app/menu']);
    }
  }

  redirectPage() {
    let rememberedInfo = localStorage.getItem('loginInfo');
    this.modalRef.hide();
    localStorage.clear();
    sessionStorage.clear();
    localStorage.setItem('loginInfo', rememberedInfo);
    location.replace('https://incareview.com');
  }

  closeApplication() {
    this.userService.logout().subscribe(data => {
      this.redirectPage();
    }, (err) => {
      this.redirectPage();
    });
  }

}
