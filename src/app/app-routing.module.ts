import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./auth/login/login.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { ForgotpasswordComponent } from "./auth/forgotpassword/forgotpassword.component";
import { ResetpasswordComponent } from "./auth/resetpassword/resetpassword.component";
import { ValidEmailComponent } from "./auth/valid-email/valid-email.component";
import { LinkviaComponent } from "./auth/linkvia/linkvia.component";
import { AppLayoutComponent } from "./layout/app-layout/app-layout.component";
import { MedicationsComponent } from "./medications/medications.component";
import { DosDontsComponent } from "./dos-donts/dos-donts.component";
import { AppointmentsComponent } from "./appointments/appointments.component";
import { MediaComponent } from "./media/media.component";
import { ForumsComponent } from "./forums/forums.component";
import { ContactComponent } from "./contact/contact.component";
import { MyOrgComponent } from "./my-org/my-org.component";
import { MobileMenuComponent } from "./mobile-menu/mobile-menu.component";
import { EditprofileComponent } from "./editprofile/editprofile.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { UserComponent } from "./manage-users/user.component";
import { AddPatientComponent } from "./add-patient/add-patient.component";
import { AuthGuard } from "./services/auth-guard";
import { PatientsComponent } from "./patients/patients.component";
import { CareTeamComponent } from "./care-team/care-team.component";
import { NotesComponent } from "./notes/notes.component";
import { DrugInfoComponent } from "./drug-info/drug-info.component";
import { PatientProfileComponent } from "./patient-profile/patient-profile.component";
import { InstructionsComponent } from "./instructions/instructions.component";
import { MediaListComponent } from "./media-list/media-list.component";
import { MediaFormComponent } from "./media-form/media-form.component";
import { MediaSlidesComponent } from "./media-slides/media-slides.component";
import { MediaVideofeedsComponent } from "./media-videofeeds/media-videofeeds.component";
import { MediaIvideoComponent } from "./media-ivideo/media-ivideo.component";
import { ChangePasswordComponent } from "./auth/change-password/change-password.component";
import { SettingsComponent } from "./settings/settings.component";
import { AskYourDoctorComponent } from "./ask-your-doctor/ask-your-doctor.component";
import { AddMedicationComponent } from "./add-medication/add-medication.component";
import { AddMediaListComponent } from "./add-media-list/add-media-list.component";
import { TagManagementComponent } from "./tag-management/tag-management.component";
import { TagsComponent } from "./tags/tags.component";
import { ManageComponent } from "./manage/manage.component";
import { AddCareteamComponent } from "./add-careteam/add-careteam.component";
import { AddusersComponent } from "./addusers/addusers.component";
import { CreateUserComponent } from "./create-user/create-user.component";
import { CreateDepartmentComponent } from "./create-department/create-department.component";
import { CeomanagementComponent } from "./ceomanagement/ceomanagement.component";
import { AddceoComponent } from "./addceo/addceo.component";
import { UnsupportpageComponent } from "./unsupportpage/unsupportpage.component";
import { PatientsummaryComponent } from "./patientsummary/patientsummary.component";
import { DownloadsummaryComponent } from "./downloadsummary/downloadsummary.component";
import { OptloginComponent } from "./auth/optlogin/optlogin.component";
import { CovidpatientsComponent } from "./covidpatients/covidpatients.component";
import { CovidpatientpageComponent } from "./covidpatientpage/covidpatientpage.component";
import { CovidpatientprofileComponent } from "./covidpatientprofile/covidpatientprofile.component";
import { CovidtagsComponent } from "./covidtags/covidtags.component";
import { CovidtagmanagementComponent } from "./covidtagmanagement/covidtagmanagement.component";
import { CovidMediaformComponent } from "./covid-mediaform/covid-mediaform.component";
import { CovidAddmedicationComponent } from "./covid-addmedication/covid-addmedication.component";
import { UserListComponent } from "./users/user-list.component";
import { MediaSlideshowComponent } from "./media-slideshow/media-slideshow.component";
import { AssessmentManagementComponent } from "./assessmentManagement/assessment-management.component";

const routes: Routes = [
  { path: "", redirectTo: "app/login", pathMatch: "full" },
  { path: "app/login", component: LoginComponent },
  { path: "app/otplogin", component: OptloginComponent },
  { path: "app/signup", component: SignupComponent },
  { path: "app/forgotpassword", component: ForgotpasswordComponent },
  { path: "app/resetpassword", component: ResetpasswordComponent },
  { path: "app/validemail", component: ValidEmailComponent },
  { path: "app/linkvia", component: LinkviaComponent },
  { path: "app/menu", component: MobileMenuComponent },
  {
    path: "app/editprofile",
    component: EditprofileComponent,
    canActivate: [AuthGuard],
  },
  { path: "app/notifications", component: NotificationsComponent },
  { path: "app/add-patient", component: AddPatientComponent },
  { path: "app/add-patient/:id", component: AddPatientComponent },
  { path: "app/careteam", component: CareTeamComponent },
  { path: "app/add-careteam", component: AddCareteamComponent },
  { path: "app/notes", component: NotesComponent },
  { path: "app/dosdonts", component: DosDontsComponent },
  { path: "app/druginfo", component: DrugInfoComponent },
  { path: "app/medications", component: MedicationsComponent },
  { path: "app/patient-summary", component: PatientsummaryComponent },
  // { path: 'patientprofile', component: PatientProfileComponent},
  { path: "app/instructions", component: InstructionsComponent },
  { path: "app/media", component: MediaComponent },
  { path: "app/media-list", component: MediaListComponent },
  { path: "app/media-edit/:id", component: MediaFormComponent },
  { path: "app/media-form", component: MediaFormComponent },
  { path: "app/covidmedia-form", component: CovidMediaformComponent },
  // { path: 'media-slides', component: MediaSlidesComponent },
  // { path: 'patient-media', component: MediaSlidesComponent },
  { path: "app/media-videofeeds", component: MediaVideofeedsComponent },
  { path: "app/media-ivideo", component: MediaIvideoComponent },
  { path: "app/change-password", component: ChangePasswordComponent },
  { path: "app/ask-your-doctor", component: AskYourDoctorComponent },
  { path: "app/appointments", component: AppointmentsComponent },
  { path: "app/add-medication", component: AddMedicationComponent },
  { path: "app/covidadd-medication", component: CovidAddmedicationComponent },
  { path: "app/edit-medication/:id", component: AddMedicationComponent },
  { path: "app/add-media-list", component: AddMediaListComponent },
  { path: "app/tag-management", component: TagManagementComponent },
  { path: "app/tag-management/:id", component: TagManagementComponent },
  { path: "app/assessment-management", component: AssessmentManagementComponent},
  { path: "app/covidtag-management", component: CovidtagmanagementComponent },
  {
    path: "app/covidtag-management/:id",
    component: CovidtagmanagementComponent,
  },
  { path: "app/create-user", component: CreateUserComponent },
  { path: "app/create-dept", component: CreateDepartmentComponent },
  { path: "app/addusers", component: AddusersComponent },
  { path: "app/add-ceo", component: AddceoComponent },
  { path: "app/unsupport", component: UnsupportpageComponent },
  {
    path: "",
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "app/forums", component: ForumsComponent },
      { path: "app/contact", component: ContactComponent },
      { path: "app/myorg", component: MyOrgComponent },
      { path: "app/manageusers", component: UserComponent },
      { path: "app/media-slides", component: MediaSlidesComponent },
      {
        path: "app/media-slideshow/:mediaType/:mediaId",
        component: MediaSlideshowComponent,
      },
      { path: "app/patients", component: PatientsComponent },
      { path: "app/covid", component: CovidpatientsComponent },
      { path: "app/covidpatients", component: CovidpatientpageComponent },
      {
        path: "app/covidpatientprofile/:id",
        component: CovidpatientprofileComponent,
      },
      { path: "app/covidtags", component: CovidtagsComponent },
      { path: "app/patientprofile/:id", component: PatientProfileComponent },
      { path: "app/tags", component: TagsComponent },
      { path: "app/settings", component: SettingsComponent },
      { path: "app/manage", component: ManageComponent },
      { path: "app/media-ivideo/:id", component: MediaIvideoComponent },
      { path: "app/patient-media", component: MediaSlidesComponent },
      { path: "app/ceo-management", component: CeomanagementComponent },
      { path: "app/users", component: UserListComponent },
      { path: "app/download", component: DownloadsummaryComponent },
    ],
  },
  { path: "**", redirectTo: "app/login", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
