import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserService, MyOrgService, MediaService } from '../services';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

  permissionList: any = [];
  patientList: any = [];
  search = '';
  modalRef: BsModalRef;
  showCreate = false;

  constructor(private userService: UserService,
    private router: Router,
    private mediaService: MediaService,
    private myOrgService: MyOrgService, private modalService: BsModalService) { }

  ngOnInit() {
    if (localStorage.getItem('currentusertoken')) {

      this.myOrgService.getUserPermission().subscribe((data) => {
        this.permissionList = data.result.permissionList;

        if (this.permissionList.length > 0) {
          let actions = this.permissionList[0].myActions;
          if (actions['isCreate'] === true || actions['isCreate'] === 'true') {
            this.showCreate = true;
          }
          this.patientLists();
        } else {
          this.router.navigate(['/app/menu']);
        }
      });

      this.myOrgService.mediaFormEvent.subscribe((data) => {
        if (data.type === 3) {
          if (this.modalRef) {
            this.modalRef.hide();
            this.patientLists();
          }
        }
      });
    } else {
      this.router.navigate(['/app/login']);
    }

  }

  createPatientModal(addNewPatient: TemplateRef<any>) {
    localStorage.setItem('isSpecialPatientType', 'false');
    this.modalRef = this.modalService.show(addNewPatient, { class: 'modal-lg incareview-model org-window' });
  }

  searchPatient(key) {
    this.search = key;
    this.patientLists();
  }

  patientLists() {
    this.userService.patientList(this.search).subscribe((list) => {
      this.patientList = list.result.userList;
    });
  }
  gotoPatients(patient) {
    // this.mediaService.viewSummary(patient.id).subscribe(summary => {
    //   if(summary.result.status === 400){
    //   console.log('Invalid');
    //   } else {
    //     console.log(summary.result.summaryList);
    //   }
    // });
    this.router.navigate(['/app/patientprofile', patient.id]);
  }



}
