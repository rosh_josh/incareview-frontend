export class AppConfig {
  // resourceUrl = "https://alpha.incareview.com/api/v1/gateway/";
  // socketUrl = "https://alpha.incareview.com/websocket";
  // cloudFrontURL = "https://d2tzzzwpy23qee.cloudfront.net/";
  // resourceUrl = "http://127.0.0.1:9007/api/v1/gateway/";
  // socketUrl = "http://127.0.0.1:9006/";
  resourceUrl = "https://incareview.com/api/v1/gateway/";
  socketUrl = "https://incareview.com/websocket";
  cloudFrontURL = "https://d2gydfppkc2r1k.cloudfront.net/";
  // resourceUrl = "https://incareview.com/api/v1/gateway/";
  // socketUrl = "https://incareview.com/websocket";
  // cloudFrontURL = "https://d2gydfppkc2r1k.cloudfront.net/";
  supported_Media = {
    image: ['jpg', 'jpeg', 'png', 'gif', 'jfif', 'tiff'],
    audio: ['mp3', 'aac', 'ogg', 'wav'],
    video: ['mp4', 'webm', 'ogv', 'mov', 'MOV'],
    document: ['pdf', 'xls', 'xlsx', 'doc', 'docx', 'ppt', 'pptx'],
    text: ['txt']
  }
  toolbar = [
    ['bold', 'italic', 'underline', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent', 'paragraph', 'blockquote', 'removeBlockquote', 'horizontalLine', 'orderedList', 'unorderedList'],
  ]
}