import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaService, S3UploadService, MyOrgService, VideoProcessingService } from '../services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import resetOrientation from 'reset-image-orientation';

@Component({
  selector: 'app-media-form',
  templateUrl: './media-form.component.html',
  styleUrls: ['./media-form.component.scss']
})
export class MediaFormComponent implements OnInit, OnChanges {

  @Input() isSpecial = false;
  @Input() tagID = 0;
  @Input() summaryId;
  @Input() patientId;
  @Input() mediaType;
  @Output() saveDones: EventEmitter<any> = new EventEmitter<any>();

  mediaForm: FormGroup;
  title: FormControl;
  description: FormControl;
  isPersonal: FormControl;
  tags: FormControl;
  orgDetail: FormControl;
  webUrl: FormControl;
  submitted = false;

  mediaDetail: any = {};
  mediaID = 0;
  progress = 0;
  isDisable = false;
  nodeID;
  uId;
  careList: any = [];
  showUser = true;
  tagList: any = [];
  redirectUrl;
  tagId;
  tabs;
  inputText = '';
  filesForUpload = {};
  newFile: any;
  videoFileUrl;
  imageFileUrl;
  isFileUploading: boolean = false;
  showPrivateOrg: boolean = true;

  constructor(private router: Router,
    private mediaService: MediaService,
    private toastr: ToastrService,
    private s3upload: S3UploadService,
    private myOrgService: MyOrgService,
    private videoProcess: VideoProcessingService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.videoFileUrl = '';
    this.imageFileUrl = '';
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.route.snapshot.queryParamMap.get('tagId') ?
      this.tagId = this.route.snapshot.queryParamMap.get('tagId') : -1;
    this.route.snapshot.queryParamMap.get('summaryId')
      ? this.summaryId = this.route.snapshot.queryParamMap.get('summaryId') : -1;
    this.route.snapshot.queryParamMap.get('patientID')
      ? this.patientId = this.route.snapshot.queryParamMap.get('patientID') : -1;
    this.route.snapshot.queryParamMap.get('tabs')
      ? this.tabs = this.route.snapshot.queryParamMap.get('tabs') : -1;
    this.createFormControls();
    this.createForm();
    this.route.params
      .subscribe(params => {
        if (params.id && !this.patientId) {
          this.mediaID = params.id;
          this.getMediabyID(params.id);
        }
      });

    if (this.mediaService.getTagId()) {
      this.showPrivateOrg = false;
      this.getTagInfo()
    } else {
      this.showPrivateOrg = true;
    }

    this.getMyorg();
    // this.getMediaTags();
    this.filesForUpload = this.mediaService.getFilesForUpload();
    if (this.filesForUpload && this.filesForUpload['files']) {
      let files = this.filesForUpload['files'];
      let file = this.filesForUpload['file'];

      if (file.type === 'video') {
        this.videoProcess.generateThumbnail(files[0]).then(videofile => {
          let newfilename = this.getFile(files[0].name)
          newfilename = newfilename + '.png';
          this.newFile = this.dataURLtoFile(videofile, newfilename);
          this.getBase64Link(this.newFile, 'video');
        }).catch((err) => {
          console.log(err);
        });
      } else {
        this.getBase64Link(files[0], 'image');
      }
    }

    this.s3upload.progress.subscribe((progress: any) => {
      if (this.isFileUploading) {
        this.progress = progress;
      }
    }, err => {
      console.log(err)
    });

    this.isDisable = false;
  }

  getBase64Link(file, type) {
    let reader = new FileReader();

    reader.onloadend = (e) => {
      if (type === 'video') {
        this.videoFileUrl = reader.result;
      } else {
        this.imageFileUrl = reader.result;
      }
    }

    reader.readAsDataURL(file);
  }

  ngOnChanges() {
    if (this.tagID > 0) {
      this.tagId = this.mediaService.getTagId();
      //this.getTagInfo();
    }
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  getFile(filePath) {
    return filePath.substr(filePath.lastIndexOf('\\') + 1).split('.')[0];
  }

  getTagInfo() {
    let tagType = this.mediaService.getTagType();

    if (tagType === 'general') {
      this.mediaService.viewTagByID(this.mediaService.getTagId()).subscribe((tag) => {
        this.mediaForm.controls['tags'].setValue([tag.result.tagInfoList['name']]);
      });
    } else {
      this.mediaService.viewSpecialTagByID(this.mediaService.getTagId()).subscribe((tag) => {
        this.mediaForm.controls['tags'].setValue([tag.result.tagInfoList['name']]);
      });
    }
  }

  getMyorg() {
    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      if (nodes.result.length > 0) {
        this.careList = nodes.result;
        this.careList = _.uniqBy(this.careList, 'nodeId');
      }
    });
  }

  getMediaTags() {
    this.mediaService.getMediaTag().subscribe((data: any) => {
      this.tagList = data.result.tagList;
      if (this.tagList.length > 0) {
        this.mediaForm.controls['tags'].setValue(this.tagList);
      }
    });
  }

  getMediabyID(mediaId) {
    this.mediaService.mediaViewbyID(mediaId).subscribe((media) => {
      this.mediaDetail = media.result;
      this.mediaForm.controls['title'].setValue(this.mediaDetail.name);
      this.mediaForm.controls['description'].setValue(this.mediaDetail.description);
      this.mediaForm.controls['webUrl'].setValue(this.mediaDetail.description)
      this.mediaForm.controls['isPersonal'].setValue(this.mediaDetail.isPersonal);
      this.mediaForm.controls['tags'].setValue(this.mediaDetail.tag);
      if (this.mediaDetail.isPersonal === 0) {
        this.mediaForm.controls['orgDetail'].setErrors({ required: true });
        this.showUser = true;
        this.nodeID = this.mediaDetail.nodeId;
        this.uId = this.mediaDetail.uId;
        const index = this.careList.findIndex(element => (element.nodeId === this.nodeID && element.uId === this.uId));
        if (index > -1) {
          this.mediaForm.controls['orgDetail'].setValue(this.careList[index].id);
        }
      }

      const file: any = {};
      file.size = this.mediaDetail.size;
      file.type = this.mediaDetail.type;
      file.name = this.mediaDetail.keyName;
      this.mediaService.fileData = file;
      this.isDisable = false;
    });
  }

  createFormControls() {
    this.title = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.description = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.isPersonal = new FormControl('0', [Validators.required]);
    this.tags = new FormControl('');
    this.orgDetail = new FormControl('');
    this.webUrl = new FormControl('');
  }

  createMedia(params) {
    this.isDisable = false;
    if (params.specialTagId && params.specialTagId.length === 0) {
      delete params.specialTagId;
    }

    if (params.tagId && params.tagId.length === 0) {
      delete params.tagId;
    }

    this.mediaService.createMedia(params).subscribe((media) => {
      if (media.result.err) {
        this.toastr.error(media.result.err);
      } else {
        this.submitted = false;
        this.mediaService.fileData = null;

        this.toastr.success(media.result.message);
        if (this.tagID === 0) {
          if (this.tagId) {
            this.router.navigate([this.redirectUrl]);
          } else {
            if (this.patientId) {
              this.saveDones.emit();
              this.changeRedirect();
            } else {
              this.router.navigate(['/app/media-slides']);
              this.myOrgService.mediaFormEvent.emit({ type: 2 });
            }
          }
        } else if (this.tagID > 0) {
          if (this.tagId) {
            if (this.redirectUrl) {
              this.router.navigate([this.redirectUrl]);
            } else {
              this.myOrgService.mediaFormEvent.emit({ type: 7 });
            }
          } else {
            // this.router.navigate(['media-slides']);
            this.myOrgService.mediaFormEvent.emit({ type: 7 });
          }
        }
      }
    }, (err) => {
      this.toastr.error(err.error.message);
    });
  }

  uploadFileToS3(params) {
    let files = this.filesForUpload['files'];
    let file = this.filesForUpload['file'];
    let filename = this.filesForUpload['filename'];

    if (file.type === 'image') {
      this.isDisable = true;
      resetOrientation(files[0], (base64) => {
        let newFile = this.dataURLtoFile(base64, filename);
        this.isFileUploading = true;
        this.s3upload.uploadfile(newFile).then((data: any) => {
          this.isFileUploading = false;
          file.name = data.key ? data.key : data.Key;
          params.keyName = file.name;
          this.mediaService.fileData = file;
          this.s3upload.progress.emit(100);
          this.createMedia(params);
        }).catch((err) => {
          console.log(err);
        });
      })
    } else {
      this.isDisable = true;
      this.isFileUploading = true;
      this.s3upload.uploadfile(files[0]).then((data: any) => {
        this.isFileUploading = false;
        file.name = data.key ? data.key : data.Key;
        params.keyName = file.name;
        this.mediaService.fileData = file;
        this.s3upload.progress.emit(100);
        if (file.type === 'video') {
          this.s3upload.uploadfile(this.newFile).then((data: any) => {
            this.mediaService.fileData['thumbnailUrl'] = data.key ? data.key : data.Key;
            params.thumbnailUrl = this.mediaService.fileData.thumbnailUrl;
            this.createMedia(params);
          }).catch((err) => {
            console.log(err);
          });
        } else {
          this.createMedia(params);
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  }

  createForm() {
    this.mediaForm = new FormGroup({
      title: this.title,
      description: this.description,
      isPersonal: this.isPersonal,
      tags: this.tags,
      orgDetail: this.orgDetail,
      webUrl: this.webUrl
    });
  }

  get f() { return this.mediaForm.controls; }

  onSubmit() {
    if (this.mediaForm.controls['isPersonal'].value === '0' && !this.mediaForm.controls['orgDetail'].value) {
      this.mediaForm.controls['orgDetail'].setErrors({ required: true });
    }
    if (this.mediaType === 'flashCard') {
      if (!this.mediaForm.controls['webUrl'].value) {
        this.mediaForm.controls['webUrl'].setErrors({ required: true });
      } else if (!this.mediaForm.controls['webUrl'].value.includes("https://www.incareview.com/") && !this.mediaForm.controls['webUrl'].value.includes("https://incareview.com/")) {
        this.mediaForm.controls['webUrl'].setErrors({ pattern: true });
      }
    }
    this.submitted = true;
    let tagValue = this.mediaForm.controls['tags'].value ? this.mediaForm.controls['tags'].value : [];
    if (this.inputText != "") this.inputText = this.inputText.replace("@", "");
    if (this.inputText != "") {
      tagValue.push({ display: this.inputText, value: this.inputText });
      this.inputText = '';
      tagValue = _.uniqBy(tagValue, 'value');
      this.mediaForm.controls['tags'].setValue(tagValue);
    }

    if (this.mediaForm.invalid) {
      return;
    }

    const params: any = {};
    params.tag = [];
    params['tagId'] = [];
    params['specialTagId'] = [];
    // params.tag = this.mediaForm.controls['tags'].value;
    let tagType = this.mediaService.getTagType();

    if (this.tagId) {

      if (tagType === 'general') {
        params['tagId'].push(this.tagId);
      } else {
        params['specialTagId'].push(this.tagId);
      }
    }
    if (this.mediaType === 'flashCard') {
      params.type = "flashCard";
      const url = this.mediaForm.controls['webUrl'].value.split('incareview.com/')[1]
      params.webUrl = `https://www.incareview.com/${url}`
    }
    if (this.mediaForm.controls['tags'].value && this.mediaForm.controls['tags'].value.length > 0) {
      this.mediaForm.controls['tags'].value.forEach((tag) => {
        if (tag.value) {
          params.tag.push(tag.value);
        } else if (tag) {
          params.tag.push(tag);
        }
      });
    }
    if (tagType !== 'general' && params['specialTagId'].length === params['tag'].length) params['tag'] = [];
    if (tagType === 'general' && params['tagId'].length === params['tag'].length) params['tag'] = [];
    if (this.summaryId) params.summaryId = this.summaryId;
    if (this.patientId) params.createdFor = this.patientId;

    if (this.mediaDetail && !this.mediaDetail.id) {
      //params.inherit = 'YES';
      params.name = this.mediaForm.controls['title'].value;
      params.description = this.mediaForm.controls['description'].value;
      if (this.mediaForm.controls['isPersonal'].value === '0') {
        if (this.mediaForm.controls['orgDetail'].value) {
          params.isPersonal = false;
          params.nodeId = this.nodeID.toString();
          params.uId = this.uId.toString();
        }
        // else {
        //   this.mediaForm.controls['orgDetail'].setErrors({required:true});
        // }

      } else if (this.mediaForm.controls['isPersonal'].value === '1') {
        params.isPersonal = true;

      }
      if (this.mediaService.fileData) {
        params.size = this.mediaService.fileData.size;
        params.type = this.mediaService.fileData.type;
        params.keyName = this.mediaService.fileData.name;
        this.uploadFileToS3(params);
      } else if (this.mediaType === 'flashCard') {
        this.createMedia(params);
      }
    } else {
      params.inherit = this.mediaDetail.inherit;
      params.isPersonal = this.mediaForm.controls['isPersonal'].value;
      params.name = this.mediaForm.controls['title'].value;
      params.description = this.mediaForm.controls['description'].value;
      params.size = this.mediaDetail.size;
      params.type = this.mediaDetail.type;
      params.keyName = this.mediaDetail.keyName;
      if (this.mediaForm.controls['isPersonal'].value.toString() === '0') {
        params.isPersonal = false;
        params.nodeId = this.nodeID.toString();
        params.uId = this.uId.toString();
      } else if (this.mediaForm.controls['isPersonal'].value.toString() === '1') {
        params.isPersonal = true;

      }
      this.mediaService.updateMedia(this.mediaDetail.id, params).subscribe((media) => {
        if (media.result.status === 400) {
          this.toastr.error(media.result.err);
        } else {
          this.submitted = false;
          this.mediaService.fileData = null;
          this.toastr.success(media.result.message);
          if (this.tagId) {
            this.router.navigate([this.redirectUrl]);
          } else {
            if (this.patientId) {
              this.saveDones.emit();
              this.changeRedirect();
            } else {
              this.router.navigate(['/app/media-slides']);
            }

          }
        }

      }, (err) => {
        console.log(err);
        // if (err.error.err.message) {
        //   this.toastr.error(err.error.err.message);
        // } if (err.error.message) {
        this.toastr.error(err.error.message);
        // } else {
        //   this.toastr.error(err.error.err);
        // }
      });
    }
  }

  onChange(event) {
    if (event === '0') {
      this.mediaForm.controls['orgDetail'].setErrors({ required: true });
      this.showUser = true;

    } else {
      const tmpVal = this.mediaForm.controls['orgDetail'].value;
      this.mediaForm.controls['orgDetail'].reset();
      this.mediaForm.controls['orgDetail'].setValue(tmpVal);
      this.showUser = false;
    }

  }

  onChangeUser(event) {
    if (event === '1') {
      this.mediaForm.controls['isPersonal'].setValue('1');
    } else {
      this.mediaForm.controls['isPersonal'].setValue('0');
      const index = this.careList.findIndex(element => element.id.toString() === event);
      if (index >= 0) {
        this.nodeID = this.careList[index].nodeId;
        this.uId = this.careList[index].uId;
      } else {
        this.mediaForm.controls['orgDetail'].setErrors({ required: true });
      }
    }
  }

  isCheckAlpha(keyCode) {
    return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 8 || keyCode == 32 || keyCode == 190)
  }

  redirectPage() {
    if (this.tagId) {
      this.router.navigate([this.redirectUrl]);
    } else {
      if (this.patientId) {
        this.changeRedirect();
      } else {
        this.router.navigate(['/app/media-slides']);
      }
    }
  }

  changeRedirect() {
    if (this.tabs) {
      this.router.navigateByUrl(
        this.router.createUrlTree(
          [this.redirectUrl], {
          queryParams: {
            patientId: this.patientId,
            summaryId: this.summaryId,
            tabs: this.tabs
          }
        }
        )
      );
    }
  }

}
