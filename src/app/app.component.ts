import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'incare';
  
  constructor(private router: Router) {
  	let browser = localStorage.getItem('browser');
  	if(browser === 'Safari') {
      // this.router.navigate(['/app/unsupport']);
    }      
  }
}


