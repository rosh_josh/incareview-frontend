import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidtagsComponent } from './covidtags.component';

describe('CovidtagsComponent', () => {
  let component: CovidtagsComponent;
  let fixture: ComponentFixture<CovidtagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidtagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidtagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
