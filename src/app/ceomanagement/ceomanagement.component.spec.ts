import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CeomanagementComponent } from './ceomanagement.component';

describe('CeomanagementComponent', () => {
  let component: CeomanagementComponent;
  let fixture: ComponentFixture<CeomanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CeomanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CeomanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
