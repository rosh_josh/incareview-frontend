import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserService, MyOrgService, MediaService } from '../services';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-ceomanagement',
  templateUrl: './ceomanagement.component.html',
  styleUrls: ['./ceomanagement.component.scss']
})
export class CeomanagementComponent implements OnInit {

  ceoList: any = [];
  search = '';
  userType: any = localStorage.getItem('userType');
  modalRef: BsModalRef;

  constructor(private userService: UserService, private router: Router, private myOrgService: MyOrgService, private modalService: BsModalService) { }

  ngOnInit() {
    if (!localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/login']);
    } else {
      if (Number(this.userType) === 1) {
        this.ceoLists();
        this.myOrgService.mediaFormEvent.subscribe((data) => {
          if (data.type === 3) {
            if (this.modalRef) {
              this.modalRef.hide();
              this.ceoLists();
            }
          }
        })
      } else {
        this.router.navigate(['/app/menu']);
      }
    }
  }

  searchCeo(key) {
    this.search = key;
    this.ceoLists();
  }

  ceoLists() {
    this.userService.ceoList(this.search).subscribe((list) => {
      this.ceoList = list.result.userList;
    });
  }

  createPatientModal(addNewPatient: TemplateRef<any>){
    this.modalRef = this.modalService.show(addNewPatient, { class: 'modal-lg incareview-model org-window',  backdrop : 'static', keyboard : false });
  }

}
