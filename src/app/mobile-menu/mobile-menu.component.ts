import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, UserService, MyOrgService, MediaService } from '../services';
import { CovidService } from '../services/covid.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.scss']
})
export class MobileMenuComponent implements OnInit {

  mobSideToggle = false;
  manageToggle = true;
  notificationList = [];
  notificationCount = 0;
  userInfo: any = {};
  permissionList: any = [];
  historyList: any = [];
  specialHistoryList: any = [];
  isPatient = true;
  userType;
  getname = "";

  constructor(private router: Router,
    private cdRef: ChangeDetectorRef,
    private myOrgService: MyOrgService, private covidService: CovidService,
    private notificationService: NotificationService, private userService: UserService, private mediaService: MediaService, private toastr: ToastrService) { }

  ngOnInit() {
    // this.notificationService.changemenuEvent.subscribe((data) => {
    //    this.mobSideToggle = data;
    // });
    let userId = localStorage.getItem('userid');

    if (localStorage.getItem('currentusertoken')) {
      this.getOrganisation();
      this.userType = localStorage.getItem('userType');
      this.getUser(userId);
      this.getNotification();
      this.getSummaryList(userId)
    } else {
      this.router.navigate(['/app/login'])
    }
    this.getaccordianname();

  }
  getaccordianname() {
    this.covidService.getdynamicname().subscribe((data) => {
      console.log(data)
      if (data.result) {
        this.getname = data.result.categoryList[0].name;
        console.log(this.getname)
      }
    })
  }

  getUser(id) {
    this.userService.getUserById(id).subscribe(userInfo => {
      this.userInfo = userInfo.result;
    })
  }

  getNotification() {
    this.notificationService.listNotification(1, 10).subscribe((notification) => {
      this.notificationCount = notification.result.unReadCount;
      console.log(this.notificationCount);
    });
  }

  mobNavToggle() {
    this.mobSideToggle = !this.mobSideToggle;
  }

  gotoCovid() {
    if (this.permissionList.length > 0) {
      this.router.navigate(['/app/covid']);
    }
  }

  gotoPatients() {
    if (this.permissionList.length > 0) {
      this.router.navigate(['/app/patients']);
    } else {
      this.router.navigate(['/app/patientprofile', localStorage.getItem('userid')]);
    }
  }

  getOrganisation() {
    this.myOrgService.getPermission().subscribe((data) => {
      this.permissionList = data.result.permissionList;
      if (this.permissionList.length === 0) {
        this.isPatient = true;
      } else if (this.permissionList.length > 0) {
        this.isPatient = false;
      }
    });
  }

  getSummaryList(userId) {
    this.mediaService.listSummary(userId, true).subscribe((data) => {
      this.historyList = data.result.summaryList;
      if (this.historyList && this.historyList.length === 0) {
        this.mediaService.setPatientData(undefined);
      }
    });
    this.mediaService.listspecialSummary(userId, true).subscribe((data) => {
      this.specialHistoryList = data.result.summaryList;
      if (this.specialHistoryList && this.specialHistoryList.length === 0) {
        this.mediaService.setPatientData(undefined);
      }
    });
  }

  goRedirectPage(type, url) {
    this.mediaService.setPatientData(undefined);
    let noHistory = false;
    let noSpecialHistory = false;

    if (this.historyList && this.historyList.length > 0) {
      let ids = [];
      this.historyList.forEach(item => {
        if (type === 'Dos') {
          item.dosDontsId = (item['dosDontsId'] && item['dosDontsId'].length > 0) ? item['dosDontsId'].split(',') : [];
          ids = ids.concat(item.dosDontsId);
        }
        if (type === 'Meds') {
          item.tagId = (item['medsId'] && item['medsId'].length > 0) ? item['medsId'].split(',') : [];
          ids = ids.concat(item.tagId);
        }
        if (type === 'Media') {
          item.medialinkId = (item['medialinkId'] && item['medialinkId'].length > 0) ? item['medialinkId'].split(',') : [];
          ids = ids.concat(item.medialinkId);
        }
      })
      if (type === 'Notes') {
        ids = this.historyList;
      }
      //if(ids && ids.length > 0) 
      this.mediaService.setType(type);
    } else {
      noHistory = true;
    }

    if (this.specialHistoryList && this.specialHistoryList.length > 0) {
      let ids = [];
      this.specialHistoryList.forEach(item => {
        if (type === 'Dos') {
          item.dosDontsId = (item['dosDontsId'] && item['dosDontsId'].length > 0) ? item['dosDontsId'].split(',') : [];
          ids = ids.concat(item.dosDontsId);
        }
        if (type === 'Meds') {
          item.tagId = (item['medsId'] && item['medsId'].length > 0) ? item['medsId'].split(',') : [];
          ids = ids.concat(item.tagId);
        }
        if (type === 'Media') {
          item.medialinkId = (item['medialinkId'] && item['medialinkId'].length > 0) ? item['medialinkId'].split(',') : [];
          ids = ids.concat(item.medialinkId);
        }
      })
      if (type === 'Notes') {
        ids = this.specialHistoryList;
      }
      //if(ids && ids.length > 0) 
      this.mediaService.setType(type);
    } else {
      noSpecialHistory = true;
    }

    if (noHistory && noSpecialHistory) {
      this.toastr.error('There is no tags assigned for you. Please try after sometime');
    }
    this.router.navigate(['/app/patient-summary']);
  }


  redirectPage() {
    let rememberedInfo = localStorage.getItem('loginInfo');
    localStorage.clear();
    sessionStorage.clear();
    localStorage.setItem('loginInfo', rememberedInfo);
    this.router.navigate(['/app/login']);
  }

  logout() {
    this.userService.logout().subscribe(data => {
      this.redirectPage();
    }, (err) => {
      this.redirectPage();
    });

  }

}
