import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService, UserService, MediaService, MyOrgService } from '../services/index';
import { IMyDpOptions } from 'mydatepicker';
@Component({
  selector: 'app-add-careteam',
  templateUrl: './add-careteam.component.html',
  styleUrls: ['./add-careteam.component.scss']
})
export class AddCareteamComponent implements OnInit {


  addCareForm: FormGroup;
  email: FormControl;
  username: FormControl;
  permissionId: FormControl;
  nodeId: FormControl;


  submitted = false;
  careteamID = 0;
  patientDetails: any = {};
  title = 'Add';
  redirectUrl = '';
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private mediaService: MediaService,
    private userService: UserService,
    private myOrgService: MyOrgService,
    private route: ActivatedRoute) { }


  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.route.params.subscribe((params: any) => {
      console.log(params.id);
      if (params.id) {
        this.careteamID = +params.id;
        this.title = 'Update';
        this.getPatientDetails();
      }
    });
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {

    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.permissionId = new FormControl(['READ']);
    this.username = new FormControl('', [
      Validators.required]);
      this.nodeId = new FormControl('', [Validators.required]);
  }

  createForm() {
    this.addCareForm = new FormGroup({
      email: this.email,
      permissionId: this.permissionId,
      username: this.username,
      nodeId: this.nodeId
    });
    this.addCareForm.controls['nodeId'].setValue(this.route.snapshot.queryParamMap.get('node'));
  }
  get f() { return this.addCareForm.controls; }


  createPage() {

  }

  gotoBack() {
    if (this.redirectUrl) {
      this.router.navigate([this.redirectUrl]);
    } else {
      this.router.navigate(['/app/patients']);
    }
  }

  permissionChange(change) {
    if (change) {
      this.addCareForm.controls['permissionId'].setValue(['READ', 'CREATE', 'UPDATE', 'DELETE', 'SHARE']);
    } else {
      this.addCareForm.controls['permissionId'].setValue(['READ']);
    }

  }

  getPatientDetails() {
    // this.userService.getUserById(this.careteamID).subscribe((user) => {
    //   this.patientDetails = user.result;
    //   console.log(this.patientDetails);
    //   this.addCareForm.controls['email'].setValue(this.patientDetails.email);
    //   this.addCareForm.controls['firstName'].setValue(this.patientDetails.firstName);
    //   this.addCareForm.controls['lastName'].setValue(this.patientDetails.lastName);
    //   if (this.patientDetails.dob) {
    //     this.setDate();
    //   }

    // });

  }



  onSubmit() {

    console.log(this.addCareForm.value);
    this.submitted = true;

    if (this.addCareForm.invalid) {
      return;
    }

    let params: any = {};
    params = this.addCareForm.value;

    if (this.careteamID === 0) {

      this.myOrgService.inviteUser(params).subscribe((data) => {
        this.submitted = false;
        if (data.result.status) {
          this.toastr.error(data.result.err);
        } else {
          this.toastr.success('Careteam Added successfully');
          this.gotoBack();
        }

      }, (err) => {

        this.toastr.error(err.error.message);
      });

    } else if (this.careteamID > 0) {

    }

  }
}
