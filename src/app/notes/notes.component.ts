import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { MediaService } from '../services';
import * as _ from 'lodash';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  modalRef: BsModalRef;
  notesInfo;
  noteData;

  constructor(private modalService: BsModalService,private router: Router, private mediaService: MediaService) { }

  ngOnInit() {
    this.notesInfo = this.mediaService.getPatientData();
    if(localStorage.getItem('userid')) {
      if(this.notesInfo && this.notesInfo.length > 0) {
        this.notesInfo.forEach(item => {
          let firstName = item['careInfo'] && item['careInfo']['firstName'];
          let lastName = item['careInfo'] && item['careInfo']['lastName'];
          item['docterName'] = firstName +' '+ lastName; 
        })
      } else {
        this.router.navigate(['/app/menu']);
      }
    } else {
      this.router.navigate(['/app/login']);
    }
  }

  openDetailedNotesModal(modalid, noteData) {  
    this.noteData = noteData;
    this.modalRef = this.modalService.show(modalid, { class: 'br-30'});
  }

}
