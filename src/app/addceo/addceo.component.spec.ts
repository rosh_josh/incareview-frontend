import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddceoComponent } from './addceo.component';

describe('AddceoComponent', () => {
  let component: AddceoComponent;
  let fixture: ComponentFixture<AddceoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddceoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddceoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
