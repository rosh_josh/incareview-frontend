import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService, AuthenticationService, MyOrgService } from '../services/index';
import { IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'app-addceo',
  templateUrl: './addceo.component.html',
  styleUrls: ['./addceo.component.scss']
})
export class AddceoComponent implements OnInit {

  addCeoForm: FormGroup;
  email: FormControl;
  firstName: FormControl;
  lastName: FormControl;
  dob: FormControl;
  submitted = false;
  userType: any = localStorage.getItem('userType');
  today = new Date();
  isDobError = false;

  constructor(
    private router: Router, private toastr: ToastrService, private fb: FormBuilder,
    private route: ActivatedRoute, private myOrgService: MyOrgService, private authenticationService: AuthenticationService, ) { }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    editableDateField: false,
    disableSince: { year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1 }
  };

  ngOnInit() {
    if (!localStorage.getItem('currentusertoken')) {
      this.router.navigate(['/app/login']);
    } else {
      if (this.userType === 1 || this.userType === '1') {
        this.createFormControls();
        this.createForm();
      } else {
        this.router.navigate(['app/menu']);
      }
    }
  }

  createFormControls() {

    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.firstName = new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern(/^[a-zA-Z ]{3,20}$/)
    ]);
    this.lastName = new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(/^[a-zA-Z ]{1,20}$/)
    ]);
    this.dob = new FormControl('', [
      Validators.required,]);

  }

  createForm() {
    this.addCeoForm = new FormGroup({
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      dob: this.dob
    });
  }



  get f() { return this.addCeoForm.controls; }

  onSubmit() {
    console.log(this.addCeoForm)
    this.submitted = true;
    if (this.addCeoForm.invalid) {
      return;
    }

    let minYear = this.today.getFullYear() - 18;
    let minMonth = this.today.getMonth() + 1;
    let minDate = this.today.getDate();
    let dob = this.addCeoForm.controls['dob'].value.date;
    this.isDobError = (minYear >= dob.year) ? false : true;

    if (minYear === dob.year) this.isDobError = (minMonth >= dob.month) ? false : true;
    if (minYear === dob.year && minMonth === dob.month) this.isDobError = (minDate >= dob.day) ? false : true;
    if (this.isDobError) return;

    let params: any = {};
    params.email = this.addCeoForm.controls['email'].value;
    params.firstName = this.addCeoForm.controls['firstName'].value;
    params.lastName = this.addCeoForm.controls['lastName'].value;
    params.dob = this.addCeoForm.controls['dob'].value.formatted;
    console.log(params);


    this.authenticationService.addCeo(params).subscribe(res => {
      if (res.result.status != 422) {
        this.toastr.success("Invite mail sent to CEO Successfully");
        this.submitted = false;
        this.closeModal();
        this.router.navigate(['/app/ceo-management']);
      } else {
        this.toastr.error(res.result.err);
      }
    }, err => {
    })

  }

  closeModal() {
    this.myOrgService.mediaFormEvent.emit({type: 3});
  }

}
