import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, UserService, MyOrgService, MediaService } from '../services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AccordionPanelComponent } from 'ngx-bootstrap/accordion';
import { CovidService } from '../services/covid.service';

@Component({
	selector: 'app-patientsummary',
	templateUrl: './patientsummary.component.html',
	styleUrls: ['./patientsummary.component.scss']
})
export class PatientsummaryComponent implements OnInit {
	historyList: any = [];
	covidlist: any = [];
	showGeneral: Boolean = false;
	userType;
	type;
	noteData;
	modalRef: BsModalRef;
	firstAccordian = false;
	secondAccordian = false;
	getname = "";
	@ViewChild('p1') p1: AccordionPanelComponent;
	@ViewChild('p2') p2: AccordionPanelComponent;
	@ViewChild('notesDetail') public notesDetail: TemplateRef<any>;
	format = '';

	constructor(private router: Router, private covidService: CovidService, private myOrgService: MyOrgService, private modalService: BsModalService,
		private notificationService: NotificationService, private userService: UserService, private mediaService: MediaService) { }

	getFormat(type) {
		switch(type) {
			case 'Dos':
				return `Do's & Dont's`
			case 'Notes':
				return 'Notes'
			case 'Meds':
				return 'Medications'
			case 'Media':
				return 'Media'
			default:
				return 'Reports'
		}
	}
	
	ngOnInit() {
		let userId = localStorage.getItem('userid');
		this.type = this.mediaService.getType();
		console.log(this.type)
		this.getaccordianname();
		if (localStorage.getItem('currentusertoken')) {
			this.userType = localStorage.getItem('userType');
			if (this.type) {
				this.format = this.getFormat(this.type)
				this.getSummaryList(userId)
			} else {
				this.router.navigate(['/app/menu']);
			}
		} else {
			this.router.navigate(['/app/login']);
		}
	}
	accordianfn1(a, b) {
		this.firstAccordian = a;
	}
	accordianfn2(a) {
		this.secondAccordian = a;
	}
	accordianclick() {
		alert("")
	}
	getaccordianname() {
		this.covidService.getdynamicname().subscribe((data) => {
			console.log(data)
			if (data.result) {
				this.getname = data.result.categoryList[0].name;
				console.log(this.getname)
			}
		})
	}

	getSummaryList(userId) {
		this.mediaService.listSummary(userId, true).subscribe((data) => {
			//this.historyList = data.result.summaryList;
			if (data.result && data.result.summaryList && data.result.summaryList.length === 0) {
				this.historyList = data.result.summaryList;
				this.mediaService.setPatientData(undefined);
			} else {
				let summaryType = data.result.summaryList;
				summaryType.forEach(item => {
					let firstName = item['careInfo'] && item['careInfo']['firstName'];
					let lastName = item['careInfo'] && item['careInfo']['lastName'];
					item['docterName'] = firstName + ' ' + lastName;
					if (this.type === 'Dos' && item['dosDontsId'] && item['dosDontsId'].length > 0) {
						this.historyList.push(item);
					}
					if (this.type === 'Notes' && item['notes'] && item['notes'] != "") {
						this.historyList.push(item);
					}
					if (this.type === 'Meds' && item['medsId'] && item['medsId'].length > 0) {
						this.historyList.push(item);
					}
					if (this.type === 'Media' && item['medialinkId'] && item['medialinkId'].length > 0) {
						this.historyList.push(item);
					}
				})
			}
			if (this.historyList && this.historyList.length > 0) {
				this.showGeneral = true;
			} else {
				this.showGeneral = false;
			}
		});
		this.mediaService.listspecialSummary(userId, true).subscribe((data) => {
			if (data.result && data.result.summaryList && data.result.summaryList.length === 0) {
				this.covidlist = data.result.summaryList;
				this.mediaService.setPatientData(undefined);
			} else {
				let summaryType = data.result.summaryList;
				summaryType.forEach(item => {
					let firstName = item['careInfo'] && item['careInfo']['firstName'];
					let lastName = item['careInfo'] && item['careInfo']['lastName'];
					item['docterName'] = firstName + ' ' + lastName;
					if (this.type === 'Dos') {
						if (item['dosDontsId'] && item['dosDontsId'].length > 0) {
							this.covidlist.push(item);
						}
					}
					if (this.type === 'Notes') {
						if (item['notes'] && item['notes'] != "") {
							this.covidlist.push(item);
						}
					}
					if (this.type === 'Meds') {
						if (item['medsId'] && item['medsId'].length > 0) {
							this.covidlist.push(item);
						}
					}
					if (this.type === 'Media') {
						if (item['medialinkId'] && item['medialinkId'].length > 0) {
							this.covidlist.push(item);
						}
					}
				})
			}
		})
	}

	goRedirectPage(history, isSpecial) {
		let url = '';
		this.mediaService.setPatientData(undefined);
		let ids = [];
		if (this.type === 'Dos') {
			history.dosDontsId = (history['dosDontsId'] && history['dosDontsId'].length > 0) ? history['dosDontsId'].split(',') : [];
			ids = ids.concat(history.dosDontsId);
			url = '/app/instructions';
		}

		if (this.type === 'Meds') {
			history.medsId = (history['medsId'] && history['medsId'].length > 0) ? history['medsId'].split(',') : [];
			ids = ids.concat(history.medsId);
			url = '/app/medications';
		}

		if (this.type === 'Media') {
			history.medialinkId = (history['medialinkId'] && history['medialinkId'].length > 0) ? history['medialinkId'].split(',') : [];
			let mediaObject = { medialinkId: history.medialinkId };
			if (isSpecial) {
				mediaObject['specialTagId'] = history.specialTagId;
			} else {
				mediaObject['summaryId'] = history.id;
			}
			ids.push(mediaObject);
			url = '/app/patient-media';
		}

		if (this.type === 'Notes') {
			ids = [history];
			this.enableNotes(history)
			//url = '/app/notes';
		}
		this.mediaService.setPatientData(ids);
		if (url && url != '') {
			this.router.navigate([url]);
		}
	}

	enableNotes(noteData) {
		if (noteData.notesArray && noteData.notesArray[0]) {
			noteData.notes = noteData.notesArray[0];
		}
		// else {
		// 	noteData.notes = '';
		// }
		this.noteData = noteData;
		this.modalRef = this.modalService.show(this.notesDetail, { class: 'br-30' });
	}
}
