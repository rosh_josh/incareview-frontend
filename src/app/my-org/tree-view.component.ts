import { Component, Input, Output, TemplateRef, OnInit, EventEmitter, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MyOrgService } from '../services/my-org.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { isFirstDayOfWeek } from 'ngx-bootstrap';
import { TabDirective } from 'ngx-bootstrap/tabs';

@Component({
  selector: 'tree-view',
  //   directives: [TreeView],
  templateUrl: './tree-view.component.html',
  styleUrls: ['./my-org.component.scss']
})
export class TreeViewComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('createDept') public createDept: TemplateRef<any>;
  @Input() treeData = [];
  @Input() emptyOrg = false;
  @Input() seletedorgNode;
  @Input() mobileView;
  @Input() showChild = false;
  @Input() isOrg = false;
  @Input() showCreate = true;
  changeChildEvent = new EventEmitter();
  childData;
  modalRef: BsModalRef;
  modalRefUser: BsModalRef;
  postType = '';
  postTitle = '';
  createData: any = {};

  orgForm: FormGroup;
  name: FormControl;
  description: FormControl;
  // email: FormControl;
  nodeId: FormControl;
  location: FormControl;
  isLocation = false;
  // permissionId: FormControl;
  submitted = false;
  deleteID;
  deleteOrgName = '';
  updateID;
  childNode;
  deleteTitle = '';
  showDepartment = false;
  showOrganisation = true;
  nodeChildren = [];
  selectedNode = {};

  @Output() hideOrgTittle: EventEmitter<any> = new EventEmitter();
  orgName;

  constructor(private modalService: BsModalService,
    private toastr: ToastrService, private myOrgService: MyOrgService,
    private router: Router) { }

  ngOnInit() {
    this.myOrgService.setOrgData(undefined);
    this.createFormControls();
    this.createForm();
    this.getOrgs();
    console.log('Tree Component');

    this.myOrgService.createOrgEvent.subscribe((data) => {
      if (data === 1) {
        let count = this.modalService.getModalsCount();
        if (count > 1) {
          this.modalService.hide(count);
        }

        if (this.seletedorgNode && this.seletedorgNode.id != undefined) {
          if (this.seletedorgNode.puid === 1) {
            const nodeId = '' + this.seletedorgNode.nodeId;
            this.orgForm.controls['nodeId'].setValue(nodeId);
            this.postType = 'Create';
            this.postTitle = 'Create Department';
            this.isLocation = false;
            this.removeSplice();
            const index = _.findIndex(this.myOrgService.orgchildNode, { id: data.id });
            if (index === -1) {
              this.myOrgService.orgchildNode.push({ id: data.id });
            }
            this.modalRef = this.modalService.show(this.createDept, { class: 'modal-md incareview-model' });

          }

        }
      }
    });
  }
  ngOnChanges() {
    if (this.seletedorgNode && this.seletedorgNode.id != undefined) {
      this.myOrgService.orgchildNode.forEach((childID) => {
        this.treeData.forEach((data) => {
          if (childID.id === data.id) {
            this.showNode(data, true);
          }

        });
      });
    }
    if (this.isOrg) {
      if (this.treeData.length === 1) {
        this.showNode(this.treeData[0], true);
      }

    }

  }

  createFormControls() {

    // this.email = new FormControl('', [
    //   Validators.required,
    //   Validators.email
    // ]);
    this.name = new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]);
    this.nodeId = new FormControl('', [Validators.required]);
    this.description = new FormControl('', [Validators.required]);
    this.location = new FormControl('');

    // this.permissionId = new FormControl(['READ', 'CREATE', 'UPDATE', 'DELETE']);



  }

  createForm() {
    this.orgForm = new FormGroup({
      // email: this.email,
      name: this.name,
      nodeId: this.nodeId,
      description: this.description,
      location: this.location
      // permissionId: this.permissionId
    });

  }
  get f() { return this.orgForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.orgForm.invalid) {
      return;
    }
    let params: any = {};
    params = this.orgForm.value;
    if (this.postType === 'Create') {
      delete params.location;
      this.createOrg();
    } else if (this.postType === 'Update') {
      // Update Org
      if (!this.isLocation) {
        delete params.location;
      }
      this.updateOrg();

    }


  }

  createOrg() {
    this.myOrgService.createChildOrg(this.orgForm.value).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {
        this.modalService.hide(1);
        this.toastr.error(data.result.err);
      } else {
        this.modalRef.hide();
        this.orgForm.reset();
        this.myOrgService.changeOrgEvent.emit();
        this.myOrgService.changeUserEvent.emit();
        this.toastr.success('Department Created successfully');
      }

    });
  }


  updateOrg() {
    this.myOrgService.updateOrg(this.updateID, this.orgForm.value).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {
        this.modalService.hide(1);
        this.toastr.error(data.result.err);
      } else {
        this.modalRef.hide();
        this.orgForm.reset();
        this.myOrgService.changeOrgEvent.emit();
        if (this.isLocation) {
          this.toastr.success('Organisation Updated successfully');
        } else {
          this.toastr.success('Department Updated successfully');
        }

      }

    });
  }
  backToOrg() {
    this.showDepartment = false;
    this.showOrganisation = true;
    this.hideOrgTittle.emit(false);
    this.router.navigate(['/app/settings']);
  }
  showNode(node, type) {
    this.seletedorgNode = this.selectedNode = node;
    this.orgName = node.nodes.name;
    // if (this.myOrgService.orgchildNode.length >= 2) {
    //   const index = _.findIndex(this.myOrgService.orgchildNode, { id: node.puid });
    //   if (index >= 0) {
    //     this.myOrgService.orgchildNode.splice(index + 1, this.myOrgService.orgchildNode.length);
    //   }
    // }
    // if (this.myOrgService.departmentList.length > 0) {
    //   const index = _.findIndex(this.myOrgService.departmentList, { id: node.id });
    //   if (index >= 0) {
    //     this.myOrgService.orgchildNode = [];
    //   }
    // }
    this.treeData.forEach((tree) => {
      tree.isOpen = false;
    });

    if (node.puid === 1) {
      node.isOpen = true;
      this.childData = node.id;
      this.showDepartment = true;
      this.showOrganisation = false;
      this.hideOrgTittle.emit(true);
      // const index = _.findIndex(this.treeData, { id: node.id });
      // if (index >= 0) {
      //   this.myOrgService.orgchildNode = [];
      // }
    }

    if (('isOpen' in node)) {
      node.isOpen = !node.isOpen;
      if (node.isOpen === false) {
        this.childData = '';
      }
      if (node.isOpen === true) {
        this.childData = node.id;

      }
    } else {
      node.isOpen = true;
      this.childData = node.id;
      this.showDepartment = true;
      this.showOrganisation = false;
      this.hideOrgTittle.emit(true);

      if (node.puid > 1) {
        this.showDepartment = false;
        this.showOrganisation = true;
        this.hideOrgTittle.emit(false);
      }
    }
    // if (node.isOpen) {
    //   node.isOpen = !node.isOpen;
    //   if (node.isOpen === false) {
    //     this.childData = '';
    //   }
    //   if (node.isOpen === true) {
    //     this.childData = node.id;

    //   }
    // } else {
    //   console.log(this.showChild);
    //   // if(!this.showChild){
    //   node.isOpen = true;
    //   this.childData = node.id;
    //   this.showDepartment = true;
    //   this.showOrganisation = false;
    //   this.hideOrgTittle.emit(true);

    //   if (node.puid > 1) {
    //     this.showDepartment = false;
    //     this.showOrganisation = true;
    //     this.hideOrgTittle.emit(false);
    //   }
    //   // }

    // }

    if (type) {
      if (this.myOrgService.orgchildNode.length > 0) {

        this.childNode = this.myOrgService.orgchildNode[0] && this.myOrgService.orgchildNode[0]['id'] != undefined ? this.myOrgService.orgchildNode[0]['id'] : undefined;
        // this.myOrgService.orgchildNode.shift();
        if (this.myOrgService.orgchildNode.length === 0) {

        }
      }
      // this.showDepartment = false;
      // this.showOrganisation = true;
      // this.hideOrgTittle.emit(false);
    } else {
      this.removeSplice();
    }
    this.myOrgService.seletedorgNode = node;



    const index = _.findIndex(this.myOrgService.orgchildNode, { id: node.id });
    if (index === -1) {
      this.myOrgService.orgchildNode.push({ id: node.id });
    }
    if (node.puid === 1) {
      this.myOrgService.getOrgInfo(node.nodeId).subscribe((data) => {
        if (data.result.permissionList.length > 0) {
          this.nodeChildren = node.children = data.result.permissionList[0].children;

          // this.myOrgService.seletedorgNode = node;
          if (this.myOrgService.orgID === node.id) {
            this.myOrgService.departmentList = data.result.permissionList[0].children;
          }

        } else {
          // const index = _.findIndex(this.myOrgService.orgchildNode, { id: node.id });
          // if (index === -1) {
          //   this.myOrgService.orgchildNode.push({ id: node.id });
          // }

        }


        // if (this.treeData[0].puid === 1) {
        //   const index = _.findIndex(this.treeData[0].children, { id: this.myOrgService.orgchildNode[0] });
        //   if (index >= 0) {

        //   }

        // }


      });

    }
    if (type) node.isOpen = false;
  }


  removeSplice() {
    if (this.myOrgService.orgchildNode.length > 0) {
      let deleteIndex = -1;
      this.myOrgService.orgchildNode.forEach((nodeID, place) => {
        this.treeData.forEach((childData) => {
          if (nodeID.id === childData.id) {
            deleteIndex = place;
            // console.log(place);
            // console.log(this.myOrgService.orgchildNode.length - place);
            // this.myOrgService.orgchildNode.splice(place, this.myOrgService.orgchildNode.length - place);
          }
        });
      });

      if (deleteIndex >= 0) {
        this.myOrgService.orgchildNode.splice(deleteIndex, this.myOrgService.orgchildNode.length - deleteIndex);
      }


    }
  }

  createDeptModal(createDept: TemplateRef<any>, type, data) {
    this.orgForm.reset();


    if (!this.myOrgService.seletedorgNode) {
      this.myOrgService.seletedorgNode = data;
    }
    if (data.puid === 1) {
      this.myOrgService.seletedorgNode = data;
    }
    this.orgForm.controls['location'].clearValidators();

    const nodeId = '' + data.nodeId;

    this.orgForm.controls['nodeId'].setValue(nodeId);
    this.createData = data;
    if (type === 'edit') {
      this.postType = 'Update';
      this.postTitle = 'Update Department';
      this.updateID = data.nodes.id;
      data.nodes.description ? this.orgForm.controls['description'].setValue(data.nodes.description) : -1;
      data.nodes.name ? this.orgForm.controls['name'].setValue(data.nodes.name) : -1;
      if (data.puid === 1) {
        this.isLocation = true;
        this.orgForm.controls['location'].setValidators([Validators.required]);
        this.postTitle = 'Update Organisation';
        data.nodes.location ? this.orgForm.controls['location'].setValue(data.nodes.location) : -1;
      } else {
        this.isLocation = false;
      }
    } else {
      this.postType = 'Create';
      this.postTitle = 'Create Department';
      this.isLocation = false;
      // data.isOpen = false;
      // this.showNode(data, false);
      // this.myOrgService.seletedorgNode = data;
      this.removeSplice();
      const index = _.findIndex(this.myOrgService.orgchildNode, { id: data.id });
      if (index === -1) {
        this.myOrgService.orgchildNode.push({ id: data.id });
      }

    }
    if (this.mobileView) {
      data['postType'] = this.postType;
      data['postTitle'] = this.postTitle;
      data['isLocation'] = this.isLocation;
      this.myOrgService.setOrgData(data);
      this.router.navigate(['/app/create-dept']);
    } else {
      this.modalRef = this.modalService.show(createDept, { class: 'modal-md incareview-model' });
    }

  }

  closeModal() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
    this.orgForm.reset();
    this.submitted = false;

    // this.myOrgService.seletedorgNode = null;
  }
  // deleteUserModal(deleteuser: TemplateRef<any>, data) {
  //   console.log(data);
  //   if (data.puid === 1) {
  //     this.isLocation = true;
  //     this.deleteTitle = 'Organisation';
  //   } else {
  //     this.isLocation = false;
  //     this.deleteTitle = 'Department';
  //   }
  //   this.deleteID = data.id;
  //   this.deleteOrgName = data.name;
  //   this.modalRefUser = this.modalService.show(deleteuser, { class: 'modal-lg incareview-model' });
  // }
  inviteUserModal(inviteUser: TemplateRef<any>) {
    this.modalRefUser = this.modalService.show(inviteUser, { class: 'modal-lg incareview-model' });
  }
  paymentModal(payment: TemplateRef<any>) {
    this.modalRefUser = this.modalService.show(payment, { class: 'modal-lg incareview-model' });
  }
  deleteConfirmModel(confirmDelete: TemplateRef<any>, data) {
    this.deleteID = data.id;
    this.deleteOrgName = data.name;
    this.modalRef = this.modalService.show(confirmDelete, { class: 'modal-lg incareview-model incare-window' });
    // this.modalRefUser.hide();
    this.removeSplice();
    const index = _.findIndex(this.myOrgService.orgchildNode, { id: data.id });
    if (index === -1) {
      this.myOrgService.orgchildNode.push({ id: data.id });
    }
  }
  createOrgModal(createOrg: TemplateRef<any>) {
    this.modalRef = this.modalService.show(createOrg, { class: 'modal-lg incareview-model org-window' });
  }
  cancelDelete() {
    // this.modalService.hide(1);
    this.modalRef.hide();
  }


  deleteOrg() {
    if (this.deleteID) {
      this.myOrgService.deleteOrg(this.deleteID).subscribe((org) => {
        this.deleteID = '';
        if (org.result.status) {
          this.modalRef.hide();
          document.body.classList.remove('modal-open');
          this.toastr.error(org.result.err);
        } else {

          this.modalRef.hide();
          document.body.classList.remove('modal-open');
          this.toastr.success(this.deleteOrgName + ' deleted successfully');
          this.removeSplice();
          this.myOrgService.changeOrgEvent.emit();
          this.myOrgService.changeUserEvent.emit();
        }

      });
    }

  }

  onSelect(data: TabDirective): void {
    if (data.heading === 'Users') {
      this.myOrgService.selectedTab = 'Users';

    } else {
      this.myOrgService.selectedTab = 'Departments';
    }

  }

  ngOnDestroy() {

    // if (this.showOrganisation) {
    //   this.showOrganisation = false;
    //   this.showDepartment = true;
    // }


  }

  getOrgs() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList && permissionList.length > 0) {
        let mypermission = permissionList[0];
        let myActions = mypermission['myActions'];
        this.showCreate = (myActions['isCreate'] === true ||
          myActions['isCreate'] === 'true') ? true : false;
      } else {
        this.showCreate = false;
      }
    });
  }

}