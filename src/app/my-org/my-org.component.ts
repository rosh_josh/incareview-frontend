import { Component, Input, OnInit, TemplateRef, NgZone, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { TreeModel } from 'ng2-tree';
import { Observable } from 'rxjs/Observable';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TreeViewComponent } from './tree-view.component';
import { MyOrgService, UserService } from '../services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-my-org',
  templateUrl: './my-org.component.html',
  styleUrls: ['./my-org.component.scss'],
  // directives: [TreeView]
})
export class MyOrgComponent implements OnInit, OnDestroy {

  modalRef: BsModalRef;
  modalRefUser: BsModalRef;
  myTree: any = [];
  @Input() mobileView;
  orgForm: FormGroup;
  name: FormControl;
  email: FormControl;
  location: FormControl;
  description: FormControl;
  // nodeId: FormControl;
  // permissionId: FormControl;
  submitted = false;
  showCreateOrg = false;
  hideCreateOrg = false;
  showTreeview = false;
  createOrgData = true;
  searchMobileToggle = true;
  emptyOrg = true;
  hideMail = false;
  seletedNode;
  userType;
  userInfo;
  showOrgContent = false;
  isOrg = false;
  showCreate = true;

  constructor(private modalService: BsModalService,
    private toastr: ToastrService,
    private router: Router,
    private ngZone: NgZone,
    private cdRef: ChangeDetectorRef,
    private myOrgService: MyOrgService, private userService: UserService) {
    window.onresize = (e) => {

      this.ngZone.run(() => {
        if (window.innerWidth < 1025) {
          this.showTreeview = true;
        } else {
          this.showTreeview = false;
        }
      });
    };

    if (window.innerWidth < 1025) {
      this.showTreeview = true;
    } else {
      this.showTreeview = false;
    }
    this.createFormControls();
    this.createForm();

  }

  selectOrgContent(event) {
    this.showOrgContent = event;
    this.cdRef.detectChanges();
  }

  toggleSearchMobile() {
    this.searchMobileToggle = !this.searchMobileToggle;
  }

  createPage() {
    this.orgForm.reset();
    this.showCreateOrg = true;
    this.hideCreateOrg = false;
    if (this.userType !== 1) {
      this.orgForm.controls['email'].setValue(this.userInfo['email']);
      this.orgForm.controls['email'].disable();
    } else {
      this.hideMail = true;
    }
  }

  goBackOrg() {
    this.showCreateOrg = false;
    this.hideCreateOrg = true;
  }

  createOrgModal(createOrg: TemplateRef<any>) {
    this.orgForm.reset();
    this.modalRef = this.modalService.show(createOrg, { class: 'modal-lg incareview-model org-window' });
    if (this.userType !== 1) {
      this.orgForm.controls['email'].setValue(this.userInfo['email']);
      this.orgForm.controls['email'].disable();
    } else {
      this.hideMail = true;
    }
  }

  get f() { return this.orgForm.controls; }

  ngOnInit() {
    this.myOrgService.selectedTab = '';
    let userId = localStorage.getItem('userid');

    if (localStorage.getItem('currentusertoken')) {
      this.getUserPermission();

      this.userService.getUserById(userId).subscribe(userInfo => {
        this.userInfo = userInfo.result;
        this.userType = this.userInfo['userType'];
      })

      this.getOrgs(true);
      this.myOrgService.changeOrgEvent.subscribe((data) => {
        this.getOrgs(false);
      });
    } else {
      this.router.navigate(['/app/login'])
    }

  }


  createFormControls() {

    this.email = new FormControl('123', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.name = new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(/^[a-zA-Z_ ]{1,}$/)
    ]);
    this.location = new FormControl('', [Validators.required]);
    this.description = new FormControl('', [Validators.required]);
    // this.nodeId = new FormControl('1');

    // this.permissionId = new FormControl(['READ', 'CREATE', 'UPDATE', 'DELETE']);



  }

  createForm() {
    this.orgForm = new FormGroup({
      email: this.email,
      name: this.name,
      location: this.location,
      description: this.description,
      // nodeId: this.nodeId,
      // permissionId: this.permissionId
    });




  }


  getOrgs(type) {
    this.myOrgService.getPermission().subscribe((data) => {
      this.hideCreateOrg = true;
      this.myTree = data.result.permissionList;

      if (this.myTree.length > 0) {
        if (type) {
          this.myOrgService.orgID = data.result.permissionList[0].id;
        }
        this.emptyOrg = false;
        this.isOrg = true;
        if (this.myOrgService.seletedorgNode) {
          this.seletedNode = this.myOrgService.seletedorgNode;
        }
      } else {
        this.emptyOrg = true;
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.orgForm.invalid) {
      return;
    }
    this.myOrgService.createOrg(this.orgForm.value).subscribe((data) => {
      this.submitted = false;
      if (this.modalRef) {
        this.modalRef.hide();
      }
      this.close();
      this.toastr.success('Organisation Created successfully');
      this.showCreateOrg = false;
      this.hideCreateOrg = true;
      this.createOrgData = false;
      this.getOrgs(true);
      this.getUserPermission();
      // if(this.showCreateOrg) {
      //   this.router.navigate(['/manage']);
      // }
            
      // this.showTreeview = true;
      
    });
  }

  close() {
    this.orgForm.reset();
    this.submitted = false;
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  createDepartment() {
    if (this.myOrgService.selectedTab === 'Users') {
      this.myOrgService.createOrgEvent.emit(2);
    } else {
      this.myOrgService.createOrgEvent.emit(1);

    }
  }

  clear() {
    //this.seletedorgNode = '';
    this.getOrgs(false);
  }

  ngOnDestroy() {
    // console.log(this.myOrgService.seletedorgNode);
    // if (this.myOrgService.seletedorgNode) {
    //   this.myOrgService.seletedorgNode = null;
    // }

  }

   getUserPermission() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
       if(permissionList && permissionList.length > 0) {
         let mypermission = permissionList[0];
         let myActions = mypermission['myActions'];
         this.showCreate = (myActions['isCreate'] === true ||
          myActions['isCreate'] === 'true') ? true : false;
       } else {
         this.showCreate = false;
       }
    });
  }


}
