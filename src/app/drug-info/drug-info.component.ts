import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MediaService } from '../services';
import * as _ from 'lodash';

@Component({
  selector: 'app-drug-info',
  templateUrl: './drug-info.component.html',
  styleUrls: ['./drug-info.component.scss']
})
export class DrugInfoComponent implements OnInit {
  medViewList: any;

  constructor(private router: Router, private mediaService: MediaService) { }


  ngOnInit() {
    this.medViewList = this.mediaService.getMedData();

    if (this.medViewList && this.medViewList.medicinetime && this.medViewList.medicinetime.indexOf('Others') > -1) {
      this.medViewList.medicinetime = this.medViewList.medicinetime.split('Others')[0];

      if (this.medViewList.medTimeOthers) {
        this.medViewList.medicinetime += this.medViewList.medTimeOthers;
      }
    }

    if (this.medViewList && this.medViewList.foodTime && this.medViewList.foodTime.indexOf('Others') > -1) {
      this.medViewList.foodTime = this.medViewList.foodTime.split('Others')[0];

      if (this.medViewList.medFoodTimeOthers) {
        this.medViewList.foodTime += this.medViewList.medFoodTimeOthers;
      }
    }

    if (localStorage.getItem('userid')) {
      if (!this.medViewList) {
        this.router.navigate(['/app/medications'])
      }
    } else {
      this.router.navigate(['/app/login']);
    }

  }

}
