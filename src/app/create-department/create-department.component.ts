import { Component, Input, Output, TemplateRef, OnInit, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MyOrgService } from '../services';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.scss']
})
export class CreateDepartmentComponent implements OnInit {
  orgForm: FormGroup;
  name: FormControl;
  description: FormControl;
  // email: FormControl;
  nodeId: FormControl;
  location: FormControl;
  isLocation = false;
  // permissionId: FormControl;
  submitted = false;
  nodeInfo;
  postType;
  updateID;
  postTitle;

  constructor(private toastr: ToastrService,
    private myOrgService: MyOrgService, private router: Router) { }

  ngOnInit() {
  	this.createFormControls();
    this.createForm();
  	if(localStorage.getItem('currentusertoken')) {
  	  this.nodeInfo = this.myOrgService.getOrgData();
  	  if(!this.nodeInfo) {
  	  	this.router.navigate(['/app/manage']);
  	  } else {
  	  	this.postType = this.nodeInfo['postType'];
  	  	this.postTitle = this.nodeInfo['postTitle'];
   		this.addControls();
  	  }    
    } else {      
       this.router.navigate(['/app/login'])      
    }  	
  }




    createFormControls() {
	    this.name = new FormControl('', [
	      Validators.required,
	      Validators.minLength(1)
	    ]);
	    this.nodeId = new FormControl('', [Validators.required]);
	    this.description = new FormControl('', [Validators.required]);
	    this.location = new FormControl('');
  	}

    createForm() {
	    this.orgForm = new FormGroup({
	      name: this.name,
	      nodeId: this.nodeId,
	      description: this.description,
	      location: this.location
	    });
	}

	addControls() {
	  let data = this.nodeInfo;
	  const nodeId = '' + this.nodeInfo['nodeId'];
   	  this.orgForm.controls['nodeId'].setValue(nodeId);
        if(this.postType === 'Update') { 
      		this.updateID = data.nodes.id;
            data.nodes.description ? this.orgForm.controls['description'].setValue(data.nodes.description) : -1;
            data.nodes.name ? this.orgForm.controls['name'].setValue(data.nodes.name) : -1;
            data.nodes.location ? this.orgForm.controls['location'].setValue(data.nodes.location) : -1;
        }
	}

  	get f() { return this.orgForm.controls; }

  	onSubmit() {
  		this.submitted = true;
	    if (this.orgForm.invalid) {
	      return;
	    }
	    let params: any = {};
	    params = this.orgForm.value;
	    if (this.postType === 'Create') {
	      delete params.location;
	      this.createDepatmart();
	    } else if (this.postType === 'Update') {
	      this.updateDepatmart();
	    }
  	}

  	createDepatmart() {
	    this.myOrgService.createChildOrg(this.orgForm.value).subscribe((data) => {
	      this.submitted = false;
	      if (data.result.status) {
	        this.toastr.error(data.result.err);
	      } else {
	        this.orgForm.reset();
	        this.toastr.success('Department Created successfully');
	        this.router.navigate(['/app/manage'])
	      }
	    });
	}

	updateDepatmart() {
		this.myOrgService.updateOrg(this.updateID, this.orgForm.value).subscribe((data) => {
	      this.submitted = false;
	      if (data.result.status) {
	        this.toastr.error(data.result.err);
	      } else {
	        this.orgForm.reset();
	        if(this.isLocation){
	          this.toastr.success('Organisation Updated successfully');
	        } else {
	          this.toastr.success('Department Updated successfully');
	        }
	        this.router.navigate(['/app/manage']);
	      }
	    });
	}

}
