import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MediaService } from "../services";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-media-slideshow",
  templateUrl: "./media-slideshow.html",
  styleUrls: ["./media-slideshow.component.scss"],
})
export class MediaSlideshowComponent implements OnInit {
  selectedMediaList: any = [];
  selectedIndex: number = 0;
  constructor(
    private mediaService: MediaService,
    private route: ActivatedRoute,
    private router: Router,
    private domSanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.getMediaList(params.mediaType, params.mediaId);
    });
  }

  getMediaList(mediaType: string, mediaId: number) {
    this.mediaService.getMedia().subscribe((media) => {
      const mediaList: any[] = media.result;
      this.selectedMediaList =
        mediaList.filter((item) => item.mediaType === mediaType)[0] || [];
      this.selectedIndex = this.selectedMediaList.media.findIndex(
        (item) => Number(item.id) === Number(mediaId)
      );
    });
  }

  nextSlide() {
    this.selectedIndex++;
  }

  prevSlide() {
    this.selectedIndex--;
  }

  redirectPage() {
    const redirectUrl = this.route.snapshot.queryParamMap.get("redirectUrl");
    this.router.navigate([redirectUrl]);
  }

  getMediaUrl(mediaInfo) {
    if (mediaInfo.webUrl) {
      return this.domSanitizer.bypassSecurityTrustResourceUrl(mediaInfo.webUrl);
    } else {
      return mediaInfo.fileUrl;
    }
  }
}
