import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskYourDoctorComponent } from './ask-your-doctor.component';

describe('AskYourDoctorComponent', () => {
  let component: AskYourDoctorComponent;
  let fixture: ComponentFixture<AskYourDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskYourDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskYourDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
