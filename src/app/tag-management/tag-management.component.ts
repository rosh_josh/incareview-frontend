import {
  Component,
  OnInit,
  Input,
  OnChanges,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { MediaService, MyOrgService } from "../services";
import { ToastrService } from "ngx-toastr";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AppConfig } from "./../app.config";
import * as _ from "lodash";

@Component({
  selector: "app-tag-management",
  templateUrl: "./tag-management.component.html",
  styleUrls: ["./tag-management.component.scss"],
})
export class TagManagementComponent implements OnInit, OnChanges {
  summaryMaximumLimit = 15000;
  tabs = 1;
  mediaList: any = [];
  dontScroll = false;
  showDoInstruction = true;
  showDontsInstruction = true;
  medsList: any = [];
  nodeID;
  uId;
  DosMessage = "";
  DontsMessage = "";
  DosLists: any = [];
  DontsList: any = [];
  tagInfoList;
  tagData = {};
  tagErrors = {
    name: false,
    summary: false,
    summarylengthexceed: false,
  };
  tagSubmit = false;
  tagSummary = false;
  tagId;
  medicationsOpen = true;

  @Input() changeTagId;
  modalRef: BsModalRef;
  editmodalRef: BsModalRef;
  @ViewChild("addMedicationModal") public addMedicationModal: TemplateRef<any>;
  @ViewChild("createTagMediaModal") public createMediaModal: TemplateRef<any>;
  @ViewChild("mediaFormTagModal") public mediaFormModal: TemplateRef<any>;
  editMediaID = 0;
  showCreate = false;
  format = "";
  deletedId;
  selectMedia = {};
  selectMeds = {};
  dosDonts = {};
  isEdit = false;
  mediaForm: FormGroup;
  title: FormControl;
  description: FormControl;
  mediaType: String = "";

  constructor(
    private mediaService: MediaService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private config: AppConfig,
    private myOrgService: MyOrgService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    let tabId = this.mediaService.getTabId();
    this.getOrganisation();
    this.createFormControls();
    this.createForm();
    if (tabId) {
      this.tabs = tabId;
    } else {
      this.mediaService.setTabId(this.tabs);
    }
    // this.getMediaList();
    // this.getMeds();
    this.getMyorg();
    // this.getDos();
    this.route.params.subscribe((params) => {
      if (params.id > 0) {
        this.viewTagInfo(params.id);
      }
      if (params.id) {
        this.viewTagInfo(params.id);
      }
    });

    this.myOrgService.mediaFormEvent.subscribe((data) => {
      let count = this.modalService.getModalsCount();
      if (data.type === 5 || data.type === 7) {
        this.modalRef && this.modalRef.hide();
        if (this.changeTagId > 0) {
          this.viewTagInfo(this.changeTagId);
        }
      } else if (data.type === 6 || data.type === 1) {
        this.modalRef && this.modalRef.hide();
        this.mediaType = "";
        if (data.mediaType === "flashCard") {
          this.mediaType = data.mediaType;
        }
        if (count === 1)
          this.modalRef = this.modalService.show(this.mediaFormModal, {
            backdrop: "static",
            keyboard: false,
            class: "modal-lg incareview-model org-window",
          });
      }
    });

    this.myOrgService.createTagEvent.subscribe((data) => {
      if (data === 1) {
        this.createTagName("name");
      }
      // else if (data === 2){
      //   this.editTagName('name');
      // } else if (data === 3){
      //   this.validateName('name', 'type')
      // } else if(data === 4){
      //   this. viewTagInfo('id');
      // }
    });
  }

  createForm() {
    this.mediaForm = new FormGroup({
      title: this.title,
      description: this.description,
    });
  }

  createFormControls() {
    this.title = new FormControl("", [Validators.required]);
    this.description = new FormControl("", [Validators.required]);
  }

  ngOnChanges() {
    if (this.changeTagId > 0) {
      this.viewTagInfo(this.changeTagId);
    } else {
      this.tagId = null;
      this.tagInfoList = [];
      this.tagSubmit = false;
      this.tagSummary = false;
      this.medsList = [];
      this.DosLists = [];
      this.DontsList = [];
      this.mediaList = [];
      this.tagData = {};
    }
  }

  viewTagInfo(id) {
    this.mediaService.viewTagByID(id).subscribe((tag) => {
      this.tagInfoList = tag.result.tagInfoList;
      this.tagId = this.tagInfoList["id"];
      this.tagSubmit =
        this.tagInfoList && this.tagInfoList["name"] ? true : false;
      this.tagSummary =
        this.tagInfoList && this.tagInfoList["summary"] ? true : false;
      this.medsList = this.tagInfoList.medsInfo;
      const result = _.chain(this.tagInfoList.dosDontsInfo)
        .groupBy("type")
        .toPairs()
        .map(function (currentItem) {
          return _.zipObject(["type", "list"], currentItem);
        })
        .value();

      const found = result.findIndex((element) => element.type === "Donts");
      const foundDos = result.findIndex((element) => element.type === "Dos");
      this.DosLists = [];
      this.DontsList = [];
      if (foundDos >= 0) {
        this.DosLists = result[foundDos].list;
      }
      if (found >= 0) {
        this.DontsList = result[found].list;
      }

      if (this.tagInfoList.mediaInfo && this.tagInfoList.mediaInfo.length > 0) {
        this.tagInfoList.mediaInfo.forEach((item) => {
          item["format"] = item["keyName"] ? item["keyName"].split(".")[1] : "";
        });
      }

      const medialist = _.chain(this.tagInfoList.mediaInfo)
        .groupBy("type")
        .toPairs()
        .map(function (currentItem) {
          return _.zipObject(["mediaType", "media"], currentItem);
        })
        .value();
      this.mediaList = medialist;
      this.tagData = {
        name: this.tagInfoList["name"],
        id: this.tagInfoList["id"],
        nodeId: this.tagInfoList["nodeId"],
        uId: this.tagInfoList["uId"],
        summary: this.tagInfoList["summary"],
        createdBy: this.tagInfoList["createdBy"],
      };
    });
  }

  openTab(id) {
    if (this.tagData && this.tagData["id"]) {
      this.tabs = id;
      this.mediaService.setTabId(this.tabs);
    }
  }

  addDoInstruction() {
    this.showDoInstruction = true;
    this.showDontsInstruction = true;
  }

  addDontsInstruction() {
    this.showDontsInstruction = true;
    this.showDoInstruction = true;
  }

  closeDos() {
    this.showDontsInstruction = true;
    this.DosMessage = "";
    this.isEdit = false;
    this.dosDonts = {};
  }

  closeDonts() {
    this.showDoInstruction = true;
    this.DontsMessage = "";
    this.isEdit = false;
    this.dosDonts = {};
  }

  closeDosDonts(type, message) {
    if (type === "Donts") {
      this.closeDonts();
    } else {
      this.closeDos();
    }
    this.toastr.success(type + message);
  }

  createDosDonts(params, type) {
    this.mediaService.createDos(params).subscribe(
      (dos: any) => {
        this.viewTagInfo(this.tagId);
        this.closeDosDonts(type, " Created Successfully");
        // this.getDos();
      },
      (err) => {
        if (err.error.message) {
          this.toastr.error(err.error.message);
        } else {
          this.toastr.error(err.error.err.message);
        }
      }
    );
  }

  updateDosDonts(params, type) {
    this.mediaService.UpdateDos(this.dosDonts["id"], params).subscribe(
      (dos: any) => {
        this.viewTagInfo(this.tagId);
        this.closeDosDonts(type, " Updated Successfully");
      },
      (err) => {
        if (err.error.message) {
          this.toastr.error(err.error.message);
        } else {
          this.toastr.error(err.error.err.message);
        }
      }
    );
  }

  addDos(type) {
    if (this.DosMessage || this.DontsMessage) {
      const params: any = {};

      if (type === "Donts") {
        params.message = this.DontsMessage;
      } else {
        params.message = this.DosMessage;
      }
      params.type = type;
      params.nodeId = this.nodeID;
      params.uId = this.uId;
      params.tagId = this.tagId;
      if (this.dosDonts && this.dosDonts["id"]) {
        params.nodeId = this.dosDonts["nodeId"];
        params.uId = this.dosDonts["uId"];
        params.tagId = this.dosDonts["tagId"];
        this.updateDosDonts(params, type);
      } else {
        this.createDosDonts(params, type);
      }
    } else {
      this.toastr.error(type + " Message is Required");
    }
  }

  getMediaList() {
    this.mediaService.getMedia().subscribe((media) => {
      this.mediaList = media.result;
    });
  }

  getMeds() {
    this.mediaService.getMeds().subscribe((meds: any) => {
      this.medsList = meds.result.medList;
    });
  }

  getMyorg() {
    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      if (nodes.result[0]) {
        this.nodeID = nodes.result[0].nodeId;
        this.uId = nodes.result[0].uId;
      }
    });
  }

  getDos() {
    this.mediaService.getDos().subscribe((dos: any) => {
      let dosDontsList = dos.result.DosDontsList;
      const result = _.chain(dosDontsList)
        .groupBy("type")
        .toPairs()
        .map(function (currentItem) {
          return _.zipObject(["type", "list"], currentItem);
        })
        .value();

      const found = result.findIndex((element) => element.type === "Donts");
      const foundDos = result.findIndex((element) => element.type === "Dos");
      this.DosLists = [];
      this.DontsList = [];
      if (foundDos >= 0) {
        this.DosLists = result[foundDos].list;
      }
      if (found >= 0) {
        this.DontsList = result[found].list;
      }
    });
  }

  deleteDos(type, value) {
    this.mediaService.deleteDos(value, this.nodeID).subscribe((dos) => {
      this.viewTagInfo(this.tagId);
      this.modalRef.hide();
      this.toastr.success(type + " Deleted Successfully");
      //this.getDos();
    });
  }

  onHidden(): void {
    console.log("Dropdown is hidden");
    this.dontScroll = false;
  }
  onShown(): void {
    console.log("Dropdown is shown");
    // this.dontScroll = true;
  }
  isOpenChange(): void {
    console.log("Dropdown state is changed");
  }

  /* deleteMedia(media) {
     this.mediaService.deleteMedia(media.id).subscribe((data) => {
       this.toastr.success(data.result.message);
       this.getMediaList();
     });
   }*/

  updateMedia(media) {
    let redirectUrl = this.location.path();
    const removeOldUrl = this.router.url.substring(
      0,
      this.router.url.indexOf("?")
    );
    redirectUrl = removeOldUrl ? removeOldUrl : redirectUrl;
    this.router.navigateByUrl(
      this.router.createUrlTree(["/app/media-edit", media.id], {
        queryParams: {
          tagId: this.tagData && this.tagData["id"],
          redirectUrl,
        },
      })
    );
  }

  resetAccordionState() {
    setTimeout(
      function () {
        this.medicationsOpen = !this.medicationsOpen;
      }.bind(this),
      200
    );
  }

  gotoaddMed() {
    this.resetAccordionState();
    if (window.innerWidth < 673) {
      let redirectUrl = this.location.path();
      const removeOldUrl = this.router.url.substring(
        0,
        this.router.url.indexOf("?")
      );
      redirectUrl = removeOldUrl ? removeOldUrl : redirectUrl;

      this.router.navigateByUrl(
        this.router.createUrlTree(["/app/add-medication"], {
          queryParams: {
            tagId: this.tagData && this.tagData["id"],
            redirectUrl,
          },
        })
      );
    } else {
      this.modalRef = this.modalService.show(this.addMedicationModal, {
        backdrop: "static",
        keyboard: false,
        class: "modal-lg incareview-model org-window",
      });
    }
  }

  editMeds(medList) {
    this.resetAccordionState();
    if (window.innerWidth < 673) {
      const redirectUrl = this.location.path();

      this.router.navigateByUrl(
        this.router.createUrlTree(["/app/edit-medication", medList.id], {
          queryParams: {
            tagId: this.tagData && this.tagData["id"],
            redirectUrl,
          },
        })
      );
    } else {
      this.editMediaID = medList.id;
      this.modalRef = this.modalService.show(this.addMedicationModal, {
        backdrop: "static",
        keyboard: false,
        class: "modal-lg",
      });
    }
  }

  deleteMedsInfo() {
    if (this.selectMeds && this.selectMeds["id"]) {
      this.mediaService
        .deleteMeds(this.selectMeds["id"], this.nodeID)
        .subscribe((data) => {
          this.modalRef.hide();
          this.selectMeds = {};
          this.viewTagInfo(this.tagId);
          if (data.result.message) {
            this.toastr.success(data.result.message);
          } else {
            this.toastr.error(data.result.err);
          }
        });
    }
  }

  cancelMeds() {
    this.selectMeds = {};
    this.modalRef.hide();
  }

  deleteMeds(confirmTagMedDelete: TemplateRef<any>, medList) {
    this.resetAccordionState();
    this.selectMeds = medList;
    this.modalRef = this.modalService.show(confirmTagMedDelete, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  gotoAddMedia() {
    let tagId = this.tagData && this.tagData["id"];

    if (tagId) {
      this.mediaService.setTagId(tagId);
    }
    this.mediaService.setTagType("general");

    if (window.innerWidth < 673) {
      const redirectUrl = this.location.path();

      this.router.navigateByUrl(
        this.router.createUrlTree(["/app/media"], {
          queryParams: {
            tagId,
            redirectUrl,
          },
        })
      );
    } else {
      this.modalRef = this.modalService.show(this.createMediaModal, {
        class: "modal-lg",
      });
    }
  }

  editTagName(type) {
    if (type === "name") this.tagSubmit = !this.tagSubmit;
    if (type === "summary") this.tagSummary = !this.tagSummary;
  }

  updateTag(type) {
    if (this.tagData["id"]) {
      if (!this.tagSubmit && type !== "name")
        this.tagData["name"] = this.tagInfoList["name"];
      if (!this.tagSummary && type !== "summary")
        this.tagData["summary"] = this.tagInfoList["summary"];

      this.mediaService
        .updateTag(this.tagData["id"], this.tagData)
        .subscribe((data) => {
          this.toastr.success("Tag Updated Successfully");
          this.viewTagInfo(this.tagData["id"]);
        });
    }
  }

  validateName(name, type) {
    this.tagErrors[type] = name === "" ? true : false;
    if (type === "summary") {
      if (name && name.length > this.summaryMaximumLimit) {
        this.tagErrors["summarylengthexceed"] = true;
      } else {
        this.tagErrors["summarylengthexceed"] = false;
      }
    }
  }

  isCheckAlpha(keyCode) {
    return (
      (keyCode >= 65 && keyCode <= 90) ||
      (keyCode >= 97 && keyCode <= 122) ||
      keyCode == 8 ||
      keyCode == 32 ||
      keyCode == 190
    );
  }

  createTagName(type, optionalfield = "") {
    if (
      this.tagData["summary"] &&
      this.tagData["summary"].length > this.summaryMaximumLimit
    ) {
      this.tagErrors["summarylengthexceed"] = true;
    }
    if (!this.tagData["name"]) {
      this.tagErrors["name"] = true;
    } else if (!this.tagErrors[type] && !this.tagData[type]) {
      this.tagErrors[type] = true;
    } else if (
      !this.tagErrors[type] &&
      !this.tagData["id"] &&
      (optionalfield === "" || !this.tagErrors[optionalfield])
    ) {
      this.tagData["nodeId"] = this.nodeID;
      this.tagData["uId"] = this.uId;
      this.mediaService.createTag(this.tagData).subscribe((data) => {
        if (data["result"]["status"] === 400) {
          this.toastr.error(data["result"]["err"]);
        } else {
          this.toastr.success("Tag Created Successfully");
          this.tagData = data["result"]["summary"];
          this.router.navigate(["/app/tag-management/" + this.tagData["id"]]);
          //this.viewTagInfo(this.tagData['id']);
        }
      });
    } else if (
      !this.tagErrors[type] &&
      this.tagData["id"] &&
      (optionalfield === "" || !this.tagErrors[optionalfield])
    ) {
      this.updateTag(type);
    } else {
      return this.tagErrors;
    }
  }

  redirectPage(id) {
    const redirectUrl = this.location.path();
    if (this.tagId) {
      this.router.navigateByUrl(
        this.router.createUrlTree(["/app/media-ivideo/" + id], {
          queryParams: { tagId: this.tagId, redirectUrl },
        })
      );
    } else {
      this.router.navigate(["/app/media-ivideo/" + id]);
    }
  }

  getOrganisation() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList.length === 0) {
        this.showCreate = false;
      } else if (permissionList.length > 0) {
        let actions = permissionList[0].myActions;
        if (actions["isCreate"] === true || actions["isCreate"] === "true") {
          this.showCreate = true;
        }
      }
    });
  }

  keyDownFunction(event, type) {
    if (event.keyCode == 13) {
      if (this.DosMessage || this.DontsMessage) {
        this.addDos(type);
      }
    }
  }

  openDosDontRemoveModal(modalid, type, id) {
    this.deletedId = id;
    this.format = type;
    this.modalRef = this.modalService.show(modalid, {
      backdrop: "static",
      keyboard: false,
      class: "modal-lg incare-window br-30 incareview-model",
    });
  }

  deleteMedia(confirmMediaDelete: TemplateRef<any>, media) {
    this.selectMedia = media;
    this.modalRef = this.modalService.show(confirmMediaDelete, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  deleteMediaInfo() {
    if (this.selectMedia && this.selectMedia["id"]) {
      this.mediaService
        .deleteMedia(this.selectMedia["id"])
        .subscribe((data) => {
          if (data.result.message) {
            this.toastr.success(data.result.message);
          } else {
            this.toastr.error(data.result.err);
          }
          this.modalRef.hide();
          this.selectMedia = {};
          this.viewTagInfo(this.changeTagId);
        });
    }
  }

  editMediaInfo() {
    if (this.mediaForm.valid) {
      let updatedMedia = {
        name: this.mediaForm.get("title").value,
        description: this.mediaForm.get("description").value,
        nodeId: this.selectMedia["nodeId"],
        uId: this.selectMedia["uId"],
        isPersonal: this.selectMedia["isPersonal"],
      };
      this.mediaService
        .updateMedia(this.selectMedia["id"], updatedMedia)
        .subscribe((data) => {
          if (data.result.message) {
            this.editmodalRef.hide();
            this.selectMedia = {};
            this.viewTagInfo(this.changeTagId);
            this.toastr.success(data.result.message);
          } else {
            this.toastr.error(data.result.err);
          }
        });
    }
  }

  editMedia(editMedia: TemplateRef<any>, media) {
    this.mediaForm.reset();
    this.selectMedia = media;
    this.mediaForm.get("title").setValue(media.name);
    this.mediaForm.get("description").setValue(media.description);
    this.editmodalRef = this.modalService.show(editMedia, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  closeMediaEdit() {
    this.editmodalRef.hide();
  }

  cancelDelete() {
    this.selectMedia = {};
    this.modalRef.hide();
  }

  editDosDonts(type, value) {
    this.dosDonts = value;
    this.isEdit = true;
    if (type === "Dos") {
      this.showDontsInstruction = false;
      this.DosMessage = value.message;
    }
    if (type === "Donts") {
      this.showDoInstruction = false;
      this.DontsMessage = value.message;
    }
  }

  getInnerHTML(val) {
    val = val.toString();
    return val.replace(/(<([^>]+)>)/gi, "");
  }

  deleteTag(confirmTagDelete: TemplateRef<any>, taglist) {
    this.modalRef = this.modalService.show(confirmTagDelete, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  deleteTagInfo() {
    if (this.tagInfoList && this.tagInfoList["id"]) {
      this.mediaService
        .deleteTag(this.tagInfoList["nodeId"], this.tagInfoList["id"])
        .subscribe((data) => {
          if (data.result.message) {
            this.toastr.success(data.result.message);
            this.router.navigate(["/app/tags"]);
          } else {
            this.toastr.error(data.result.err);
          }
          this.modalRef.hide();
        });
    }
  }
}
