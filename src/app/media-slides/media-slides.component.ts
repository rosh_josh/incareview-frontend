import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { MediaService, MyOrgService } from "../services";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import {
  BsModalService,
  BsModalRef,
  ModalDirective,
} from "ngx-bootstrap/modal";
import { Location } from "@angular/common";
import * as _ from "lodash";

@Component({
  selector: "app-media-slides",
  templateUrl: "./media-slides.component.html",
  styleUrls: ["./media-slides.component.scss"],
})
export class MediaSlidesComponent implements OnInit {
  @ViewChild("mediaFormModal") public mediaFormModal: TemplateRef<any>;
  @ViewChild("updateMedia") public updateMedia: TemplateRef<any>;
  dontScroll = false;
  mediaList: any = [];
  isSearch = false;
  modalRef: BsModalRef;
  editmodalRef: BsModalRef;
  showMedia = false;
  userId;
  userType: any = localStorage.getItem("userType");
  isPatient = false;
  showCreate = false;
  selectMedia = {};
  redirectUrl = "";
  mediaForm: FormGroup;
  title: FormControl;
  description: FormControl;
  mediaType: String = "";

  constructor(
    private mediaService: MediaService,
    private router: Router,
    private location: Location,
    private toastr: ToastrService,
    private myOrgService: MyOrgService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.userId = localStorage.getItem("userid");
    this.getOrganisation();
    this.createFormControls();
    this.createForm();
    if (this.router.url === "/app/patient-media") {
      let medias = this.mediaService.getPatientData();
      if (medias && medias[0]) {
        this.redirectUrl = "/app/patient-summary";
        this.isPatient = true;
        this.getPatientMedia(
          medias[0]["specialTagId"]
            ? medias[0]["specialTagId"]
            : medias[0]["summaryId"],
          medias[0]["specialTagId"] ? true : false
        );
      } else {
        this.router.navigate(["/app/patient-summary"]);
      }
    } else {
      this.redirectUrl = "/app/menu";
      this.isPatient = false;
      this.getMediaList();
    }

    // this.getMediaList();
    this.myOrgService.mediaFormEvent.subscribe((socketDt) => {
      let count = this.modalService.getModalsCount();
      this.mediaType = "";
      if (socketDt.type === 1) {
        this.modalRef && this.modalRef.hide();
        if (socketDt.mediaType === "flashCard") {
          this.mediaType = socketDt.mediaType;
          this.modalRef = this.modalService.show(this.mediaFormModal, {
            backdrop: "static",
            keyboard: false,
            class: "modal-lg incareview-model org-window",
          });
        } else if (count === 1) {
          this.modalRef = this.modalService.show(this.mediaFormModal, {
            backdrop: "static",
            keyboard: false,
            class: "modal-lg incareview-model org-window",
          });
        }
      } else if (socketDt.type === 2) {
        if (this.modalRef) {
          this.modalRef.hide();
          if (count > 1) {
            this.modalService.hide(count);
          }
          this.getMediaList();
        }
      }
    });
  }

  createForm() {
    this.mediaForm = new FormGroup({
      title: this.title,
      description: this.description,
    });
  }

  createFormControls() {
    this.title = new FormControl("", [Validators.required]);
    this.description = new FormControl("", [Validators.required]);
  }

  getPatientMedia(summaryId, isSpeical) {
    this.mediaService
      .listPatientMedia(this.userId, summaryId, isSpeical)
      .subscribe((media) => {
        let mediaList = media.result.mediaLinkList;
        let mediaFile = [];
        if (mediaList && mediaList.length > 0) {
          mediaList.forEach((item) => {
            if (item["mediaInfo"]) mediaFile.push(item["mediaInfo"]);
            let format =
              item["mediaInfo"] && item["mediaInfo"]["keyName"]
                ? item["mediaInfo"]["keyName"].split(".")[1]
                : null;
            item["mediaInfo"]["format"] = format;
          });
          if (mediaFile && mediaFile.length > 0)
            this.mediaList = _.chain(mediaFile)
              .groupBy("type")
              .map((value, key) => ({ mediaType: key, media: value }))
              .value();
        } else {
          //this.router.navigate(['/app/menu']);
        }
        //this.mediaList = media.result;
      });
  }

  closeMediaEdit() {
    this.editmodalRef.hide();
  }

  removeTagId() {
    this.mediaService.setTagId(null);
  }

  createMedia(createMediaModal: TemplateRef<any>) {
    this.mediaService.setTagId(null);
    this.modalRef = this.modalService.show(createMediaModal, {
      class: "modal-lg incareview-model org-window",
    });
  }

  // showMediaForm(mediaFormModal: TemplateRef<any>){
  //   this.modalRef = this.modalService.show(mediaFormModal, { class: 'modal-lg incareview-model org-window' });
  // }

  getMediaList() {
    this.mediaService.getMedia().subscribe((media) => {
      this.mediaList = media.result;

      if (this.mediaList && this.mediaList.length > 0) {
        this.mediaList.forEach((item) => {
          item.media.forEach((key) => {
            let format = key["keyName"] ? key["keyName"].split(".")[1] : "";
            key["format"] = format;
          });
        });
      }
    });
  }

  onHidden(): void {
    console.log("Dropdown is hidden");
    this.dontScroll = false;
  }
  onShown(): void {
    console.log("Dropdown is shown");
    // this.dontScroll = true;
  }
  isOpenChange(): void {
    console.log("Dropdown state is changed");
  }

  editMedia(editMedia: TemplateRef<any>, media) {
    this.mediaForm.reset();
    this.selectMedia = media;
    this.mediaForm.get("title").setValue(media.name);
    this.mediaForm.get("description").setValue(media.description);
    this.editmodalRef = this.modalService.show(editMedia, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  editMediaInfo() {
    if (this.mediaForm.valid) {
      let updatedMedia = {
        name: this.mediaForm.get("title").value,
        description: this.mediaForm.get("description").value,
        nodeId: this.selectMedia["nodeId"],
        uId: this.selectMedia["uId"],
        isPersonal: this.selectMedia["isPersonal"],
      };
      this.mediaService
        .updateMedia(this.selectMedia["id"], updatedMedia)
        .subscribe((data) => {
          if (data.result.message) {
            this.editmodalRef.hide();
            this.selectMedia = {};
            this.getMediaList();
            this.toastr.success(data.result.message);
          } else {
            this.toastr.error(data.result.err);
          }
        });
    }
  }

  deleteMedia(confirmMediaDelete: TemplateRef<any>, media) {
    this.selectMedia = media;
    this.modalRef = this.modalService.show(confirmMediaDelete, {
      class: "modal-lg incareview-model incare-window",
    });
  }

  deleteMediaInfo() {
    if (this.selectMedia && this.selectMedia["id"]) {
      this.mediaService
        .deleteMedia(this.selectMedia["id"])
        .subscribe((data) => {
          if (data.result.message) {
            this.toastr.success(data.result.message);
          } else {
            this.toastr.error(data.result.err);
          }
          this.modalRef.hide();
          this.selectMedia = {};
          this.getMediaList();
        });
    }
  }

  cancelDelete() {
    this.selectMedia = {};
    this.modalRef.hide();
  }

  updateMediaForm(media) {
    this.router.navigate(["/app/media-edit", media.id]);
  }

  scrollHandler(event) {
    //Do Nothing
  }

  searchMedia(search: string) {
    this.mediaService.searchMedia(search).subscribe((media) => {
      this.mediaList = media.result;
    });
  }

  goTomediaView(url: string, id: number, mediaType: string) {
    this.router.navigateByUrl(
      this.router.createUrlTree([`/app/media-slideshow/${mediaType}/${id}`], {
        queryParams: {
          redirectUrl: this.router.url,
        },
      })
    );
    // this.router.navigateByUrl(
    //   this.router.createUrlTree(
    //     [url + '/' + id], {
    //     queryParams: {
    //       redirectUrl: this.router.url
    //     }
    //   }
    //   )
    // );
  }

  getOrganisation() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList.length === 0) {
        this.showCreate = false;
      } else if (permissionList.length > 0) {
        let actions = permissionList[0].myActions;
        if (actions["isCreate"] === true || actions["isCreate"] === "true") {
          this.showCreate = true;
        }
      }
    });
  }

  navigateUrl() {
    this.router.navigate([this.redirectUrl]);
  }
}
