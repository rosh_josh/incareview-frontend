import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaSlidesComponent } from './media-slides.component';

describe('MediaSlidesComponent', () => {
  let component: MediaSlidesComponent;
  let fixture: ComponentFixture<MediaSlidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSlidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSlidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
