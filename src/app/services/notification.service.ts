import { Injectable , EventEmitter, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from './../app.config';
@Injectable({
  providedIn: 'root'
})
export class NotificationService implements OnDestroy {

  changemenuEvent = new EventEmitter();
  // resourceUrl = 'https://frontend.incareview.com:9006/api/v1/';
  private socket;
   userId = localStorage.getItem('userid');
  constructor(private http: HttpClient, private config: AppConfig) {
    // this.socket = io('http://frontend.incareview.com:9006');

    //   this.socket = io('https://incareview.com:9006/api/v1/', {
    //     secure: true // for SSL
    // });
    console.log(this.config.socketUrl)
    this.socket = io.connect(this.config.socketUrl);
    this.socket.on('connect', (data) => {
      console.log('Socket Connected');
      this.socket.emit('register', this.userId);
      console.log('Socket User Registered');
    });

    //  this.socket.on('notification:' + this.userId, (user) => {
    //     console.log(user);
    //   });
  }

  // List Notification
  listNotification(page, limit) {
    return this.http.get<any>(this.config.resourceUrl + `notification/list?page=${page}&limit=${limit}`)
      .map(res => res);
  }

  // Update Notification
  updateNotification(id) {
    return this.http.get<any>(this.config.resourceUrl + `notification/update/${id}`)
      .map(res => res);
  }

  // Delete Notification
  deleteNotification(notificationId) {
    return this.http.request<any>('delete', this.config.resourceUrl + `notification/remove`, { body: notificationId })
      .map(res => res);
  }

  // View By Notificaation Id
  viewNotificationbyID(id) {
    return this.http.get<any>(this.config.resourceUrl + `notification/view/${id}`)
      .map(res => res);
  }

  public sendMessage(event, message) {
    this.socket.emit(event, message);
  }

  public getMessages = (event) => {
    return Observable.create((observer) => {
      this.socket.on(event, (message) => {
        observer.next(message);
      });
    });
  }

 ngOnDestroy() {
  this.socket.disconnect();
 }
}
