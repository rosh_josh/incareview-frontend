import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpInterceptor } from '@angular/common/http';
import { AppConfig } from './../app.config';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class MyOrgService {
  changeOrgEvent = new EventEmitter();
  changeUserEvent = new EventEmitter();
  multiDeleteEvent = new EventEmitter();
  mediaFormEvent = new EventEmitter();
  createOrgEvent = new EventEmitter();
  createTagEvent = new EventEmitter();
  seletedNode;
  childNode = [];
  seletedorgNode;
  orgchildNode = [];
  orgID = 0;
  departmentList = [];
  selectedTab = 'Departments';

  list: any;

  public setOrgData(data) {
    this.list = data;
  }

  public getOrgData() {
    return this.list;
  }


  constructor(private http: HttpClient, private config: AppConfig) {

  }


  // Create Org
  createOrg(data) {
    return this.http.post<any>(this.config.resourceUrl + 'organisation/createorg', data)
      .map(res => res);
  }

  // Create Child Org
  createChildOrg(data) {
    return this.http.post<any>(this.config.resourceUrl + 'organisation/create', data)
      .map(res => res);
  }

  // Update Org
  updateOrg(id, data) {
    return this.http.put<any>(this.config.resourceUrl + `organisation/updateorg/${id}`, data)
      .map(res => res);
  }

  // Invite User
  inviteUser(data) {
    return this.http.post<any>(this.config.resourceUrl + 'organisation/invite_user', data)
      .map(res => res);
  }

  // Update Permission
  updatePermission(id, data) {
    return this.http.put<any>(this.config.resourceUrl + `role/update_permission/${id}`, data)
      .map(res => res);
  }

  // Delete User
  deleteUser(data) {
    return this.http.post<any>(this.config.resourceUrl + `role/delete_permission`, data)
      .map(res => res);
  }

  // Get Specific Org Info
  getOrgInfo(nodeID) {
    return this.http.get<any>(this.config.resourceUrl + `organisation/list?nodeId=${nodeID}`)
      .map(res => res);
  }

  // Delete Org
  deleteOrg(nodeID) {
    return this.http.delete<any>(this.config.resourceUrl + `organisation/delete/${nodeID}`)
      .map(res => res);
  }

  // Permission List
  getPermission() {
    return this.http.get<any>(this.config.resourceUrl + 'role/list_permission')
      .map(res => res);
  }

  // UserPermission List
  getUserPermission() {
    return this.http.get<any>(this.config.resourceUrl + 'organisation/list_permission')
      .map(res => res);
  }

  // View Org by ID
  vieworgbyId(u_id) {
    return this.http.get<any>(this.config.resourceUrl + `organisation/view-org/${u_id}`)
      .map(res => res);
  }

  // Get My Org
  getMyOrg() {
    return this.http.get<any>(this.config.resourceUrl + `organisation/care_team`)
      .map(res => res);
  }

  // Care Team DropDown List
  careTeamDropdown() {
    return this.http.get<any>(this.config.resourceUrl + `organisation/care_dropdown`)
      .map(res => res);
  }

  // Care Team User List
  careUserList(nodeID) {
    return this.http.get<any>(this.config.resourceUrl + `organisation/care_team_user/${nodeID}`)
      .map(res => res);
  }
}
