import { Injectable } from '@angular/core';
import {
    HttpErrorResponse,
    HttpResponse,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { LoaderService } from './loader.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    private requests: HttpRequest<any>[] = [];

    constructor(private loaderService: LoaderService, public router: Router) { }

    removeRequest(req: HttpRequest<any>) {
        // console.log(req);
        // console.log('this.requests.......', this.requests);
        // const i = this.requests.indexOf(req);
        // if (i >= 0) {
        //     this.requests.splice(i, 1);
        // }
        // console.log('after deletion this.requests.......', this.requests);
        // this.loaderService.isLoading.next(this.requests.length > 0);
        this.loaderService.isLoading.next(false);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.requests.push(req);
        this.loaderService.isLoading.next(true);

        // add authorization header with jwt token if available

        const currentUser = localStorage.getItem('currentusertoken');
        if (currentUser) {

            req = req.clone({

                setHeaders: {

                    Authorization: `Bearer ${currentUser}`

                }
                /* headers: req.headers.set('Cache-Control', 'no-cache')
                      .set('Pragma', 'no-cache'),*/

            });

        }

        return Observable.create(observer => {
            const subscription = next.handle(req)
                .subscribe(
                    event => {
                        if (event instanceof HttpResponse) {
                            this.removeRequest(req);
                            observer.next(event);
                        }
                    },
                    error => {
                        let data = {};
                        data = {
                            reason: error && error.error.reason ? error.error.reason : '',
                            status: error.status
                        };
                        // console.log(error);
                        if (error.error.err === 'Un-authorized') {
                            localStorage.clear();
                            this.router.navigate(['/app/login']);
                        }
                        this.removeRequest(req);
                        observer.error(error);
                    },
                    () => {
                        this.removeRequest(req);
                        observer.complete();
                    });
            // remove request from queue when cancelled
            return () => {
                this.removeRequest(req);
                subscription.unsubscribe();
            };
        });
    }
}