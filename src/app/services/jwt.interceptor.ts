import { Injectable } from '@angular/core';

import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()

export class JwtInterceptor implements HttpInterceptor {

    constructor(public router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // add authorization header with jwt token if available

        const currentUser = localStorage.getItem('currentusertoken');
        if (currentUser) {

            request = request.clone({

                setHeaders: {

                    Authorization: `Bearer ${currentUser}`

                }
                /* headers: request.headers.set('Cache-Control', 'no-cache')
                      .set('Pragma', 'no-cache'),*/

            });

        }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                // if (event instanceof HttpResponse) {
                //     console.log('event--->>>', event);
                //     // this.errorDialogService.openDialog(event);
                // }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                // console.log(error);
                if (error.error.err === 'Un-authorized') {
                    localStorage.clear();
                    this.router.navigate(['/app/login']);
                }
                return throwError(error);
            }));

    }

}