import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './../app.config';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private config: AppConfig, private http: HttpClient) {

  }

  // Get User
  getUser() {
    return this.http.get<any>(this.config.resourceUrl + `users/view/`)
      .map(res => res);
  }

  // Delete Patient
  deletePatient(userID) {
    return this.http.delete<any>(this.config.resourceUrl + `users/deletepatient/${userID}`)
      .map(res => res);
  }

  // Get User By ID
  getUserById(userID) {
    return this.http.get<any>(this.config.resourceUrl + `users/view/${userID}`)
      .map(res => res);
  }

  // Update Profile
  updateProfile(data) {
    return this.http.put<any>(this.config.resourceUrl + 'users/update_profile', data)
      .map(res => res);
  }

  // Change Password
  changePassword(data) {
    return this.http.post<any>(this.config.resourceUrl + 'users/change-pass', data)
      .map(res => res);
  }

  // Patient User List 
  patientList(search) {
    return this.http.get<any>(this.config.resourceUrl + `users/list?search=` + encodeURIComponent(search))
      .map(res => res);
  }

  // CEO User List 
  ceoList(search) {
    return this.http.get<any>(this.config.resourceUrl + `users/list?search=${search}`)
      .map(res => res);
  }

  getUsersList() {
    return this.http.get<any>(this.config.resourceUrl + `users/allUsers`)
      .map(res => res);
  }

  logout() {
    return this.http.get<any>(this.config.resourceUrl + `users/logout`).map(res => res);
  }
}
