import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import "rxjs/Rx";
import { map } from 'rxjs/operators';
import { AppConfig } from './../app.config'
import { Http, RequestOptions, Response, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  list:any = {};

  public setData(data) {
    this.list = data;
  }

  public getData() {
    return this.list;
  }

  constructor(private http: HttpClient, private config: AppConfig) { }

  isAuthenticated(): boolean {

   
    // return sessionStorage.getItem('currentusertoken') != null || localStorage.getItem('currentusertoken') != null;
    if(localStorage.getItem('currentusertoken')) {
    
      let userInfo: any = {};
      localStorage.getItem('userInfo') ?  userInfo = JSON.parse(localStorage.getItem('userInfo')) :-1;
     if(userInfo.isForce === 0) {
       return true;
      
      } else {
        return false;
      }
    } else {
      return false;
    }
    
  }

  //Oauth - code
  loginCode(loginData) {    
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.post<any>(this.config.resourceUrl + "oauth/code", loginData, {headers: header})
    .map(res => res );
  } 

  //OTP - code
  loginOtp(loginData) {    
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.post<any>(this.config.resourceUrl + "oauth/otp_code", loginData, {headers: header})
    .map(res => res );
  } 

  //Login
  loginAuth(data){
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.post<any>(this.config.resourceUrl + "oauth/token", data, {headers: header})
    .map(res => res );
  }

  //Forgot Password
  forgotPassword(email, countrycode){
    return this.http.get<any>(this.config.resourceUrl + 'users/forget-pass?email=' + email+'&country_Code='+countrycode)
    .map(res => res );
  }

  //Email validation for signup
  validEmail(email){
    return this.http.get<any>(this.config.resourceUrl + 'users/validemail?email=' + email+'&is_Self=true')
    .map(res => res );
  }

    // Add Patient
    addPatient(data){
      return this.http.post<any>(this.config.resourceUrl + `users/add_patient`, data);
    }

  // Add Ceo
    addCeo(data){
      return this.http.post<any>(this.config.resourceUrl + `users/add_ceo`, data);
    }
  
  
  //Activate User
  activateUser(userId, permissionId){
    console.log(userId)
    console.log(permissionId)
    let url = (permissionId) ? this.config.resourceUrl + 'users/activate/'+userId+'?permissionId='+permissionId :
    this.config.resourceUrl + 'users/activate/'+userId;
    return this.http.get<any>(url)
    .map(res => res );
  }

  //Signup
  // signup(registerData){
  //   let header = new HttpHeaders();
  //   header.append("Content-Type", "application/json");
  //   return this.http.post<any>(this.config.resourceUrl + "users/create", registerData, {headers: header})
  //   .map(res => res );
  // }
  signup(registerData){
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.put<any>(this.config.resourceUrl + "users/update", registerData, {headers: header})
    .map(res => res );
  }

  //verify Forget Token
  verifyToken(code){
    return this.http.get<any>(this.config.resourceUrl + 'users/verifyToken?code=' + code)
    .map(res => res );
  }

  //Reset Password
  resetPassword(data) {    
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.post<any>(this.config.resourceUrl + "users/reset-pass", data, {headers: header})
    .map(res => res );
  } 

  //Social Login
  socialLogin(data){
    let header = new HttpHeaders();
    header.append("Content-Type", "application/json");
    return this.http.post<any>(this.config.resourceUrl + "oauth/social_login", data, {headers: header})
    .map(res => res );
  }

  public getCountryCodes(): Observable<any> {
        return this.http.get('./assets/countrycode.json');
  }
}
