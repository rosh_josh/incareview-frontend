import { Injectable, EventEmitter } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import * as uuid from 'uuid';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class S3UploadService {

  public progress: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  uploadfile(file) {
    let AWSConfig = environment.AWS;
    const uniquename = uuid.v4();

    const fileExtension = file.name.replace(/^.*\./, '');
    const filename: string = 'file/' + uniquename + '.' + fileExtension;

    AWS.config.update({
      region: AWSConfig.Region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: AWSConfig.IdentityPoolId
      })
    });

    const S3Client = new S3();

    const params = {
      Bucket: AWSConfig.Bucket,
      Key: filename,
      Body: file,
      ACL: 'public-read',
      ContentType: file.type
    };

    return new Promise((res, rej) => {
      S3Client.upload(params, function (err: any, data: any) {
        if (err) {
          console.log(err);
          return rej(err);
        }

        return res(data);

      }).on('httpUploadProgress', (event) => { // change here
        const progress = Math.round((event.loaded / event.total) * 100);
        // console.log("progress", progress);
        this.progress.emit(progress);
        // return event;
        // return res(event);
      });

    });



  }
}
