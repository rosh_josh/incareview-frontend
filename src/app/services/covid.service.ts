
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { AppConfig } from './../app.config';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CovidService {

  fileData: any = null;
  imageURL: any;
  selectedTag = new Subject<any>();
  tabId = 1;
  patientData: any;
  medData: any;
  type;


  constructor(private http: HttpClient, private config: AppConfig) {
    this.imageURL = environment.AWS.CloudFrontUrl;
  }

  public setTabId(id) {
    this.tabId = id;
  }
  public getTabId() {
    return this.tabId;

  }

  public setPatientData(data) {
    this.patientData = data;
  }
  public getPatientData() {
    return this.patientData;
  }

  public setType(data) {
    this.type = data;
  }
  public getType() {
    return this.type;
  }

  public setMedData(data) {
    this.medData = data;
  }
  public getMedData() {
    return this.medData;
  }

  // List Media
  getMedia() {
    return this.http.get<any>(this.config.resourceUrl + `media/list`)
      .map(res => res);
  }
  //get dynamic name
  getdynamicname() {
    return this.http.get<any>(this.config.resourceUrl + `specialcategory/list`)
      .map(res => res);
  }

  // Search Media
  searchMedia(search) {
    return this.http.get<any>(this.config.resourceUrl + `media/list?search=${search}`)
      .map(res => res);
  }

  // Create Media
  createMedia(data) {
    return this.http.post<any>(this.config.resourceUrl + 'media/create', data)
      .map(res => res);
  }

  // Update Media
  updateMedia(mediaID, media) {
    return this.http.put<any>(this.config.resourceUrl + `media/update/${mediaID}`, media)
      .map(res => res);
  }

  // Delete Media
  deleteMedia(mediaID) {
    return this.http.request<any>('delete', this.config.resourceUrl + `media/delete?mediaId=${mediaID}`)
      .map(res => res);
  }

  // Media ViewById
  mediaViewbyID(mediaID) {
    return this.http.get<any>(this.config.resourceUrl + `media/viewby_Id/${mediaID}`)
      .map(res => res);
  }

  // Media Tag List
  getMediaTag() {
    return this.http.get<any>(this.config.resourceUrl + `media/tag_list`)
      .map(res => res);
  }

  // Meds
  //  Create Meds
  createMeds(meds) {
    return this.http.post<any>(this.config.resourceUrl + `meds/create`, meds)
      .map(res => res);
  }

  updateMeds(medID, meds) {
    return this.http.put<any>(this.config.resourceUrl + `meds/update/${medID}`, meds)
      .map(res => res);
  }

  // List Meds
  getMeds() {
    return this.http.get<any>(this.config.resourceUrl + `meds/list`)
      .map(res => res);
  }

  // View Meds By ID
  viewMedByID(medID) {
    return this.http.get<any>(this.config.resourceUrl + `meds/viewby_id/${medID}`)
      .map(res => res);
  }

  //  Delete Meds
  deleteMeds(medsId, nodeId) {
    return this.http.request<any>('delete', this.config.resourceUrl + `meds/delete?medsId=${medsId}&nodeId=${nodeId}`)
      .map(res => res);
  }

  // Meds End

  //  Create Dos
  createDos(dos) {
    return this.http.post<any>(this.config.resourceUrl + `dosdonts/create`, dos)
      .map(res => res);
  }

  // Update Dos
  UpdateDos(dosID, dos) {
    return this.http.put<any>(this.config.resourceUrl + `dosdonts/update/${dosID}`, dos)
      .map(res => res);
  }

  // List Dos
  getDos() {
    return this.http.get<any>(this.config.resourceUrl + `dosdonts/list`)
      .map(res => res);
  }

  // View Dos By ID
  viewDosByID(dosID, nodeID) {
    return this.http.get<any>(this.config.resourceUrl + `dosdonts/viewby_id/${dosID}?nodeId=${nodeID}`)
      .map(res => res);
  }

  //  Delete Dos
  deleteDos(dosID, nodeID) {
    return this.http.request<any>('delete', this.config.resourceUrl + `dosdonts/delete?id=${dosID}&nodeId=${nodeID}`)
      .map(res => res);
  }

  // Do's & Dont's End

  // Patient Media Start

  // Get Media
  getPatientMedia(patientID) {
    return this.http.get<any>(this.config.resourceUrl + `patient/list_media?patientId=${patientID}`)
      .map(res => res);
  }


  listPatientMedia(patientID, summaryId) {
    return this.http.get<any>(this.config.resourceUrl + `patient/list_media?patientId=${patientID}&isPatient=true&summaryId=${summaryId}`)
      .map(res => res);
  }

  // Add Media
  addPatientMedia(media) {
    return this.http.post<any>(this.config.resourceUrl + `patient/add_media`, media)
      .map(res => res);
  }

  // Delete Patient Media
  deletePatientMedia(media) {
    return this.http.post<any>(this.config.resourceUrl + `patient/delete_media`, media)
      .map(res => res);
  }

  // Update Patient Profile
  updatePatient(patient, PatientID) {
    return this.http.put<any>(this.config.resourceUrl + `patient/update_patient/${PatientID}`, patient)
      .map(res => res);
  }

  // Patient Media End

  // Tags Start

  createTag(tag) {
    return this.http.post<any>(this.config.resourceUrl + `specialtag/create`, tag)
      .map(res => res);
  }

  updateTag(specialTagId, tag) {
    return this.http.put<any>(this.config.resourceUrl + `specialtag/update/${specialTagId}`, tag)
      .map(res => res);
  }

  viewTagByID(specialTagId) {
    return this.http.get<any>(this.config.resourceUrl + `specialtag/viewby_id/${specialTagId}`)
      .map(res => res);
  }

  getTags(key) {
    return this.http.get<any>(this.config.resourceUrl + `specialtag/list?search=${key}`)
      .map(res => res);
  }
  // Tags End

  // Create Summary
  createSummary(specialsummary) {
    return this.http.post<any>(this.config.resourceUrl + `specialsummary/create`, specialsummary)
      .map(res => res);
  }

  // List Summary
  listSummary(patientID, isPatient) {
    return this.http.get<any>(this.config.resourceUrl + `specialsummary/list?patientId=${patientID}&isPatient=${isPatient}`)
      .map(res => res);
  }

  // View Summary
  viewSummary(summaryId) {
    return this.http.get<any>(this.config.resourceUrl + `specialsummary/view?summaryId=${summaryId}`)
      .map(res => res);
  }

  // Delete Summary
  deleteSummary(nodeID, ID) {
    return this.http.request<any>('delete', this.config.resourceUrl + `specialsummary/delete?nodeId=${nodeID}&id=${ID}`)
      .map(res => res);
  }

  // Update Summary
  updateSummary(ID, summary) {
    return this.http.put<any>(this.config.resourceUrl + `specialsummary/update/${ID}`, summary)
      .map(res => res);
  }

  // Publish Summary
  publishSummary(ID) {
    return this.http.get<any>(this.config.resourceUrl + `specialsummary/publish/${ID}`)
      .map(res => res);
  }

  getPatientDosDonts(data) {
    return this.http.post<any>(this.config.resourceUrl + `patient/list_dosdonts`, data)
      .map(res => res);
  }

  getPatientMeds(data) {
    return this.http.post<any>(this.config.resourceUrl + `patient/list_meds`, data)
      .map(res => res);
  }


  // Delete Tag
  deleteTag(nodeID, ID) {
    return this.http.request<any>('delete', this.config.resourceUrl + `specialtag/delete?nodeId=${nodeID}&specialTagId=${ID}`)
      .map(res => res);
  }

}

