import { TestBed } from '@angular/core/testing';

import { MyOrgService } from './my-org.service';

describe('MyOrgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyOrgService = TestBed.get(MyOrgService);
    expect(service).toBeTruthy();
  });
});
