import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, RequiredValidator } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService, UserService, MediaService, MyOrgService } from '../services/index';
import { IMyDpOptions } from 'mydatepicker';
import _ from 'lodash';
@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.scss']
})
export class AddPatientComponent implements OnInit {

  addpatient = 2;
  phoneMaxLength = 9;
  addPatientForm: FormGroup;
  email: FormControl;
  firstName: FormControl;
  lastName: FormControl;
  dob: FormControl;
  submitted = false;
  emailPhoneErr = false;
  patientID = 0;
  patientDetails: any = {};
  title = 'Add';
  redirectUrl = '';
  orgName;
  orgId;
  today = new Date();
  countryCodes = [];
  phoneNumber: FormControl;
  countryCode: String;
  defaultCountry: String;
  isValidPhoneError = false;
  mobilePattern: any = /^\d{10,}$/;
  alphabetOnlyPattern = /^[A-Z0-9]+$/i;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private mediaService: MediaService,
    private userService: UserService,
    private myOrgService: MyOrgService,
    private route: ActivatedRoute) { }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    editableDateField: false,
    disableSince: { year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1 }
  };

  ngOnInit() {
    this.defaultCountry = '+353';
    this.countryCode = 'IE';
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.getCountryCodes();
    this.createFormControls();
    this.createForm();
    this.getMyorg();

  }

  removePrefixZeros(removedStatus = false) {
    if (removedStatus) {
      return this.addPatientForm.controls['phoneNumber'].value;
    } else {
      let phoneNumberValue = this.addPatientForm.controls['phoneNumber'].value;

      if (phoneNumberValue && phoneNumberValue.charAt(0).toString() === '0') {
        this.addPatientForm.controls['phoneNumber'].setValue(phoneNumberValue.substring(1, phoneNumberValue.length));
        phoneNumberValue = this.addPatientForm.controls['phoneNumber'].value;
        if (phoneNumberValue && phoneNumberValue.charAt(0).toString() === '0') {
          this.removePrefixZeros();
        } else {
          this.removePrefixZeros(true);
        }
      } else {
        this.removePrefixZeros(true);
      }
    }
  }

  getCountryCodes() {
    this.authenticationService.getCountryCodes().subscribe(data => {
      this.countryCodes = data;
      this.countryCodes.forEach((countryObj, index) => {
        if (countryObj.dial_code === '+353' || countryObj.dial_code === '+91') {
          countryObj.priority = countryObj.dial_code === '+353' ? 1 : 2;
        } else {
          countryObj.priority = 3;
        }
      });

      this.countryCodes = _.orderBy(this.countryCodes, ['priority'], ['asc']);

      this.route.params.subscribe((params: any) => {
        if (params.id) {
          this.addpatient = 2;
          this.patientID = +params.id;
          this.title = 'Update';
          this.getPatientDetails();
        }
      });
    })
  }

  updatePhoneNumberValidation() {
    this.phoneMaxLength = this.defaultCountry === '+353' ? 9 : 10;
    let phoneNumberField = this.addPatientForm.controls['phoneNumber'].value ? this.addPatientForm.controls['phoneNumber'].value : '';
    phoneNumberField = phoneNumberField.substring(0, this.phoneMaxLength);

    if (phoneNumberField) {
      this.addPatientForm.controls['phoneNumber'].setValue(phoneNumberField);
    }
  }

  setCountryInfo(flag) {
    this.countryCode = flag.code;
    this.defaultCountry = flag.dial_code;
    this.updatePhoneNumberValidation();
  }

  createFormControls() {
    this.email = new FormControl('', [
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.firstName = new FormControl('', [
      Validators.required,
      Validators.minLength(3)
    ]);
    this.lastName = new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]);
    this.dob = new FormControl('', [
      Validators.required,]);
    this.phoneNumber = new FormControl('')
  }

  createForm() {
    this.addPatientForm = new FormGroup({
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      dob: this.dob,
      phoneNumber: this.phoneNumber
    });
  }
  get f() { return this.addPatientForm.controls; }
  switchTab(tabNo) {
    this.addpatient = tabNo;
  }

  createPage() {

  }

  setFlagByCountryCode(countryCode) {
    this.countryCodes.forEach((countryObj) => {
      if (countryObj.dial_code.toString() === countryCode.toString()) {
        this.countryCode = countryObj.code;
      }
    });
  }

  getPatientDetails() {
    this.userService.getUserById(this.patientID).subscribe((user) => {
      this.patientDetails = user.result;
      console.log(this.patientDetails);
      this.addPatientForm.controls['email'].setValue(this.patientDetails.email);
      this.addPatientForm.controls['firstName'].setValue(this.patientDetails.firstName);
      this.addPatientForm.controls['lastName'].setValue(this.patientDetails.lastName);
      this.addPatientForm.controls['phoneNumber'].setValue(this.patientDetails.phoneNumber);
      this.defaultCountry = this.patientDetails.country_Code ? this.patientDetails.country_Code : this.defaultCountry;
      this.updatePhoneNumberValidation();
      if (this.patientDetails.dob) {
        this.setDate();
      }
      if (this.countryCodes && this.countryCodes.length > 0) {
        this.setFlagByCountryCode(this.patientDetails.country_Code);
      }

    });

  }

  setDate(): void {
    const date = new Date(this.patientDetails.dob);
    let today: any = new Date(this.patientDetails.dob);
    let dd = today.getDate();

    let mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    console.log(today);
    this.addPatientForm.patchValue({
      dob: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        },
        formatted: today
      }
    });
  }

  clearDate(): void {
    this.addPatientForm.patchValue({ dob: null });
  }

  closeModal() {

    if (this.patientID > 0) {
      this.myOrgService.mediaFormEvent.emit({type: 4});

    } else {
      this.myOrgService.mediaFormEvent.emit({type: 3});

    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.addPatientForm.controls['phoneNumber'].value && this.addPatientForm.controls['phoneNumber'].value.length > 0) {
      this.removePrefixZeros();
    }
    this.emailPhoneErr = false;
    this.isValidPhoneError = false;

    if (this.addPatientForm.invalid) {
      return;
    } else if (!(this.addPatientForm.controls['email'].value || this.addPatientForm.controls['phoneNumber'].value)) {
      this.emailPhoneErr = true;
      return;
    }

    if (this.addPatientForm.controls['phoneNumber'].value) {
      let isValidPhone = parseInt(this.addPatientForm.controls['phoneNumber'].value);
      this.isValidPhoneError = (isValidPhone != 0) ? false : true;
      this.mobilePattern = this.defaultCountry === '+353' ? /^\d{9,}$/ : /^\d{10,}$/;

      if (!this.mobilePattern.test(this.addPatientForm.controls['phoneNumber'].value)) {
        this.isValidPhoneError = true;
      }

      if (this.isValidPhoneError) {
        return;
      }
    }

    let params: any = {};
    // params = this.addPatientForm.value;
    params.firstName = this.addPatientForm.controls['firstName'].value;
    params.lastName = this.addPatientForm.controls['lastName'].value;
    params.dob = this.addPatientForm.controls['dob'].value.formatted;
    if (this.addPatientForm.controls['email'].value) {
      params.email = this.addPatientForm.controls['email'].value;
    }
    if (this.addPatientForm.controls['phoneNumber'].value) {
      params.country_Code = this.defaultCountry;
      params.phoneNumber = this.addPatientForm.controls['phoneNumber'].value;
    }

    if (this.addPatientForm.valid && this.patientID === 0) {
      //  params.orgName = this.orgName;
      params.orgId = this.orgId;
      this.authenticationService.addPatient(params).subscribe(res => {
        if (res.result.status != 422) {
          this.toastr.success(`User login details sent to ${params.email ?  params.email : params.country_Code + params.phoneNumber} !`);
          this.submitted = false;
          this.myOrgService.mediaFormEvent.emit({type: 3});
          if (localStorage.getItem('isSpecialPatientType') === 'true') {
            this.router.navigate(['/app/covidpatients']);
          } else {
            this.router.navigate(['/app/patients']);
          }
        } else {
          this.toastr.error(res.result.err);
        }
      }, err => {
        if (err && err.error && err.error.err) {
          this.toastr.error(err.error.err);
        } else {
          this.toastr.error('Unable to add patient. Please try again');
        }
      })
    } else if (this.addPatientForm.valid && this.patientID > 0) {
      this.mediaService.updatePatient(params, this.patientID).subscribe(res => {
        if (res.result.status != 422) {
          this.toastr.success('Patient Profile Updated Successfully');
          this.submitted = false;

          if (this.redirectUrl) {
            this.router.navigate([this.redirectUrl]);
          } else {
            this.myOrgService.mediaFormEvent.emit({type: 4});
          }

        } else {
          this.toastr.error(res.result.err);
        }
      }, err => {
        if (err.error && err.error.err && err.error.err.message) {
          this.toastr.error(err.error.err.message);
        } else {
          this.toastr.error('Unable to edit patient info. Please try again');
        }
      })
    }
  }

  getMyorg() {

    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      if (nodes.result[0]) {
        this.orgName = nodes.result[0].nodes['name'];
        this.orgId = nodes.result[0].uId;
        this.orgId = this.orgId.toString();
      }
    });

  }

  isNumberKey(evt, value) {
    //if (evt != 43 && evt > 31 && (evt < 48 || evt > 57)){
    if (evt > 31 && (evt < 48 || evt > 57)) {
      return false;
    } else if (value.length > 20) {
      return false;
    }
    return true;
  }

  isCheckAlpha(keyCode, value) {
    if (value.length === 0) {
      if (keyCode === 32) {
        return false;
      }
    }
    return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 8 || keyCode == 32 || keyCode == 190)


  }

}
