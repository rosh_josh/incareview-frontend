import { Component, OnInit } from '@angular/core';
import { CovidService } from '../services/covid.service';
@Component({
  selector: 'app-covidpatients',
  templateUrl: './covidpatients.component.html',
  styleUrls: ['./covidpatients.component.scss']
})
export class CovidpatientsComponent implements OnInit {
getname="";
  constructor(private covidService: CovidService,) { }

  ngOnInit() {
    this.getaccordianname();
  }
  getaccordianname(){
    this.covidService.getdynamicname().subscribe((data) => {
      console.log(data)
      if(data.result){
        this.getname = data.result.categoryList[0].name;
        console.log(this.getname)
      }
    })
    }

}
