import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidpatientsComponent } from './covidpatients.component';

describe('CovidpatientsComponent', () => {
  let component: CovidpatientsComponent;
  let fixture: ComponentFixture<CovidpatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidpatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidpatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
