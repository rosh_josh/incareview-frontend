import { Component, OnInit, NgZone, TemplateRef } from '@angular/core';
import { MediaService, MyOrgService } from '../services';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';

import * as _ from 'lodash';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  tagsList: any = [];
  search = '';
  showTagData = 0;
  tagErrors = {
    name: false,
    summary: false
  };
  tagData = {};
  showCreate = false;
  isCreate = false;
  isEdit = false;
  nodeID;
  uId;
  selectedEditID = 0;
  modalRef: BsModalRef;

  constructor(private mediaService: MediaService, private ngZone: NgZone, private modalService: BsModalService,
    private router: Router, private location: Location, private myOrgService: MyOrgService, private toastr: ToastrService) {
  }


  ngOnInit() {
    this.getTags();
    this.getOrganisation();
    this.getMyorg();
    this.mediaService.setTabId(undefined);
  }

  createTag(type) {
    if (!this.tagData['name']) {
      this.tagErrors['name'] = true;
    } else if (!this.tagErrors[type] && !this.tagData[type]) {
      this.tagErrors[type] = true;
    } else if (!this.tagErrors[type] && !this.tagData['id']) {
      this.tagData['nodeId'] = this.nodeID;
      this.tagData['uId'] = this.uId;
      // this.myOrgService.createTagEvent.emit(1);

      this.mediaService.createTag(this.tagData).subscribe(data => {
        if(data['result']['status'] === 400) {
            this.toastr.error(data['result']['err'])
        } else {
          this.toastr.success('Tag Created Successfully');
          this.tagData = {};
          // this.tagData = data['result']['summary'];
          // this.tagData['name'] = '';
          this.isCreate = false;
          this.getTags();
          // this.router.navigate(['/app/tag-management/' + this.tagData['id']]);
          //this.viewTagInfo(this.tagData['id']);
        }
        
      })
    } else if (!this.tagErrors[type] && this.tagData['id']) {
      this.updateTag(type);
    } else {
      return this.tagErrors;
    }
  }

  updateTag(type) {
    if (this.tagData['id']) {
      this.mediaService.updateTag(this.tagData['id'], this.tagData).subscribe(data => {
        if(data && data['result'] && data['result']['err'] ) {
          this.toastr.error(data['result']['err']);
        } else {
         this.toastr.success('Tag Updated Successfully'); 
        }
        this.selectedEditID = this.tagData['id'];
        this.isEdit = false;
        this.tagData = {};
        this.getTags();
      });
    }
  }
  editTag(tagDetail) {
    this.tagData['name'] = tagDetail['name'];
    this.tagData['id'] = tagDetail['id'];
    this.tagData['createdBy'] = tagDetail['createdBy'];
    this.tagData['nodeId'] = tagDetail['nodeId'];
    this.tagData['summary'] = tagDetail['summary'];
    this.tagData['uId'] = tagDetail['uId'];
    this.isEdit = true;
    // this.myOrgService.createTagEvent.emit(2);
  }
  // validateNameTag(e, type) {
  //   console.log(e, type)
  //   this.myOrgService.createTagEvent.emit(3);
  // }

  validateName(name, type) {
    this.tagErrors[type] = (name === "") ? true : false;
  }

  isCheckAlpha(keyCode) {
    return ((keyCode >= 65 && keyCode <= 90) ||(keyCode >= 97 && keyCode <= 122) || keyCode == 8 || keyCode == 32 || keyCode == 190)
  }

  tagDetail(tagId) {
    const redirectUrl = this.location.path();
    if (window.innerWidth < 673) {
      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/app/tag-management', tagId], {
            queryParams: {
              redirectUrl
            }
          }
        )
      );
    } else {
      this.showTagData = tagId;
      this.isCreate = false;
      if (tagId !== this.tagData['id']) {
        this.isEdit = false;
      }
    }
  }

  getTags() {
    this.mediaService.getTags(this.search).subscribe(tags => {
      this.tagsList = tags.result.tagList;
      if (this.tagsList.length > 0) {
        if (this.selectedEditID > 0) {
          const index = _.findIndex(this.tagsList, { id:this.selectedEditID });
          if(index > -1){
            this.showTagData = tags.result.tagList[index].id;
          }
        } else {
          this.showTagData = tags.result.tagList[0].id;
        }
      } else {
        this.showTagData = 0;
      }
      // this.myOrgService.createTagEvent.emit(4);
    });
  }

  searchTags(key) {
    this.search = key;
    this.getTags();
  }

  getOrganisation() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList.length === 0) {
        this.showCreate = false;
      } else if (permissionList.length > 0) {
        let actions = permissionList[0].myActions;
        if (actions['isCreate'] === true || actions['isCreate'] === 'true') {
          this.showCreate = true;
        }
      }
    });
  }

  getMyorg() {
    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      if (nodes.result[0]) {
        this.nodeID = nodes.result[0].nodeId;
        this.uId = nodes.result[0].uId;
      }
    });
  }

  showCreateTag() {
    this.isCreate = true;
  }

  cancelDelete() {
    this.tagData = {};
    this.modalRef.hide();
  }

  deleteTag(confirmTagDelete: TemplateRef<any>, taglist){
    this.tagData = taglist;
    console.log(this.tagData)
    this.modalRef = this.modalService.show(confirmTagDelete, { class: 'modal-lg incareview-model incare-window' });
  }

  deleteTagInfo() {
    if(this.tagData && this.tagData['id']) {
      this.mediaService.deleteTag(this.tagData['nodeId'], this.tagData['id']).subscribe((data) => {
        if(data.result.message) {
          this.toastr.success(data.result.message);
        } else {
          this.toastr.error(data.result.err);
        }      
        this.modalRef.hide();
        this.tagData = {};
        this.getTags();
      });
    }

  }

}
