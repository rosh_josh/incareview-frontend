import { Component, Input, Output, TemplateRef, OnInit, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MyOrgService } from '../services';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  orgForm: FormGroup;
  email: FormControl;
  nodeId: FormControl;
  permissionId: FormControl;
  username: FormControl;
  submitted = false;
  nodeInfo;
  postType;
  updateID;
  postTitle;
  uId;

  constructor(private toastr: ToastrService,
    private myOrgService: MyOrgService, private router: Router) { }

  ngOnInit() {
  	this.createFormControls();
  	this.createForm();
  	if(localStorage.getItem('currentusertoken')) {
  	  this.nodeInfo = this.myOrgService.getOrgData();
  	  if(!this.nodeInfo) {
  	  	this.router.navigate(['/app/manage']);
  	  } else {
  	  	this.postType = this.nodeInfo['postType'];
  	  	this.postTitle = this.nodeInfo['postTitle'];
        this.addControls();
  	  }    
    } else {      
       this.router.navigate(['/app/login'])      
    }  
  }

  addControls() {
    let data = this.nodeInfo;
    const nodeId = '' + this.nodeInfo['nodeId'];
       this.orgForm.controls['nodeId'].setValue(nodeId);
        if(this.postType === 'Update') { 
            this.updateID = data.id;
            this.uId = data['uId'];
            this.orgForm.controls['email'].disable();
            this.orgForm.controls['username'].disable();
            data.email ? this.orgForm.controls['email'].setValue(data.email) : -1;
            data.username ? this.orgForm.controls['username'].setValue(data.username) : -1;
            data.permissionId ? this.orgForm.controls['permissionId'].setValue(data.permissionId) : -1;
        }
  }

   createFormControls() {
    this.email = new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-z0-9A-Z.-]+\.[A-Za-z]{2,4}$/)]);
    this.nodeId = new FormControl('', [Validators.required]);
    this.permissionId = new FormControl(['READ']);
    this.username = new FormControl('', [Validators.required]);
  }

  createForm() {
    this.orgForm = new FormGroup({
      email: this.email,
      nodeId: this.nodeId,
      permissionId: this.permissionId,
      username: this.username
    });
  }

  get f() { return this.orgForm.controls; }

  updatePermission() {
    this.submitted = true;
    if (this.orgForm.invalid) {
      return;
    }
    let params: any = {};
    params = this.orgForm.getRawValue();
    params.uId = this.uId.toString();
    params.nodeId = params.nodeId.toString();
    delete params.username;

    this.myOrgService.updatePermission(this.updateID, params).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {
        this.toastr.error(data.result.err);
      } else {
        this.orgForm.reset();
        this.toastr.success('User Permission Updated successfully');
        this.router.navigate(['/app/manage']);        
      }

    }, (err) => {
      this.toastr.error(err.error.message);
    });
  }

  createPermisssion() {
    let params: any = {};
    params = this.orgForm.value;
 
    this.myOrgService.inviteUser(params).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {      
        this.toastr.error(data.result.err);
      } else {
        this.orgForm.reset({ nodeId: '', email: '', permissionId: ['READ'] });
        this.toastr.success('User Invited successfully');
        this.router.navigate(['/app/manage'])
      }

    }, (err) => {     
      this.toastr.error(err.error.message);
    });
  }

  inviteUser() {
    this.submitted = true;
      if (this.orgForm.invalid) {
        return;
      }

      if(this.postType === 'Invite') {
        this.createPermisssion();
      } else {
        this.updatePermission();
      }    
  }

  permissionChange(change) {
    if (change) {
      this.orgForm.controls['permissionId'].setValue(['READ', 'CREATE', 'UPDATE', 'DELETE', 'SHARE']);
    } else {
      this.orgForm.controls['permissionId'].setValue(['READ']);
    }

  }

}
