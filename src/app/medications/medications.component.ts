import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService, UserService, MyOrgService, MediaService } from '../services';
import * as _ from 'lodash';

@Component({
  selector: 'app-medications',
  templateUrl: './medications.component.html',
  styleUrls: ['./medications.component.scss']
})
export class MedicationsComponent implements OnInit {
	medIds:any;
	medList:any = [];
	medViewList:any = [];
  constructor(private router: Router, private myOrgService: MyOrgService,
    private notificationService: NotificationService, private userService: UserService, private mediaService: MediaService) { }

  ngOnInit() {
    this.mediaService.setMedData(undefined);
  	this.medIds = this.mediaService.getPatientData();
    if(localStorage.getItem('userid')) {
      if(this.medIds) {
        this.getMeds();
      } else {
        this.router.navigate(['/app/menu']);
      }
    } else {
      this.router.navigate(['/app/login']);
    }
  	
  }

  checkFormat(typesList, otherTypeValue, formatType) {
    const typeArr = [];
    for (let [key, value] of Object.entries(typesList)) {
      if (key === 'Others' && value && otherTypeValue) {
        typeArr.push(otherTypeValue)
      } else if(key !== 'Others' && value && formatType === 'food'){
        typeArr.push(`${key} Food`)
      } else if(key !== 'Others' && value && formatType === 'medicine'){
        typeArr.push(key)
      }
    }
    return typeArr.join(",")
  }

  getMeds() {
     this.mediaService.getPatientMeds({medsId: this.medIds}).subscribe(meds => {
      this.medList = meds.result.medsList;
  		if(this.medList && this.medList.length > 0) {
  			this.medList.forEach(item => {
          item['foodTime'] = this.checkFormat(item.medFoodTime, item.medFoodTimeOthers, 'food')
          item['medicinetime'] = this.checkFormat(item.medTime, item.medTimeOthers, 'medicine')
          this.medViewList.push(item);
  				//item.tagId = (item['tagId'] && item['tagId'].length > 0) ? item['tagId'].split(',') : [];
        })
  		}
  	})
  }

  goToDragInfo(medData) {
    this.mediaService.setMedData(medData);
    this.router.navigate(['/app/druginfo']);
  }

}
