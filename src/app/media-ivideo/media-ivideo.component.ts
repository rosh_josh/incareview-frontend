import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaService, MyOrgService } from '../services';
import { Gtag } from 'angular-gtag';
import { BrowserModule, DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-media-ivideo',
  templateUrl: './media-ivideo.component.html',
  styleUrls: ['./media-ivideo.component.scss']
})
export class MediaIvideoComponent implements OnInit {

  mediaDetail: any = {};
  tagId;
  redirectUrl;
  isPatient = false;
  showCreate = false;
  docUrl;
  webUrl;
  videoUrl: SafeResourceUrl
  viewer = 'google';
  officeViewer = 'office';


  constructor(private router: Router, private route: ActivatedRoute, private gtag: Gtag,
    private mediaService: MediaService, private myOrgService: MyOrgService, private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getOrganisation();
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    if (this.redirectUrl === '/app/patient-media') {
      this.isPatient = true;
    }
    this.tagId = this.route.snapshot.queryParamMap.get('tagId');

    this.route.params
      .subscribe(params => {
        this.getMediabyID(params.id);
      });

  }

  getMediabyID(mediaId) {
    this.mediaService.mediaViewbyID(mediaId).subscribe((media) => {
      this.mediaDetail = media.result;
      if (this.mediaDetail) {
        this.gtag.event('Preview', { 'event_category': this.mediaDetail['name'], 'event_label': 'MediaView', 'event_action': 'Preview' });
        this.mediaDetail['format'] = this.mediaDetail['keyName'] && this.mediaDetail['keyName'].split('.')[1];
        this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.mediaService.imageURL + this.mediaDetail['keyName']);
        this.docUrl = this.mediaService.imageURL + this.mediaDetail['keyName'];
        this.webUrl = this.mediaDetail.webUrl ? this.domSanitizer.bypassSecurityTrustResourceUrl(this.mediaDetail.webUrl) : '';
        let relatedVideo = this.mediaDetail.relatedVideo;
        if (relatedVideo && relatedVideo.length > 0) {
          this.mediaDetail.relatedVideo.forEach(item => {
            item['format'] = item['keyName'] ? item['keyName'].split('.')[1] : '';
          })
        }
      }
    });
  }

  redirectPage() {
    this.router.navigate([this.redirectUrl]);
    /*if(this.tagId) {
      
    } else {
      this.router.navigate(['media-slides']);
    }*/
  }

  getOrganisation() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList.length === 0) {
        this.showCreate = false;
      } else if (permissionList.length > 0) {
        let actions = permissionList[0].myActions;
        if (actions['isCreate'] === true || actions['isCreate'] === 'true') {
          this.showCreate = true;
        }
      }
    });
  }

}
