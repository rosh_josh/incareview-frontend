import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaIvideoComponent } from './media-ivideo.component';

describe('MediaIvideoComponent', () => {
  let component: MediaIvideoComponent;
  let fixture: ComponentFixture<MediaIvideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaIvideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaIvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
