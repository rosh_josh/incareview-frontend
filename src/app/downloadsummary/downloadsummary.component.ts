import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaService } from '../services';
import { CovidService } from '../services/covid.service';
import * as _ from 'lodash';
import { BrowserModule, DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AppConfig } from './../app.config';


@Component({
  selector: 'app-downloadsummary',
  templateUrl: './downloadsummary.component.html',
  styleUrls: ['./downloadsummary.component.scss']
})
export class DownloadsummaryComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  @Input() summaryId;
  @Input() specialSummaryId;
  tabs;
  redirectUrl;
  downloadInfo: any = {};
  summaryList;
  videoUrl: SafeResourceUrl
  fis: any;
  patientId;
  cloudFrontURL;
  constructor(private covidService: CovidService, private router: Router, private mediaService: MediaService, private domSanitizer: DomSanitizer, private route: ActivatedRoute, private config: AppConfig,) { }

  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.route.snapshot.queryParamMap.get('summaryId')
      ? this.summaryId = this.route.snapshot.queryParamMap.get('summaryId') : -1;
    this.route.snapshot.queryParamMap.get('specialSummaryId')
      ? this.specialSummaryId = this.route.snapshot.queryParamMap.get('specialSummaryId') : -1;
    this.route.snapshot.queryParamMap.get('tabs')
      ? this.tabs = this.route.snapshot.queryParamMap.get('tabs') : -1;
    this.route.snapshot.queryParamMap.get('patientId')
      ? this.patientId = this.route.snapshot.queryParamMap.get('patientId') : -1;
      this.cloudFrontURL = this.config.cloudFrontURL;
      this.getPatientSummary()
  }

  checkFormat(typesList, otherTypeValue, formatType) {
    const typeArr = [];
    for (let [key, value] of Object.entries(typesList)) {
      if (key === 'Others' && value && otherTypeValue) {
        typeArr.push(otherTypeValue)
      } else if(key !== 'Others' && value && formatType === 'food'){
        typeArr.push(`${key} Food`)
      } else if(key !== 'Others' && value && formatType === 'medicine'){
        typeArr.push(key)
      }
    }
    return typeArr.join(",")
  }

  getMedInfo(medIds) {
    this.mediaService.getPatientMeds({ medsId: medIds }).subscribe(meds => {
      let medList = meds.result.medsList;
      if (medList && medList.length > 0) {
        medList.forEach(item => {
          item['foodTime'] = this.checkFormat(item.medFoodTime, item.medFoodTimeOthers, 'food')
          item['medicinetime'] = this.checkFormat(item.medTime, item.medTimeOthers, 'medicine')
        })
      }
      this.downloadInfo['medList'] = medList;
    })
  }

  findcheckList(summary, ignoreCheck = false) {
    let mediaList = ignoreCheck ? summary['mediaInfo'] : _.filter(summary['mediaInfo'], { isSelect: true });
    let isMeds = ignoreCheck ? summary['medsInfo'] : _.filter(summary['medsInfo'], { isSelect: true });
    let isDosDont = ignoreCheck ? summary['dosDontsInfo'] : _.filter(summary['dosDontsInfo'], { isSelect: true });
    // let imageMediaDownload = ignoreCheck ? _.filter(mediaList, { type: 'image' }) : _.filter(mediaList, { isSelect: true, type: 'image' });
    let isDos = ignoreCheck ? _.filter(isDosDont, { type: 'Dos' }) : _.filter(isDosDont, { isSelect: true, type: 'Dos' });
    let isDonts = ignoreCheck ? _.filter(isDosDont, { type: 'Donts' }) : _.filter(isDosDont, { isSelect: true, type: 'Donts' });
    if (isMeds && isMeds.length > 0) {
      let medIds = [];
      isMeds.forEach(item => medIds.push(item['id']));
      this.getMedInfo(medIds);
    }
    // if (imageMediaDownload && imageMediaDownload.length > 0) {
    //   imageMediaDownload.forEach((item, index) => {
    //     item['url'] = this.mediaService.imageURL + item['keyName'];
    //   })
    // }

    const updatedMediaList = mediaList.map(media => {

      // const a = document.createElement("a");
      // a.style.display = "none";
      // document.body.appendChild(a);

      // // Set the HREF to a Blob representation of the data to be downloaded
      // a.href = media.fileUrl;

      // // Use download attribute to set set desired file name
      // a.setAttribute("download", "sample");
      // a.target = "_blank";
      // // Trigger the download by simulating click
      // a.click();

      // // Cleanup
      // window.URL.revokeObjectURL(a.href);
      // document.body.removeChild(a);

      if(media.thumbnailUrl) {
        media.previewURL = `${this.cloudFrontURL}${media.thumbnailUrl}`;
      } else if(media.type === 'text') {
        media.previewURL = './assets/images/incare-mobile/txt.png';
      } else if(media.type === 'audio') {
        media.previewURL = './assets/images/incare-mobile/audio.png';
      } else if(media.type === 'video') {
        media.previewURL = './assets/images/incare-mobile/video.png';
      } else if(media.fileUrl.includes('.pdf')) {
        media.previewURL= './assets/images/incare-mobile/pdf.png';
      } else if(media.fileUrl.includes('.xlsx') || media.fileUrl.includes('.xls') || media.fileUrl.includes('.csv')) {
        media.previewURL= './assets/images/incare-mobile/excel.png';
      } else if(media.fileUrl.includes('.doc') || media.fileUrl.includes('.docx')) {
        media.previewURL= './assets/images/incare-mobile/word.png';
      } else if(media.fileUrl.includes('.ppt') || media.fileUrl.includes('.pptx')) {
        media.previewURL= './assets/images/incare-mobile/ppt.png';
      } else if(media.fileUrl.includes('.zip') || media.fileUrl.includes('.rar')) {
        media.previewURL= './assets/images/incare-mobile/zip.png';
      }
      return media
    })

    let patientInfo = summary.patientInfo;
    let careInfo = summary.careInfo;
    let patientName = (patientInfo['firstName']) ? patientInfo['firstName'] + ' ' + patientInfo['lastName'] : patientInfo['email'];
    let careName = (careInfo['firstName']) ? careInfo['firstName'] + ' ' + careInfo['lastName'] : careInfo['email'];

    this.downloadInfo = { notes: summary['notes'], media: updatedMediaList, dos: isDos, donts: isDonts, meds: isMeds, patientName: patientName, careName: careName, createdAt: this.summaryList['created_at'] };
  }

  getPatientSummary() {

    if (this.summaryId) {
      this.mediaService.viewSummary(this.summaryId).subscribe(summary => {
        if (summary.result.status === 400) {
          console.log('Invalid');
        } else {
          this.summaryList = summary.result.summaryList;
          if (this.summaryList) {
            this.findcheckList(this.summaryList)
          }
        }
      });
    } else {
      this.covidService.viewSummary(this.specialSummaryId).subscribe(summary => {
        if (summary.result.status === 400) {
          console.log('Invalid');
        } else {
          this.summaryList = summary.result.summaryList;
          if (this.summaryList) {
            this.findcheckList(this.summaryList, true)
          }
        }
      });
    }
  }

  printSummary() {
    window.print();
  }

  redirectPage() {
    this.router.navigateByUrl(
      this.router.createUrlTree(
        [this.redirectUrl], {
        queryParams: { patientId: this.patientId, summaryId: this.summaryId, tabs: this.tabs }
      }
      ));
  }

}
