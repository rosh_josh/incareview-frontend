import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadsummaryComponent } from './downloadsummary.component';

describe('DownloadsummaryComponent', () => {
  let component: DownloadsummaryComponent;
  let fixture: ComponentFixture<DownloadsummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadsummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadsummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
