import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyOrgService, MediaService } from '../../services';
import { CovidService } from '../../services/covid.service';

@Component({
  selector: 'app-bottomnav',
  templateUrl: './bottomnav.component.html',
  styleUrls: ['./bottomnav.component.scss']
})
export class BottomnavComponent implements OnInit {
  permissionList: any = [];
  historyList: any = [];
  isPatient = true;
  userType;
  getname="";

  constructor(private router: Router, private myOrgService: MyOrgService,
    private mediaService: MediaService,private covidService: CovidService,) { }

  ngOnInit() {
    let userId = localStorage.getItem('userid');
    this.getaccordianname();
    if (localStorage.getItem('currentusertoken')) {
      this.userType = localStorage.getItem('userType');
      this.getOrganisation();
      this.getSummaryList(userId)
    } else {
      this.router.navigate(['/app/login'])
    }
  }

  getaccordianname() {
    this.covidService.getdynamicname().subscribe((data) => {
      if(data.result){
        this.getname = data.result.categoryList[0].name;
      }
    })
  }

  getOrganisation() {
    this.myOrgService.getPermission().subscribe((data) => {
      this.permissionList = data.result.permissionList;
      if (this.permissionList.length === 0) {
        this.isPatient = true;
      } else if (this.permissionList.length > 0) {
        this.isPatient = false;
      }
    });
  }

  getSummaryList(userId) {
    this.mediaService.listSummary(userId, true).subscribe((data) => {
      this.historyList = data.result.summaryList;
      if (this.historyList && this.historyList.length === 0) {
        this.mediaService.setPatientData(undefined);
      }
    });
  }

  redirectTo(uri: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate([uri]));
  }

  goRedirectPage(type, url) {
    this.mediaService.setPatientData(undefined);
    /*if (this.historyList && this.historyList.length > 0) {
      let ids = [];
      this.historyList.forEach(item => {
        if (type === 'Dos') {
          item.dosDontsId = (item['dosDontsId'] && item['dosDontsId'].length > 0) ? item['dosDontsId'].split(',') : [];
          ids = ids.concat(item.dosDontsId);
        }
        if (type === 'Meds') {
          item.tagId = (item['tagId'] && item['tagId'].length > 0) ? item['tagId'].split(',') : [];
          ids = ids.concat(item.tagId);
        }        
      })
      if (type === 'Notes') {
        ids = this.historyList;
      }
      
    }*/
    this.mediaService.setType(type);
    this.redirectTo('/app/patient-summary');
  }
}
