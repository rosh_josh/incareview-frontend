import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MyOrgService, MediaService } from '../../services';
import { CovidService } from '../../services/covid.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  @Output() sidemenuToggle = new EventEmitter<boolean>();

  shouldShow = false;
  isPatient = false;
  historyList:any=[];
  userType;
  getname="";

  constructor( private router: Router,private toastr: ToastrService,
    private myOrgService: MyOrgService, private mediaService :MediaService,private covidService: CovidService,) { }

  ngOnInit() {
    if (localStorage.getItem('currentusertoken')) {
      let userId = localStorage.getItem('userid');
      this.userType = localStorage.getItem('userType');
      this.getOrganisation();
      this.getSummaryList(userId);
    } else {
      this.router.navigate(['/app/login'])
    }
    this.getaccordianname();
  }
  getaccordianname(){
    this.covidService.getdynamicname().subscribe((data) => {
      console.log(data)
      if(data.result){
        this.getname = data.result.categoryList[0].name;
        console.log(this.getname)
      }
    })
    }
  


  sidebarToggle() {
    this.sidemenuToggle.emit(this.shouldShow = !this.shouldShow);
    console.log(1);
  }


  getOrganisation() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
      if (permissionList.length === 0) {
        this.isPatient = true;
      } else if (permissionList.length > 0) {
        let actions = permissionList[0].myActions; 
         this.isPatient = false;
      }
    });
  }


  getSummaryList(userId) {
    this.mediaService.listSummary(userId, true).subscribe((data) => {
        this.historyList = data.result.summaryList;
        if(this.historyList && this.historyList.length === 0) {
          this.mediaService.setPatientData(undefined);
        }
      });
  }

  goRedirectPage(type, url) {
    this.mediaService.setPatientData(undefined);
    /*if (this.historyList && this.historyList.length > 0) {
      let ids = [];
      this.historyList.forEach(item => {
        if (type === 'Dos') {
          item.dosDontsId = (item['dosDontsId'] && item['dosDontsId'].length > 0) ? item['dosDontsId'].split(',') : [];
          ids = ids.concat(item.dosDontsId);
        }
        if (type === 'Meds') {
          item.tagId = (item['tagId'] && item['tagId'].length > 0) ? item['tagId'].split(',') : [];
          ids = ids.concat(item.tagId);
        } 
        if (type === 'Media') {
          item.medialinkId = (item['medialinkId'] && item['medialinkId'].length > 0) ? item['medialinkId'].split(',') : [];
          ids = ids.concat(item.medialinkId);
        }        
      })
      if (type === 'Notes') {
        ids = this.historyList;
      }
      
    }*/
    this.mediaService.setType(type);
    this.router.navigate(['/app/patient-summary']);
  }
}
