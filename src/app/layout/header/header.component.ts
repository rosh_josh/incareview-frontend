import { Component, OnInit, Output, EventEmitter, HostListener, ComponentFactoryResolver } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NotificationService, UserService } from '../../services';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  @Output() sidemenuToggle = new EventEmitter<boolean>();

  origin = 1; // 1 - Unread; 2 - Read; 0 - Both
  notifyCount: any = {
    count: ''
  };
  pageNumber = '';
  itemPerPage = 10;
  count: any;
  notififyList: any =
    {
      entity: [{}]
    };
  public show = true;

  userInfo = {};
  notificationList = [];

  throttle = 30;
  scrollDistance = 1;


  limit = 3;
  page = 1;
  userType;
  showSettings;

  // @HostListener('window:scroll', ['$event'])
  // scrollHandler($event) {
  //   console.log($event.target.scrollTop);
  //   this.onScrollDown('0');
  // }


  constructor(private router: Router,
    private notificationService: NotificationService, private userService: UserService) { }

  ngOnInit() {
    let userId = localStorage.getItem('userid');
    if (localStorage.getItem('currentusertoken')) {
      this.userType = localStorage.getItem('userType');
      this.getUser(userId);
      this.getNotification();
      this.notificationService.getMessages('notification:' + userId).subscribe((data) => {
        this.page = 1;
        this.notificationList = [];
        this.getNotification();
      });
    } else {
      this.router.navigate(['/app/login'])
    }

    this.router.events.subscribe((routerInfo) => {
      if (routerInfo && routerInfo instanceof NavigationEnd && routerInfo.url === '/app/settings') {
        this.showSettings = false;
      } else {
        this.showSettings = true;
      }
    });

    if (this.router.url !== '/app/settings') {
      this.showSettings = true;
    }
  }

  getUser(id) {
    this.userService.getUserById(id).subscribe(userInfo => {
      this.userInfo = userInfo.result;
    })
  }

  getNotification() {
    this.notificationService.listNotification(this.page, this.limit).subscribe((notification) => {
      if (notification.result.notification.length > 0) {
        notification.result.notification.forEach(notify => {
          this.notificationList.push(notify);
        });
      }
      // console.log(this.notificationList);
    });
  }

  onUp(ev) {
    console.log('scrolled up!', ev);
  }

  onScrollDown(ev) {
    // console.log('scrolled down!!', ev);
    this.page = this.page + 1;
    this.getNotification();
  }

  // scrollHandler(event) {
  //   console.log(event);
  // }

  changeRead(notification) {
    console.log(notification);
    this.notificationService.updateNotification(notification.id).subscribe((data) => {
      console.log(data);
      this.page = 1;
      this.notificationList = [];
      this.getNotification();
    });
  }

  redirectPage() {
    let rememberedInfo = localStorage.getItem('loginInfo');
    localStorage.clear();
    sessionStorage.clear();
    localStorage.setItem('loginInfo', rememberedInfo);
    this.router.navigate(['/app/login']);
  }

  logout() {
    this.userService.logout().subscribe(data => {
      this.redirectPage();
    }, (err) => {
      this.redirectPage();
    });
  }
}
