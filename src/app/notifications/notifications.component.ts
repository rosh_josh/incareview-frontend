import { Component, OnInit, HostListener } from '@angular/core';
import { NotificationService } from '../services';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notificationList = [];
  limit = 5;
  page = 1;

  // @HostListener('window:scroll', ['$event'])
  // scrollHandler($event) {
  //   console.log($event);
  // }

  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  seletedID = 0;

  constructor(private notificationService: NotificationService) { }

  swipeEvent(e, notification) {
    console.log(e, notification);
    this.seletedID = notification.id;
    // if (e.direction == 2) {
    //   //direction 2 = right to left swipe.
    //   console.log("dsfdsfdsf")
    // }
    // if (e.direction == 1) {
    //   console.log("asd")
    // }
  }

  ngOnInit() {
    this.getNotification();
    const userId = localStorage.getItem('userid');
  this.notificationService.getMessages('notification:' + userId).subscribe((data) => {
    console.log(data);
    this.page  = 1;
    this.notificationList = [];
    this.getNotification();


  });
  }

  getNotification() {
    this.notificationService.listNotification(this.page, this.limit).subscribe((notification) => {
      // this.notificationList = notification.result.notification;
      if (notification.result.notification.length > 0) {
        notification.result.notification.forEach(notify => {
          this.notificationList.push(notify);
        });
      }
      console.log(this.notificationList);
    });

  }
  // scrollHandler(event) {
  //   console.log(event);
  // }

  onUp(ev) {
    console.log('scrolled up!', ev);
  }

  onScrollDown(ev) {
    console.log('scrolled down!!', ev);
    this.page = this.page + 1;
    this.getNotification();
  }

  scrollHandler(event) {
    console.log(event);

  }

  view(notification) {
    console.log('view');
    this.notificationService.viewNotificationbyID(notification.id).subscribe((data) => {
      console.log(data);
    });
  }

  changeRead(notification, index) {
    this.notificationService.updateNotification(notification.id).subscribe((data) => {
     this.notificationList[index]['isRead'] = 1;
    });

  }

  delete(notification, index) {
    console.log('delete');
    this.notificationService.deleteNotification({ notificationId: [notification.id] }).subscribe((data) => {
      this.notificationList.splice(index, 1)
    });
  }

}
