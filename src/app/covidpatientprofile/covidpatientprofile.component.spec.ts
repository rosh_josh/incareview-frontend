import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidpatientprofileComponent } from './covidpatientprofile.component';

describe('CovidpatientprofileComponent', () => {
  let component: CovidpatientprofileComponent;
  let fixture: ComponentFixture<CovidpatientprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidpatientprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidpatientprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
