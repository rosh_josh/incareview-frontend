import { Component, OnInit, TemplateRef, NgZone, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MediaService, MyOrgService, UserService } from '../services';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { FormControl } from '@angular/forms';
import { AppConfig } from './../app.config';
import * as _ from 'lodash';
import { CovidService } from '../services/covid.service';

@Component({
  selector: 'app-covidpatientprofile',
  templateUrl: './covidpatientprofile.component.html',
  styleUrls: ['./covidpatientprofile.component.scss']
})
export class CovidpatientprofileComponent implements OnInit {

  @ViewChild('editPatientModal') public editPatientModal: TemplateRef<any>;
  @ViewChild('addVideoModal') public addVideoModal: TemplateRef<any>;
  @ViewChild('addSummaryMedicationModal') public addSummaryMedicationModal: TemplateRef<any>;
  @ViewChild('createMediaModal') public createMediaModal: TemplateRef<any>;
  @ViewChild('mediaFormModal') public mediaFormModal: TemplateRef<any>;

  modalRef: BsModalRef;
  deletePatient;
  deleteID;
  summaryMaximumLimit = 20000;
  summaryrequired = false;
  summarylengthexceed = false;
  isFirstOpen = true;
  tabs = 1;
  showDoInstruction = false;
  showDontsInstruction = false;
  medsList: any = [];
  nodeID;
  uId;
  DosMessage = '';
  DontsMessage = '';
  DosLists: any = [];
  DontsList: any = [];
  mediaList: any = [];
  mediaLinkList: any = [];
  flatMediaList: any = [];
  dontScroll = false;
  formControlValue = '';
  patientID = 0;
  patientDetails: any = {};
  currentUserID = 0;
  tagsList: any = [];
  seletedTagIds: any = [];
  tagInfoList;
  tags: any = [];
  notesSummary: any = [];
  editNote = false;
  removeTag;
  showBottomNav = true;
  isDrop = false;
  Seleteditems = [];
  public items = [];
  DosdArrayList = [];
  public validators = [this.startsWithAt];
  isAddMedia = false;
  mediaFilter: any = { name: '' };
  mediaIDs: any = [];
  deleteMediaIDs: any = [];
  isSummary = false;
  summaryList: any = [];
  careTeamList: any = [];
  careTeamUser: any = [];
  dosDontsIds: any = [];
  medsIds: any = [];
  selectedTagID = 0;
  medialinkIds: any = [];
  previousUrl: string;
  showCare = false;
  historyList: any = [];
  isPermission = false;
  isMobileview = false
  notesInfo = [];
  tagId;
  isAllowCreate = true;
  summaryId;
  redirectPatientID;
  selectMedia = {};
  isEnableDos = true;
  isEnablenotes = true;
  isEnableMedia = true;
  isEnableMeds = true;
  dosDonts = {};
  isEdit = false;
  editMedsID;
  addMedSummaryId;
  selectMeds = {};
  isNotesEdit = false;
  mediaType: String = '';
  checkList = [{ id: 1, value: 'Notes Added', type: 'Notes', isSelect: false }, { id: 2, value: "Do's & Dont's Added", type: 'DosDonts', isSelect: false },
  { id: 3, value: 'Medications Added', type: 'Meds', isSelect: false }, { id: 4, value: 'Educational Information Added', type: 'Media', isSelect: false }]

  constructor(private covidService: CovidService, private modalService: BsModalService,
    private mediaService: MediaService, private router: Router, private route: ActivatedRoute, private location: Location,
    private myOrgService: MyOrgService, private userService: UserService,
    private toastr: ToastrService, private ngZone: NgZone, private config: AppConfig) {

    window.onresize = (e) => {

      this.ngZone.run(() => {
        this.isCheckMobile();

      });
    };

    this.isCheckMobile();

  }

  isCheckMobile() {
    if (window.innerWidth < 673) {
      this.isMobileview = false;
    } else {
      this.isMobileview = true;
    }
  }

  deleteConfirmModel(confirmDelete: TemplateRef<any>, data) {
    this.deleteID = data.id;
    this.deletePatient = data.firstName;
    this.modalRef = this.modalService.show(confirmDelete, { class: 'modal-lg incareview-model incare-window' });
  }

  deletePatientUser() {
    if (this.deleteID) {
      this.userService.deletePatient(this.deleteID).subscribe((org) => {
        this.deleteID = '';
        if (org.result.status) {
          this.modalRef.hide();
          document.body.classList.remove('modal-open');
          this.toastr.error(org.result.err);
        } else {
          this.modalRef.hide();
          document.body.classList.remove('modal-open');
          this.toastr.success(this.deletePatient + ' deleted successfully');
          this.deletePatient = '';
          this.router.navigate(['/app/covidpatients']);
        }
      });
    }
  }

  addDoInstruction(dosData) {
    if (dosData) {
      dosData.showDos = true;
    } else {
      this.showDoInstruction = true;
      this.showDontsInstruction = true;
    }

    //this.selectedTagID = dosData.tagID;
    // this.showDoInstruction = true;
  }

  addDontsInstruction(dontData) {
    //this.selectedTagID = dontData.tagID;
    if (this.dosDonts && this.dosDonts['id']) {
      dontData.showDos = false;
      this.dosDonts = {};
      this.isEdit = false;
    }

    if (dontData) {
      dontData.showDonts = true;
    } else {
      this.showDontsInstruction = true;
      this.showDoInstruction = true;

    }
    // this.showDontsInstruction = true;
  }

  closeDos(dosData) {
    this.showDoInstruction = true;
    if (dosData && dosData.type) dosData.showDos = false;
    this.DosMessage = '';
    this.isEdit = false;
    this.dosDonts = {};
  }

  closeDonts(dontData) {
    this.showDoInstruction = false;
    if (dontData && dontData.type) dontData.showDonts = false;
    this.DontsMessage = '';
    this.isEdit = false;
    this.dosDonts = {};
  }

  ngOnInit() {
    this.route.snapshot.queryParamMap.get('summaryId')
      ? this.summaryId = this.route.snapshot.queryParamMap.get('summaryId') : -1;
    this.redirectPatientID = this.route.snapshot.queryParamMap.get('patientId');
    let tabId = this.route.snapshot.queryParamMap.get('tabs');
    if (tabId) this.tabs = parseInt(tabId);

    this.route.params.subscribe((params) => {
      this.patientID = +params.id;
      this.getPatientDetails();
      this.getPatientMedias();
      if (this.summaryId) this.getPatientSummary();
      this.getCareTeamDropdownList();
      this.getHistory();
      this.myOrgService.getUserPermission().subscribe((data) => {
        let permissionList = data.result.permissionList;
        if (permissionList.length > 0) {
          let actions = permissionList[0].myActions;
          if (actions['isCreate'] === true || actions['isCreate'] === 'true') {
            this.isPermission = true;
          }
        } else {
          this.isPermission = false;
        }
      });
    });
    this.currentUserID = +localStorage.getItem('userid');
    // this.getMediaList();
    // this.getMeds();
    this.getMyorg();
    // this.getDos();
    this.getTags();

    this.covidService.selectedTag.subscribe((data) => {
      const tag = JSON.parse(data);
      const found = this.seletedTagIds.findIndex(element => element.id === tag.id);
      if (found < 0) {
        this.seletedTagIds.push({ id: tag.id, name: tag.name });
        this.getTagDetails(tag.id);
      }

    });

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((e: any) => {
        if (this.previousUrl) {
          const careURL = this.previousUrl.substring(0, 14);
          if (careURL === '/app/add-careteam?') {
            this.showCare = true;
            this.isFirstOpen = false;
          }
        }
        this.previousUrl = e.url;
      });

    this.myOrgService.mediaFormEvent.subscribe((data) => {
      let count = this.modalService.getModalsCount();
      this.mediaType = '';
      if (data.type === 4) {
        if (this.modalRef) this.modalRef.hide();
        this.getPatientDetails();
      }
      if (data.type === 5) {
        if (this.modalRef) this.modalRef.hide();
        this.redirectPatientID = this.patientID;
        if (this.summaryId) {
          this.getPatientSummary();
        } else {
          this.getHistory();
        }
      } else if (data.type === 1 && data.mediaType === 'flashCard') {
        this.modalRef && this.modalRef.hide();
        this.mediaType = data.mediaType;
        this.modalRef = this.modalService.show(this.mediaFormModal, { backdrop: 'static', keyboard: false, class: 'modal-lg incareview-model org-window' });
        if (count > 1) {
          this.modalService.hide(count);
        }
      }
      /*
              if (data === 9) {
                this.modalRef.hide();
                if(count === 1) this.modalRef = this.modalService.show(this.mediaFormModal, { backdrop : 'static', keyboard : false, class: 'modal-lg incareview-model org-window' });
              }
              if (data === 2) {
                //if(this.modalRef) 
                this.modalRef.hide();
                this.redirectPatientID = this.patientID;
      
                  if(this.summaryId) {
                   this.getPatientSummary();
                  } else {
                    this.getHistory();
                  }
              }*/
    })
    this.summaryrequired = false;
    this.summarylengthexceed = false;
  }

  getHistory() {
    this.covidService.listSummary(this.patientID, false).subscribe((data) => {
      this.historyList = data.result.summaryList;

      if (this.redirectPatientID && !this.summaryId) {
        if (this.historyList && this.historyList.length > 0) {
          this.summaryId = this.historyList[0]['id'];
          this.getPatientSummary();
        }
      }
    });
  }

  editPatient(id) {
    const redirectUrl = this.location.path();
    // console.log(redirectUrl);
    if (!this.isMobileview) {
      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/app/add-patient', id], {
          queryParams: {
            redirectUrl
          }
        })
      );
    } else {
      this.modalRef = this.modalService.show(this.editPatientModal, { class: 'modal-lg incareview-model org-window' });
    }
  }

  getCareTeamDropdownList() {
    this.myOrgService.careTeamDropdown().subscribe((data) => {
      this.careTeamList = data.result.careTeamInfo;
      if (this.careTeamList) {
        this.onCareTeamChange(this.careTeamList[0].id);
      }
    });
  }

  onCareTeamChange(input) {

    this.myOrgService.careUserList(input).subscribe((data) => {
      this.careTeamUser = data.result.careTeamUser;
      if (this.careTeamUser && this.careTeamUser[0]) {
        this.nodeID = this.careTeamUser[0].nodeId;
      } else {
        this.nodeID = input;
      }
    });

  }

  dosAddLink(type, dosID) {

    const found = this.dosDontsIds.findIndex(element => +element === dosID);
    if (type) {
      if (found < 0) {
        this.dosDontsIds.push(dosID);
      }
    } else {
      if (found >= 0) {
        this.dosDontsIds.splice(found, 1);
      }
    }
    this.updateDosAddLink();

  }

  updateMedDosDontsSummary(params) {
    if (this.isSummary) {
      this.covidService.updateSummary(this.summaryList.id, params).subscribe((summary) => {
        this.getPatientSummary();
      }, (err) => {
        this.toastr.error(err.error.err);
        this.getPatientSummary();
      });
    }
  }

  updateDosAddLink() {
    const params: any = {};
    params.createdFor = this.patientID;
    if (this.seletedTagIds && this.seletedTagIds[0]) {
      params.specialTagId = this.seletedTagIds;
    }
    params.nodeId = this.nodeID;
    params.uId = this.uId;
    params.dosDontsId = this.dosDontsIds;
    params.medsId = this.medsIds;
    this.updateMedDosDontsSummary(params)
  }

  mediaLinkInfoDetails(summary) {
    // let mediaLinkInfo = summary['medialinkInfo'];
    let mediaLinkInfo = summary['mediaInfo'];

    let linkMedia = [];
    if (mediaLinkInfo && mediaLinkInfo.length > 0) {
      mediaLinkInfo.forEach(item => {
        /*if(item['mediaInfo'] && item['mediaInfo']['id']) {
          let format = item['mediaInfo']['keyName'].split('.')[1];
          item['mediaInfo']['format'] = format;
          linkMedia.push(item['mediaInfo']);
        }*/
        let format = item['keyName'] ? item['keyName'].split('.')[1] : '';
        item['format'] = format;
        linkMedia.push(item);
      })
      const medialist = _.chain(linkMedia).groupBy('type').toPairs()
        .map(function (currentItem) {
          return _.zipObject(['mediaType', 'media'], currentItem);
        }).value();
      this.mediaLinkList = medialist;
      // console.log("mediaLinkList", this.mediaLinkList)
    } else {
      this.mediaLinkList = [];
    }
  }

  changeViewMenu() {
    let dosDonts = [], notes = [], media = [], meds = [];
    let tagInfo = this.summaryList['tagInfo'];
    let isPublish = this.summaryList.isPublish;
    if (tagInfo && tagInfo.length > 0 && isPublish) {
      tagInfo.forEach(item => {
        if (item['dosDontsInfo'] && item['dosDontsInfo'].length > 0) dosDonts = dosDonts.concat(item['dosDontsInfo']);
        if (item['mediaInfo'] && item['mediaInfo'].length > 0) media = media.concat(item['mediaInfo']);
        if (item['medsInfo'] && item['medsInfo'].length > 0) meds = meds.concat(item['medsInfo']);
        if (item['summary']) notes.push(item['summary'])
      })
      this.isEnableDos = (dosDonts.length === 0 && this.dosDontsIds.length === 0) ? false : true;
      this.isEnablenotes = (notes.length === 0 && !this.summaryList.notes) ? false : true;
      this.isEnableMedia = (media.length === 0 && this.summaryList.mediaInfo && this.summaryList.mediaInfo.length === 0) ? false : true;
      this.isEnableMeds = (meds.length === 0 && this.medsList.length === 0) ? false : true;
    } else {
      this.isEnableDos = true;
      this.isEnablenotes = true;
      this.isEnableMedia = true;
      this.isEnableMeds = true;
    }

    // console.log(this.isEnableDos, this.isEnablenotes, this.isEnableMedia, this.isEnableMeds)
  }

  findcheckList(summary) {
    let isMedia = summary['mediaInfo'] && summary['mediaInfo'].length > 0 ? summary['mediaInfo'] : [];
    let isMeds = summary['medsInfo'] && summary['medsInfo'].length > 0 ? summary['medsInfo'] : [];
    let isDosDont = summary['dosDontsInfo'] && summary['dosDontsInfo'].length > 0 ? summary['dosDontsInfo'] : [];
    this.checkList.forEach(item => {
      if (item['type'] === 'Media') {
        item['isSelect'] = isMedia && isMedia.length > 0;
      }
      if (item['type'] === 'Meds') {
        item['isSelect'] = isMeds && isMeds.length > 0;
      }
      if (item['type'] === 'DosDonts') {
        item['isSelect'] = isDosDont && isDosDont.length > 0;
      }
      if (item['type'] === 'Notes') {
        item['isSelect'] = summary['notes'] && summary['notes'].length > 0;
      }
    })

  }

  getPatientSummary() {

    this.covidService.viewSummary(this.summaryId).subscribe(summary => {
      if (summary.result.status === 400) {
      } else {
        this.summaryList = summary.result.summaryList;
        if (this.summaryList) {
          this.findcheckList(this.summaryList)
          this.DosdArrayList = [];
          this.medsList = [];
          // this.mediaList = [];
          this.notesInfo = [];
          //  this.flatMediaList = [];
          //this.mediaLinkList = [];
          this.dosDontsIds = this.summaryList.dosDontsId;
          this.medialinkIds = this.summaryList.medialinkInfo;
          this.flatMediaList = this.summaryList.mediaInfo;
          this.medsIds = this.summaryList.medsId;
          this.isSummary = true;
          this.nodeID = this.summaryList.nodeId;
          this.uId = this.summaryList.uId;
          this.medsList = this.summaryList['medsInfo'];
          this.changeViewMenu();
          this.mediaLinkInfoDetails(this.summaryList)
          this.viewDosDont();
          this.viewMediaInfo();


          if (this.isAllowCreate) {
            this.isAllowCreate = !this.summaryList.isPublish;
          }

          // if (this.summaryList.notes) {
          //   this.summaryList.notes = this.summaryList.notes.replace(/,/g, '');
          //   this.notesInfo.push(this.summaryList.notes);
          //   this.editNote = true;
          // }
          this.seletedTagIds = [];
          this.Seleteditems = [];

          if (summary.result && summary.result.summaryList && summary.result.summaryList.tagInfo) {
            summary.result.summaryList.tagInfo.forEach((tag) => {
              this.Seleteditems.push({
                name: tag.name, value: '@' + tag.name, display: '@' + tag.name,
                id: tag.id, summary: tag.summary
              });
              this.notesInfo.push(tag.summary);
              this.seletedTagIds.push(tag.id);
              // this.tagListAdd(tag, tag.id, this.summaryList.medialinkInfo);

            });
          }

          if (this.Seleteditems && this.Seleteditems.length > 0) this.Seleteditems = this.Seleteditems.reverse();
          this.autoUpdateSummaryDosDonts(this.tagId, this.summaryList.tagInfo);
          this.getHistory();
        }

        if (this.redirectPatientID) this.location.go('/app/covidpatientprofile/' + this.patientID);



      }
    });

  }

  getPatientMedias() {
    this.covidService.getPatientMedia(this.patientID).subscribe((list) => {
    });
  }

  addTags() {
    this.tags = [];
    const matchMentions = /(@\w+) ?/g;
    let mention;
    // tslint:disable-next-line
    while ((mention = matchMentions.exec(this.formControlValue))) {
      this.tags.push({
        indices: {
          start: mention.index,
          end: mention.index + mention[1].length
        },
        value: mention[1].slice(1),
        data: mention[1]
      });
    }

    this.seletedTagIds.forEach((selectedTag) => {
      let isData = false;
      this.tags.forEach((changeTag) => {

        selectedTag.indices = changeTag.indices;
        if (selectedTag.name === changeTag.value) {
          isData = true;
          // selectedTag.indices = changeTag.indices;
        }
      });
      if (isData) {
        // this.getTagDetails(selectedTag.id);
      } else {
        // const data = this.formControlValue.slice(selectedTag.indices.start, selectedTag.indices.end);
        // if (data !== '@' + selectedTag.name) {
        //   this.formControlValue = this.formControlValue
        //     .replace(this.formControlValue.substring(selectedTag.indices.start, selectedTag.indices.end), '');
        // }

      }
    });
  }

  getTagDetails(tagID) {
    this.covidService.viewTagByID(tagID).subscribe((tag) => {
      this.tagInfoList = tag.result.tagInfoList;
      // this.medsList = this.tagInfoList.medsInfo;
      this.tagListAdd(this.tagInfoList, this.tagInfoList.id);

    });
  }


  getCurrentSummary() {
    this.summaryList = [];
    this.dosDontsIds = [];
    this.isSummary = false;
    this.Seleteditems = [];
    this.seletedTagIds = [];
    this.medsList = [];
    this.DosdArrayList = [];
    this.medsIds = [];
    this.flatMediaList = [];
    //this.mediaList = [];
    //this.mediaLinkList = [];
    this.getPatientSummary();

  }


  viewDosDont() {
    this.DosdArrayList = [];
    if (this.summaryList['dosDontsInfo'] && this.summaryList['dosDontsInfo'].length > 0) {
      let unitInfo = [];
      this.summaryList['dosDontsInfo'].forEach(item => {
        unitInfo.push(item['unitInfo']);
      })
      let summaryDosDont = [];
      unitInfo = _.uniqBy(unitInfo, 'id');
      unitInfo.forEach(item => {
        let filters = _.filter(this.summaryList['dosDontsInfo'], { nodeId: item['nodeId'] });
        summaryDosDont.push({ unitInfo: item, dosDontsInfo: filters });
      })

      summaryDosDont.forEach(item => {
        const result = _.chain(item['dosDontsInfo'])
          .groupBy('type').toPairs().map(function (currentItem) {
            return _.zipObject(['type', 'list'], currentItem);
          }).value();

        if (result.length === 0) {
          result.push({ type: 'Dos', list: [] }, { type: 'Donts', list: [] });
        } else if (result.length === 1) {
          if (result[0].type === 'Dos') {
            result.push({ type: 'Donts', list: [] });
          }
          if (result[0].type === 'Donts') {
            result.push({ type: 'Dos', list: [] });
          }
        }
        this.DosdArrayList.push({ unitInfo: item['unitInfo'], list: result })
        // console.log("result", this.DosdArrayList)
      })

    }
  }


  viewMediaInfo() {
    if (this.flatMediaList && this.flatMediaList.length > 0) {
      this.flatMediaList.forEach(item => {
        item['checked'] = item['isSelect'];
        let format = item['keyName'] ? item['keyName'].split('.')[1] : '';
        item['format'] = format;
      });
    } else {
      this.flatMediaList = [];
    }
    // console.log("flatMediaList", this.flatMediaList)

    const medialist = _.chain(this.flatMediaList)
      .groupBy('type')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['mediaType', 'media'], currentItem);
      })
      .value();
    this.mediaList = medialist;
    // console.log("mediaList", this.mediaList)
  }


  tagListAdd(tagInfoList, tagID, LinkIDList?: any) {
    this.viewDosDont()
    /*tagInfoList.medsInfo.forEach((meds) => {
      meds.tagID = tagID;
      this.medsList.push(meds);
    });*/
    const result = _.chain(this.summaryList['dosDontsInfo'])
      .groupBy('type')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['type', 'list'], currentItem);
      })
      .value();

    if (result.length === 0) {
      result.push({ type: 'Dos', list: [] }, { type: 'Donts', list: [] });
      // result[0].type = 'Dos';
      // result[1].type = 'Donts';
    } else if (result.length === 1) {
      if (result[0].type === 'Dos') {
        result.push({ type: 'Donts', list: [] });
      }
      if (result[0].type === 'Donts') {
        result.push({ type: 'Dos', list: [] });
      }
    }
    // this.DosdArrayList.push({ unitInfo: tagInfoList.unitInfo, list: result, tagID: tagID });

    // const found = result.findIndex(element => element.type === 'Donts');
    // const foundDos = result.findIndex(element => element.type === 'Dos');
    // // this.DosLists = [];
    // // this.DontsList = [];
    // if (foundDos >= 0) {
    //   // this.DosLists = result[foundDos].list;
    //   result[foundDos].list.forEach((dos) => {
    //     dos.tagID = tagID;
    //     this.DosLists.push(dos);
    //   });
    // }
    // if (found >= 0) {
    //   // this.DontsList = result[found].list;
    //   result[found].list.forEach((dos) => {
    //     dos.tagID = tagID;
    //     this.DontsList.push(dos);
    //   });
    // }

    tagInfoList.mediaInfo.forEach((media) => {
      media.tagID = tagID;
      media.checked = false;

      if (LinkIDList) {
        LinkIDList.forEach((linkmedia) => {
          linkmedia.mediaId = +linkmedia.mediaId;

          if (linkmedia.mediaId === media.id) {
            media.checked = true;
          }

        });
      }
      let format = media['keyName'] ? media['keyName'].split('.')[1] : '';
      media['format'] = format;

      this.flatMediaList.push(media);
    });

    const medialist = _.chain(this.flatMediaList)
      .groupBy('type')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['mediaType', 'media'], currentItem);
      })
      .value();
    this.mediaList = medialist;


  }

  openTab(id) {
    if (this.isNotesEdit) {
      this.toastr.error("You have unsaved notes. Please save it to proceed further");
    } else {
      this.tabs = id;
    }
    /*if (this.isSummary || this.seletedTagIds.length > 0) {
      
    }*/
    /*if(id === 2) {
      if(this.notesSummary.length > 0) {
        this.notesSummary.forEach(item => this.notesInfo.push(item.summary));
      }
    }*/
  }

  openAddMedication(createOrg: TemplateRef<any>) {
    this.modalRef = this.modalService.show(createOrg, { class: 'modal-lg incareview-model' });
  }

  close() {
    this.modalRef.hide();
  }
  createPage() {

  }



  getTags() {
    this.covidService.getTags('').subscribe(tags => {
      this.tagsList = tags.result.tagList;

      if (this.tagsList) {
        this.tagsList.forEach((tag) => {
          this.items.push({
            name: tag.name, value: '@' + tag.name, display: '@' + tag.name,
            id: tag.id, summary: tag.summary
          });
        });
      }


      // { display: '@Pizza', value: '@Pizza', name: 'Pizza' },
    });
  }

  getPatientDetails() {
    this.userService.getUserById(this.patientID).subscribe((user) => {
      this.patientDetails = user.result;
    });
  }

  closeDosDonts(type, DosandList, params, message) {
    if (params.summaryId) {
      this.getCurrentSummary();
    } else {
      this.redirectPatientID = this.patientID;
      this.getHistory();
    }
    if (DosandList.type === 'Donts') {
      this.closeDonts(DosandList);
    } else {
      this.closeDos(DosandList);
    }
    this.toastr.success(type + message);
  }

  createDosDonts(params, type, DosandList) {
    this.covidService.createDos(params).subscribe((dos: any) => {
      this.closeDosDonts(type, DosandList, params, ' Created Successfully');
    }, (err) => {
      if (err.error.message) {
        this.toastr.error(err.error.message);
      } else {
        this.toastr.error(err.error.err.message);
      }
    });
  }

  updateDosDonts(params, type, DosandList) {
    this.covidService.UpdateDos(this.dosDonts['id'], params).subscribe((dos: any) => {
      this.getPatientSummary();
      this.closeDosDonts(type, DosandList, params, ' Updated Successfully');
    }, (err) => {
      if (err.error.message) {
        this.toastr.error(err.error.message);
      } else {
        this.toastr.error(err.error.err.message);
      }
    });
  }


  addDos(DosandList) {
    let type = DosandList && DosandList.type ? DosandList.type : DosandList;
    if (this.DosMessage || this.DontsMessage) {
      const params: any = {};
      params.summaryId = this.summaryList && this.summaryList.id ? this.summaryList.id : undefined;
      //params.tagId = this.selectedTagID;
      params.createdFor = this.patientID;
      if (DosandList.type === 'Donts' || DosandList === 'Donts') {
        params.message = this.DontsMessage;
      } else {
        params.message = this.DosMessage;
      }
      params.type = type;
      params.nodeId = this.nodeID;
      params.uId = this.uId;
      if (this.dosDonts && this.dosDonts['id']) {
        params.nodeId = this.dosDonts['nodeId'];
        params.uId = this.dosDonts['uId'];
        params.specialTagId = this.dosDonts['tagId'];
        params.summaryId = this.dosDonts['summaryId'];
        this.updateDosDonts(params, type, DosandList);
      } else {
        this.createDosDonts(params, type, DosandList)
      }
    } else {
      this.toastr.error(type + ' Message is Required');
    }
  }

  keyDownFunction(event, type) {
    if (event.keyCode == 13) {
      if (this.DosMessage || this.DontsMessage) {
        this.addDos(type);
      }
    }
  }

  dosDontsFocus() {
    this.showBottomNav = false
  }

  dosDontsFocusOut() {
    this.showBottomNav = true
  }

  getMediaList() {
    this.covidService.getMedia().subscribe((media) => {

      this.mediaList = media.result;
    });
  }

  getMeds() {
    this.covidService.getMeds().subscribe((meds: any) => {
      this.medsList = meds.result.medList;
    });

  }

  getMyorg() {

    this.myOrgService.getMyOrg().subscribe((nodes: any) => {
      if (nodes.result[0]) {

        this.nodeID = nodes.result[0].nodeId;
        this.uId = nodes.result[0].uId;

      }
    });

  }

  getDos() {
    this.covidService.getDos().subscribe((dos: any) => {

      const result = _.chain(dos.result.DosDontsList)
        .groupBy('type')
        .toPairs()
        .map(function (currentItem) {
          return _.zipObject(['type', 'list'], currentItem);
        })
        .value();
      const found = result.findIndex(element => element.type === 'Donts');
      const foundDos = result.findIndex(element => element.type === 'Dos');
      this.DosLists = [];
      this.DontsList = [];
      if (foundDos >= 0) {
        this.DosLists = result[foundDos].list;
      }
      if (found >= 0) {
        this.DontsList = result[found].list;
      }
    });
  }

  deleteDos(type, value, tag) {
    this.covidService.deleteDos(value, this.nodeID).subscribe((dos) => {
      if (dos.result.status === 400) {
        this.toastr.error(dos.result.err);
      } else {

        const found = this.DosdArrayList.findIndex(element => element.tagID === tag.tagID);
        if (found >= 0) {
          const foundType = this.DosdArrayList[found].list.findIndex(element => element.type === type);
          if (foundType >= 0) {
            const index = this.DosdArrayList[found].list[foundType].list.findIndex(element => element.id === value);
            if (index >= 0) {
              this.DosdArrayList[found].list[foundType].list.splice(index, 1);
            }

          }

        }

        this.toastr.success(type + ' Deleted Successfully');
      }

      // this.getDos();
      // this.getCurrentSummary();
    });

  }

  onHidden(): void {
    // console.log('Dropdown is hidden');
    this.dontScroll = false;
  }
  onShown(): void {
    // console.log('Dropdown is shown');
    // this.dontScroll = true;
  }
  isOpenChange(): void {
    // console.log('Dropdown state is changed');
  }

  /*deleteMedia(media) {
    this.mediaService.deleteMedia(media.id).subscribe((data) => {
      this.toastr.success(data.result.message);
      this.getMediaList();
    });
  }*/

  updateMedia(media) {
    this.router.navigate(['/app/media-edit', media.id]);
  }

  gotoaddMed() {
    this.editMedsID = null;
    this.addMedSummaryId = this.summaryId;
    if (window.innerWidth < 673) {
      let redirectUrl = this.location.path();
      const removeOldUrl = this.router.url.substring(0, this.router.url.indexOf("?"));
      redirectUrl = (removeOldUrl) ? removeOldUrl : redirectUrl;
      // console.log(redirectUrl);
      let summaryId = this.summaryId;
      let patientID = this.patientID;

      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/app/add-medication'], {
          queryParams: {
            redirectUrl,
            summaryId,
            patientID,
            tabs: this.tabs
          }
        }
        )
      );
    } else {
      this.modalRef = this.modalService.show(this.addSummaryMedicationModal, { backdrop: 'static', keyboard: false, class: 'modal-lg incareview-model org-window' });
    }
  }

  editMeds(medList) {
    this.addMedSummaryId = this.summaryId;
    this.editMedsID = medList.id;
    if (window.innerWidth < 673) {
      let redirectUrl = this.location.path();
      const removeOldUrl = this.router.url.substring(0, this.router.url.indexOf("?"));
      redirectUrl = (removeOldUrl) ? removeOldUrl : redirectUrl;
      // console.log(redirectUrl);
      let summaryId = this.summaryId;
      let patientID = this.patientID;
      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/app/edit-medication', this.editMedsID], {
          queryParams: {
            redirectUrl,
            summaryId,
            patientID,
            tabs: this.tabs,
            medId: this.editMedsID
          }
        }
        )
      );
    } else {
      this.modalRef = this.modalService.show(this.addSummaryMedicationModal, { backdrop: 'static', keyboard: false, class: 'modal-lg' });
    }
  }


  gotoAddCare() {
    const redirectUrl = this.location.path();
    this.router.navigateByUrl(
      this.router.createUrlTree(
        ['/app/add-careteam'], {
        queryParams: {
          node: this.nodeID,
          redirectUrl
        }
      }
      )
    );
  }

  deleteMedsInfo() {
    if (this.selectMeds && this.selectMeds['id']) {
      this.covidService.deleteMeds(this.selectMeds['id'], this.nodeID).subscribe((data) => {
        this.modalRef.hide();
        this.selectMeds = {};
        this.getCurrentSummary();
        if (data.result.message) {
          this.toastr.success(data.result.message);
        } else {
          this.toastr.error(data.result.err);
        }
      });
    }
  }

  cancelMeds() {
    this.selectMeds = {};
    this.modalRef.hide();
  }

  deleteMeds(confirmMedDelete: TemplateRef<any>, medList) {
    this.selectMeds = medList;
    this.modalRef = this.modalService.show(confirmMedDelete, { class: 'modal-lg incareview-model incare-window' });
  }

  findChoices(searchText: string) {


    this.tagsList.filter(item =>
      item.name.toLowerCase().includes(searchText.toLowerCase())
    );
    return this.tagsList;
  }

  getChoiceLabel(choice: any) {
    this.covidService.selectedTag.next(JSON.stringify(choice));
    return `@${choice.name} `;
  }



  mediaChange(type, media) {
    const mediaID = media.id;
    const found = this.mediaIDs.findIndex(element => element === mediaID);
    const index = this.medialinkIds.findIndex(element => +element.mediaId === mediaID);
    const deleteindex = this.deleteMediaIDs.findIndex(element => +element === mediaID);
    if (type) {
      if (found < 0 && index < 0) {
        this.mediaIDs.push(mediaID);
      }
      if (deleteindex >= 0) {
        this.deleteMediaIDs.splice(deleteindex, 1);
      }

    } else {
      if (found >= 0) {
        const ID = this.mediaIDs.splice(found, 1);
      }
      if (index >= 0 && deleteindex < 0) {
        this.deleteMediaIDs.push(mediaID);
      }
      // else {
      //   // const index = this.medialinkIds.findIndex(element => +element === mediaID);
      //   if (index >= 0) {
      //     this.medialinkIds.splice(index, 1);
      //   }
      // }

    }
    this.addMedia();

  }

  addMedia() {
    if (this.mediaIDs.length > 0) {
      const params: any = {};
      params.createdFor = this.patientID;
      params.nodeId = this.nodeID;
      params.uId = this.uId;
      params.mediaId = this.mediaIDs;
      params.summaryId = this.summaryList.id;

      this.covidService.addPatientMedia(params).subscribe((list) => {
        this.toastr.success('Media Selected Successfully');
        // this.modalRef.hide();
        // this.modalService.hide(1);
        // let mediaList: any = [];
        // mediaList = [...this.mediaIDs, ...this.medialinkIds];
        // this.addSummaryMedia(mediaList);
        this.isAddMedia = false;
        // this.getPatientSummary();
        if (window.innerWidth < 673) {
          this.isAddMedia = false;
        } else {
          if (this.modalRef) this.modalRef.hide();
        }
        this.getCurrentSummary();
      });


    }
    // else {
    //   this.toastr.error('Please Select Any One Media!');
    // }

    if (this.deleteMediaIDs.length > 0) {
      this.deletelinkMedia();
    }
  }

  deletelinkMedia() {
    let deletedLinkId = [];
    if (this.medialinkIds && this.medialinkIds.length > 0) {
      this.deleteMediaIDs.forEach(key => {
        this.medialinkIds.forEach(item => {
          if (key === item['mediaId']) {
            deletedLinkId.push(item['id']);
          }
        })
      })
    }
    // if (this.mediaIDs.length > 0) {
    const params: any = {};
    params.createdFor = this.patientID;
    params.nodeId = this.nodeID;
    params.uId = this.uId;
    params.mediaLinkId = deletedLinkId;
    params.summaryId = this.summaryList.id;

    this.covidService.deletePatientMedia(params).subscribe((list) => {
      this.isAddMedia = false;
      this.toastr.success('Media Un-Selected Successfully');
      this.getCurrentSummary();

      if (window.innerWidth < 673) {
        this.isAddMedia = false;
      } else {
        //this.modalRef.hide();
      }
      // let mediaList: any = [];
      // mediaList = [...this.mediaIDs, ...this.medialinkIds];
      // this.addSummaryMedia(mediaList);
      // this.getPatientSummary();
    });


    // } else {
    //   this.toastr.error('Please Select Any One Media!');
    // }
  }

  addSummaryMedia(mediaList) {
    const params1: any = {};
    params1.createdFor = this.patientID;
    params1.specialTagId = this.seletedTagIds;
    params1.nodeId = this.nodeID;
    params1.uId = this.uId;
    params1.medialinkId = mediaList;
    if (this.isSummary) {
      this.covidService.updateSummary(this.summaryList.id, params1).subscribe((summary) => {
        this.isAddMedia = false;
        this.getPatientSummary();
      }, (err) => {
        this.toastr.error(err.error.err);
        this.getPatientSummary();
      });
    }
  }

  gotoBack() {
    this.isAddMedia = false;
  }

  gotoAddMediaList() {
    // console.log(this.flatMediaList);
    this.isAddMedia = true;
    if (window.innerWidth < 673) {
      this.isAddMedia = true;
    } else {
      this.isAddMedia = false;
      this.modalRef = this.modalService.show(this.addVideoModal, { class: 'modal-md' });
    }
    // const redirectUrl = this.location.path();
    // this.router.navigate(['/app/add-media-list'], {
    //   queryParams: {
    //     redirectUrl
    //   }
    // });
  }

  private startsWithAt(control: FormControl) {
    if (control.value.charAt(0) !== '@') {
      return {
        'startsWithAt@': true
      };
    }

    return null;
  }

  onTextChange(event) {
    // Do Nothing...
  }
  onItemAdded(event) {
    this.getTagDetails(event.id);
    const found = this.seletedTagIds.findIndex(element => element === event.id);
    if (found < 0) {
      this.tagId = event.id;
      this.seletedTagIds.push(event.id);
      this.notesSummary.push({ id: event.id, summary: event.summary });
      this.notesInfo.push(event.summary);
      this.createSummaryList();
    }
  }

  onItemRemoved(event) {
    this.removeTagData(event.id);
    const found = this.seletedTagIds.findIndex(element => element === event.id);
    //if (found >= 0 && this.seletedTagIds.length > 1) {
    this.seletedTagIds.splice(found, 1);
    this.notesSummary.splice(found, 1);
    if (this.notesInfo && this.notesInfo.length > 0) {
      let notesInfo = this.notesInfo.toString();
      this.notesInfo = [];
      let removedNote = notesInfo.replace(event.summary, ' ');
      this.notesInfo.push(removedNote);
    }
    this.tagId = event.id;
    this.removeTag = event.id;
    console.log(this.seletedTagIds)
    this.createSummaryList();
    /*} else {
      this.toastr.error('Minimum One Tag Requried');
      this.getPatientSummary();
    }*/
  }

  createSummaryList() {
    const params: any = {};
    params.createdFor = this.patientID;
    params.specialTagId = this.seletedTagIds;
    params.nodeId = this.nodeID;
    params.uId = this.uId;
    if (this.notesInfo && this.notesInfo.length > 0) params.notes = this.notesInfo.toString();
    if (this.isSummary) {
      if (this.removeTag) params['removeTag'] = this.removeTag;
      this.covidService.updateSummary(this.summaryList.id, params).subscribe((summary) => {
        this.removeTag = undefined;
        this.getCurrentSummary();
      }, (err) => {
        this.toastr.error(err.error.err);
        this.getPatientSummary();
      });
    } else {
      this.covidService.createSummary(params).subscribe((summary) => {
        this.summaryId = summary.result['summary']['id'];
        this.getPatientSummary();
      });

    }

  }

  onInputFocused(event) {
  }


  removeTagData(tagID) {
    this.DosdArrayList.forEach((DosArray, index) => {
      if (DosArray.tagID === tagID) {
        this.DosdArrayList.splice(index, 1);
      }
    });

    this.DosLists.forEach((dos, index, object) => {
      if (dos.tagID === tagID) {
        this.DosLists.splice(index, 1);
      }
    });

    this.DosLists.forEach((dos, index, object) => {
      if (dos.tagID === tagID) {
        this.DosLists.splice(index, 1);
      }
    });

    this.DontsList.forEach((donts, index, object) => {
      if (donts.tagID === tagID) {
        object.splice(index, 1);
      }
    });

    this.medsList.forEach((meds, index, object) => {
      if (meds.tagID === tagID) {
        object.splice(index, 1);
      }
    });

    if (this.flatMediaList && this.flatMediaList.length > 0) {
      this.flatMediaList.forEach((media, index, object) => {
        if (media.tagID === tagID) {
          object.splice(index, 1);
        }
      });
    } else {
      this.flatMediaList = [];
    }

    const medialist = _.chain(this.flatMediaList)
      .groupBy('type')
      .toPairs()
      .map(function (currentItem) {
        return _.zipObject(['mediaType', 'media'], currentItem);
      })
      .value();
    this.mediaList = medialist;
  }


  publishSummary() {
    this.isAllowCreate = !this.isAllowCreate;
    this.covidService.publishSummary(this.summaryList.id).subscribe((data) => {
      if (!this.isAllowCreate) this.toastr.success(data.result.message);
      this.getCurrentSummary();
    });
  }

  changeAllow() {
    this.isAllowCreate = !this.isAllowCreate;
  }

  editNotes() {
    this.editNote = !this.editNote;
    this.isFirstOpen = true;

  }

  createNotes() {
    if (this.notesInfo.toString() === '<br>' || this.notesInfo[0] === '<p style="margin-bottom: 1rem;"><br></p>' || this.notesInfo[0] === '<br>') {
      this.notesInfo = [];
    }

    if (this.notesInfo.length > 0 && !this.summarylengthexceed) {
      this.summaryrequired = false;
      this.isNotesEdit = false;
      let noteInfo = this.notesInfo;
      this.notesInfo = [];
      this.notesInfo.push(noteInfo.toString());
      this.createSummaryList();
      this.editNote = !this.editNote;
    } else if (this.notesInfo.length < 1) {
      this.summaryrequired = true;
    }


  }

  redirectPage(id) {
    const redirectUrl = this.location.path();
    this.router.navigateByUrl(
      this.router.createUrlTree(['/app/media-ivideo/' + id], { queryParams: { redirectUrl } })
    );
  }

  autoUpdateSummaryDosDonts(tagId, tagInfo) {
    if (tagId && tagInfo.length > 0) {
      let index = tagInfo.findIndex(obj => obj.id === tagId);
      this.tagId = undefined;
      if (index > -1) {
        let dosdontsId = tagInfo[index]['dosDontsInfo'];
        if (dosdontsId && dosdontsId.length > 0) {
          let dosdonts = [];
          dosdontsId.forEach(item => dosdonts.push(item['id']));
          this.dosDontsIds = this.dosDontsIds && this.dosDontsIds.length > 0 ? this.dosDontsIds : [];
          this.dosDontsIds = this.dosDontsIds.concat(dosdonts);
          this.dosDontsIds = _.uniq(this.dosDontsIds);
          this.updateDosAddLink()
        }
      } else {
        let newDosdonts = [];
        tagInfo.forEach((item, i) => {
          let dosdontsId = item['dosDontsInfo'];
          if (dosdontsId && dosdontsId.length > 0) {
            dosdontsId.forEach(key => {
              if (this.dosDontsIds && this.dosDontsIds.length > 0) {
                let index = this.dosDontsIds.findIndex(obj => obj === key['id'].toString());
                if (index > -1) newDosdonts.push(key['id']);
              }
            })

          }
        })
        this.dosDontsIds = newDosdonts;
        this.updateDosAddLink();

      }
    }
  }

  showSummary(summaryId) {
    window.scrollTo(0, 0)
    this.summaryId = summaryId;
    this.getPatientSummary();
  }

  hideModal() {
    this.modalRef.hide();
    this.modalRef = this.modalService.show(this.mediaFormModal, { backdrop: 'static', keyboard: false, class: 'modal-lg incareview-model org-window' });
  }

  hideModals() {
    this.modalRef.hide();
    this.redirectPatientID = this.patientID;
    if (this.summaryId) {
      this.getPatientSummary();
    } else {
      this.getHistory();
    }
  }

  gotoAddMedia() {
    if (window.innerWidth < 673) {
      const redirectUrl = this.location.path();
      this.router.navigateByUrl(
        this.router.createUrlTree(
          ['/app/media'], {
          queryParams: {
            redirectUrl,
            summaryId: this.summaryId,
            patientID: this.patientID,
            tabs: this.tabs
          }
        }
        )
      );
    } else {
      this.modalRef = this.modalService.show(this.createMediaModal, { class: 'modal-lg' });

    }

  }

  getInnerHTML(val) {
    val = val.toString();
    return val.replace(/(<([^>]+)>)/ig, '');
  }

  deleteMedia(confirmMediaDelete: TemplateRef<any>, media) {
    this.selectMedia = media
    this.modalRef = this.modalService.show(confirmMediaDelete, { class: 'modal-lg incareview-model incare-window' });
  }

  deleteMediaInfo() {
    if (this.selectMedia && this.selectMedia['id']) {
      this.covidService.deleteMedia(this.selectMedia['id']).subscribe((data) => {
        if (data.result.message) {
          this.toastr.success(data.result.message);
        } else {
          this.toastr.error(data.result.err);
        }
        this.modalRef.hide();
        this.selectMedia = {};
        this.getCurrentSummary();
      });
    }
  }

  cancelDelete() {
    this.selectMedia = {};
    this.modalRef.hide();
  }

  editDosDonts(type, value, array) {
    this.dosDonts = value;
    this.isEdit = true;
    if (type === 'Dos') {
      array.showDos = true;
      array.showDonts = false;
      this.showDontsInstruction = false;
      this.DosMessage = value.message;
    }
    if (type === 'Donts') {
      array.showDonts = true;
      array.showDos = false;
      this.showDoInstruction = false;
      this.DontsMessage = value.message;
    }
  }

  medsAddLink(isChecked, medsId) {
    const found = this.medsIds.findIndex(element => element.toString() === medsId.toString());
    if (isChecked) {
      if (found === -1) {
        this.medsIds.push(medsId);
      }
    } else {
      if (found > -1) {
        this.medsIds.splice(found, 1);
      }
    }
    this.medsIds = _.uniq(this.medsIds, 'id');
    this.updateDosAddLink();
  }


  onContentChange(value) {
    this.isNotesEdit = true;
    if (value && value.length > this.summaryMaximumLimit) {
      this.summarylengthexceed = true;
    } else {
      this.summarylengthexceed = false;
    }
    if (!value || value.length === 0) {
      this.summaryrequired = true;
    } else {
      this.summaryrequired = false;
    }
  }

  downloadSummary() {
    const redirectUrl = this.location.path();
    this.router.navigateByUrl(
      this.router.createUrlTree(['/app/download/'], { queryParams: { redirectUrl, tabs: this.tabs, specialSummaryId: this.summaryId, patientId: this.patientID } })
    );
  }

}