import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidtagmanagementComponent } from './covidtagmanagement.component';

describe('CovidtagmanagementComponent', () => {
  let component: CovidtagmanagementComponent;
  let fixture: ComponentFixture<CovidtagmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidtagmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidtagmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
