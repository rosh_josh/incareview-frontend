import { Component, Input, Output, TemplateRef, OnInit, EventEmitter, OnChanges, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MyOrgService } from '../services/my-org.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserViewComponent implements OnInit, OnChanges {
  @ViewChild('createDept') public createDept: TemplateRef<any>;
  @Input() treeData = [];
  @Input() emptyOrg = false;
  @Input() isMulti = false;
  @Input() seletedNode;
  @Input() mobileView;
  @Input() isFirst = false;
  childData;
  modalRef: BsModalRef;
  modalRefUser: BsModalRef;
  postType = '';
  postTitle = '';
  createData: any = {};

  orgForm: FormGroup;
  name: FormControl;
  email: FormControl;
  nodeId: FormControl;
  permissionId: FormControl;
  description: FormControl;
  username: FormControl;
  submitted = false;
  deleteID;
  deleteOrgName = '';
  deleteUserName = '';
  deleteUserEmail = '';
  deleteUserpermission = ''
  updateID;
  deleteIDs: any = [];
  currentUserID;
  currentPermission = 0;
  currentPermissionType = '';
  childNode;
  isLocation = false;
  location: FormControl;
  orgUser;
  showCreate = false;

  constructor(private modalService: BsModalService,
    private toastr: ToastrService,
    private myOrgService: MyOrgService, private router: Router) { }

  ngOnInit() {
    this.myOrgService.setOrgData(undefined);
    this.getOrgs();
    let user: any = {};
    user = JSON.parse(localStorage.getItem('userInfo'));
    this.currentUserID = user.id;
    this.createFormControls();
    this.createForm();
    this.myOrgService.createOrgEvent.subscribe((data) => {
      if (data === 2) {
        let count = this.modalService.getModalsCount();
        if(count > 1) {
          this.modalService.hide(count);
        }
        if (this.seletedNode) {
          if (this.seletedNode.puid === 1) {
            this.orgForm.reset();
            const nodeId = '' + this.seletedNode.nodeId;
        
            this.orgForm.controls['nodeId'].setValue(nodeId);
            this.postType = 'Invite';
            this.postTitle = 'Invite User';
            this.orgForm.controls['email'].enable();
            this.orgForm.controls['username'].enable();
            this.orgForm.controls['username'].setValidators([Validators.required, Validators.pattern(/^[a-zA-Z_ ]{1,}$/)]);
            this.orgForm.controls['name'].clearValidators();
            this.orgForm.controls['description'].clearValidators();
            this.orgForm.controls['location'].clearValidators();
            this.orgForm.controls['permissionId'].setValue(['READ']);
            const emailPattern = `/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$/`;
            // const emailPattern = '^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$';
            this.orgForm.controls['email'].setValidators([Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)]);
      
            this.removeSplice();
            const index = _.findIndex(this.myOrgService.childNode, { id: this.seletedNode.id });
            if (index === -1) {
              this.myOrgService.childNode.push({ id: this.seletedNode.id });
            }
            this.modalRef = this.modalService.show(this.createDept, { class: 'modal-md incareview-model' });

          }
        }
      }
    });

  }
  ngOnChanges() {

    // if (this.treeData.length > 0) {
    //   if (this.treeData[0].puid === 1) {
    //     this.showNode(this.treeData[0], true);
    //     this.orgUser = this.treeData[0]
    //   }
    // }
    // if (this.seletedNode) {
    //   const index = _.findIndex(this.treeData, { id: this.seletedNode.id });
    //   if (index !== -1) {
    //     this.showNode(this.treeData[index], true);
    //     this.myOrgService.seletedNode = null;
    //   }




    // }
    if (this.isFirst) {
      if (this.treeData.length > 0) {
        if (this.treeData[0].puid === 1) {
          this.showNode(this.treeData[0], true);
          this.orgUser = this.treeData[0]
        }
      }
    }


    // if (this.seletedNode) {
    this.myOrgService.childNode.forEach((childID) => {
      this.treeData.forEach((data) => {
        if (childID.id === data.id) {
          this.showNode(data, true);
        }

      });
    });

    // }

  }

  createFormControls() {

    this.email = new FormControl('');
    this.name = new FormControl('');
    this.nodeId = new FormControl('', [Validators.required]);

    this.permissionId = new FormControl(['READ']);
    this.description = new FormControl('');
    this.username = new FormControl('');
    this.location = new FormControl('');


  }

  createForm() {
    this.orgForm = new FormGroup({
      email: this.email,
      name: this.name,
      nodeId: this.nodeId,
      permissionId: this.permissionId,
      description: this.description,
      username: this.username,
      location: this.location
    });

  }
  get f() { return this.orgForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.orgForm.invalid) {
      return;
    }

    if (this.postType === 'Invite') {
      this.inviteUser();
    } else if (this.postType === 'Update') {
      this.updateOrg();
    }


  }

  inviteUser() {
    let params: any = {};
    params = this.orgForm.value;
    delete params.name;
    delete params.description;
    delete params.location;
    this.myOrgService.inviteUser(params).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {
        this.modalService.hide(1);
        this.orgForm.reset({ nodeId: '', email: '', permissionId: ['READ'] });
        this.toastr.error(data.result.err);
      } else {
        this.modalRef.hide();
        this.orgForm.reset({ nodeId: '', email: '', permissionId: ['READ'] });
        this.myOrgService.changeUserEvent.emit();
        this.toastr.success('User Invited successfully');
      }

    }, (err) => {

      this.toastr.error(err.error.message);
    });
  }


  updateOrg() {
    let params: any = {};
    params = this.orgForm.value;
    delete params.email;
    delete params.permissionId;
    delete params.username;
    if (!this.isLocation) {
      delete params.location;
    }
    this.myOrgService.updateOrg(this.updateID, params).subscribe((data) => {
      this.submitted = false;
      if (data.result.status) {
        this.modalService.hide(1);
        this.toastr.error(data.result.err);
      } else {
        this.modalRef.hide();
        this.orgForm.reset();
        this.myOrgService.changeUserEvent.emit();
        if (this.isLocation) {
          this.toastr.success('Organisation Updated successfully');
        } else {
          this.toastr.success('Department Updated successfully');
        }
      }

    });
  }

  showNode(node, type) {

    this.treeData.forEach((tree) => {
      tree.isOpen = false;
    });

    if (('isOpen' in node)) {
      node.isOpen = !node.isOpen;
      this.childData = node.id;
      // if (node.isOpen === false) {
      //   this.childData = '';
      // }
      if (node.isOpen === true) {
        this.childData = node.id;

      }
    } else {
      node.isOpen = true;
      this.childData = node.id;
    }
    // this.childData = node.id;
    this.deleteIDs = [];
    this.myOrgService.multiDeleteEvent.emit(this.deleteIDs);
    this.seletedNode = this.myOrgService.seletedNode = node;

    if (type) {
      if (this.myOrgService.childNode.length > 0) {
        // this.childNode = this.myOrgService.childNode[0];
        // this.myOrgService.childNode.shift();
      }

    }
    if (!this.isFirst && !type) {
      this.removeSplice();
    }

    const index = _.findIndex(this.myOrgService.childNode, { id: node.id });
    if (index === -1) {
      this.myOrgService.childNode.push({ id: node.id });
    }

    if (this.isFirst) {
      node.isOpen = true;
      this.childData = node.id;
      this.myOrgService.getOrgInfo(node.nodeId).subscribe((data) => {

        if (data.result.permissionList.length > 0) {
          node.children = data.result.permissionList[0].children;
          node.accessInfo = data.result.permissionList[0].accessInfo;
        }

      });
    }

  }


  removeSplice() {
    if (this.myOrgService.childNode.length > 0) {
      let deleteIndex = -1;
      this.myOrgService.childNode.forEach((nodeID, place) => {
        this.treeData.forEach((childData) => {
          if (nodeID.id === childData.id) {
            deleteIndex = place;
          }
        });
      });

      if (deleteIndex >= 0) {
        this.myOrgService.childNode.splice(deleteIndex, this.myOrgService.childNode.length - deleteIndex);
      }


    }
  }

  createDeptModal(createDept: TemplateRef<any>, type, data) {

    // if (!this.myOrgService.seletedNode) {
    //   this.myOrgService.seletedNode = data;
    // }
    // if (data.puid === 1) {
    //   this.myOrgService.seletedorgNode = data;
    // }



    this.orgForm.reset();
    const nodeId = '' + data.nodeId;

    this.orgForm.controls['nodeId'].setValue(nodeId);
    this.createData = data;
    if (type === 'edit') {
      this.postType = 'Update';
      this.postTitle = 'Update Department';

      this.updateID = data.nodes.id;
      this.orgForm.controls['email'].clearValidators();
      this.orgForm.controls['username'].clearValidators();
      this.orgForm.controls['location'].clearValidators();
      data.nodes.name ? this.orgForm.controls['name'].setValue(data.nodes.name) : -1;
      data.nodes.description ? this.orgForm.controls['description'].setValue(data.nodes.description) : -1;
      this.orgForm.controls['name'].setValidators([Validators.required, Validators.minLength(1), Validators.pattern(/^[a-zA-Z_ ]{1,}$/)]);
      this.orgForm.controls['description'].setValidators([Validators.required]);
      if (data.puid === 1) {
        this.isLocation = true;
        this.orgForm.controls['location'].setValidators([Validators.required]);
        this.postTitle = 'Update Organisation';
        data.nodes.location ? this.orgForm.controls['location'].setValue(data.nodes.location) : -1;
      } else {
        this.isLocation = false;
      }

    } else {
      this.postType = 'Invite';
      this.postTitle = 'Invite User';
      this.orgForm.controls['email'].enable();
      this.orgForm.controls['username'].enable();
      this.orgForm.controls['username'].setValidators([Validators.required, Validators.pattern(/^[a-zA-Z_ ]{1,}$/)]);
      this.orgForm.controls['name'].clearValidators();
      this.orgForm.controls['description'].clearValidators();
      this.orgForm.controls['location'].clearValidators();
      this.orgForm.controls['permissionId'].setValue(['READ']);
      const emailPattern = `/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-za-z]{2,4}$/`;
      // const emailPattern = '^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$';
      this.orgForm.controls['email'].setValidators([Validators.required, Validators.pattern(/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-za-z]{2,4}$/)]);

      this.removeSplice();
      const index = _.findIndex(this.myOrgService.childNode, { id: data.id });
      if (index === -1) {
        this.myOrgService.childNode.push({ id: data.id });
      }
    }

    if (this.mobileView) {
      data['postType'] = this.postType;
      data['postTitle'] = 'Create User';
      data['isLocation'] = this.isLocation;
      this.myOrgService.setOrgData(data);
      if (this.postType === 'Invite') {
        this.router.navigate(['/create-user']);
      } else {
        this.router.navigate(['/create-dept']);
      }

    } else {
      this.modalRef = this.modalService.show(createDept, { class: 'modal-md incareview-model' });
    }
  }

  closeModal() {
    this.modalRef.hide();
    this.orgForm.reset({ nodeId: '', email: '', permissionId: ['READ'] });
    this.submitted = false;
    this.myOrgService.seletedNode = null;
  }
  deleteUserModal(deleteuser: TemplateRef<any>, data, node) {
    this.currentPermission = this.deleteID = data.permissionId;
    data.user.firstName ? this.deleteUserName = data.user.firstName + ' ' + data.user.lastName : -1;
    data.user.username ? this.deleteUserName = data.user.username : -1;
    this.deleteUserEmail = data.user.email;
    this.deleteUserpermission = data.user.permissionId;
    this.currentPermissionType = data.permissionType;
    this.deleteOrgName = node.name;
    this.modalRefUser = this.modalService.show(deleteuser, { class: 'modal-lg incareview-model' });
    this.removeSplice();
    const index = _.findIndex(this.myOrgService.childNode, { id: data.id });
    if (index === -1) {
      this.myOrgService.childNode.push({ id: data.id });
    }

  }
  deleteUserModal1(deleteuser: TemplateRef<any>, data) {
    this.deleteID = data.id;
    this.deleteOrgName = data.name;
    this.modalRefUser = this.modalService.show(deleteuser, { class: 'modal-lg incareview-model incare-window' });
  }
  inviteUserModal(inviteUser: TemplateRef<any>) {
    this.modalRefUser = this.modalService.show(inviteUser, { class: 'modal-lg incareview-model' });
  }
  paymentModal(payment: TemplateRef<any>) {
    this.modalRefUser = this.modalService.show(payment, { class: 'modal-lg incareview-model' });
  }
  deleteConfirmModel(confirmDelete: TemplateRef<any>) {
    this.modalRef = this.modalService.show(confirmDelete, { class: 'modal-lg incareview-model incare-window' });
    this.modalRefUser.hide();
  }
  createOrgModal(createOrg: TemplateRef<any>) {
    this.modalRef = this.modalService.show(createOrg, { class: 'modal-lg incareview-model org-window' });
  }
  cancelDelete() {
    this.modalService.hide(1);
    this.currentPermission ? this.currentPermission = null : -1;
    document.body.classList.remove('modal-open');
  }


  deleteUser() {
    if (this.deleteID) {
      this.deleteuser(['' + this.deleteID]);
    }

  }


  deleteOrg() {
    if (this.deleteID) {
      this.myOrgService.deleteOrg(this.deleteID).subscribe((org) => {
        this.deleteID = '';
        if (org.result.status) {
          this.modalService.hide(1);
          document.body.classList.remove('modal-open');
          this.toastr.error(org.result.err);
        } else {

          this.modalService.hide(1);
          document.body.classList.remove('modal-open');
          this.toastr.success(this.deleteOrgName + ' deleted successfully');
          this.myOrgService.changeUserEvent.emit();

        }

      });
    }

  }

  multiDelete(checked, accessInfo) {
    if (checked) {
      this.deleteIDs.push('' + accessInfo.permissionId);

    } else {
      this.deleteIDs.splice(this.deleteIDs.indexOf('' + accessInfo.permissionId), 1);
    }

    this.myOrgService.multiDeleteEvent.emit(this.deleteIDs);

  }

  deleteuser(deleteIDs) {
    this.myOrgService.deleteUser({ permissionId: deleteIDs }).subscribe((org: any) => {
      this.deleteID = '';
      if (org.result.status) {
        this.modalService.hide(1);
        document.body.classList.remove('modal-open');
        this.toastr.error(org.result.err);
      } else {

        this.modalService.hide(1);
        document.body.classList.remove('modal-open');

        let deleteName = '';
        this.deleteUserName ? deleteName = this.deleteUserName : deleteName = this.deleteUserEmail;
        this.toastr.success(deleteName + ' deleted successfully from ' + this.deleteOrgName);
        this.myOrgService.changeUserEvent.emit();
        this.removeSplice();
        const index = _.findIndex(this.myOrgService.childNode, { id: this.childData });
        if (index === -1) {
          this.myOrgService.childNode.push({ id: this.childData });
        }

      }

    });
  }

  permissionChange(change) {
    if (change) {
      this.orgForm.controls['permissionId'].setValue(['READ', 'CREATE', 'UPDATE', 'DELETE', 'SHARE']);
    } else {
      this.orgForm.controls['permissionId'].setValue(['READ']);
    }

  }

  updatePermission(accessInfo, updateUser, uID) {
    this.currentPermission = accessInfo.permissionId;
    this.orgForm.controls['email'].disable();
    this.orgForm.controls['username'].disable();
    this.orgForm.controls['email'].setValue(accessInfo.user.email);
    this.orgForm.controls['name'].setValue(uID);
    this.orgForm.controls['username'].setValue(accessInfo.user && accessInfo.user.username ? accessInfo.user.username : accessInfo.user.firstName+' '+accessInfo.user.lastName);
    this.orgForm.controls['nodeId'].setValue(accessInfo.nodeId);
    this.orgForm.controls['permissionId'].setValue(accessInfo.permissionType);
    if(this.mobileView) {
      let data = {email: accessInfo.user.email, 
      username: accessInfo.user.username, nodeId: accessInfo.nodeId,
      permissionId: accessInfo.permissionType, uId: uID, id: this.currentPermission}
      data['postType'] = 'Update';
      data['postTitle'] = 'Edit User';
      this.myOrgService.setOrgData(data);
      this.router.navigate(['/create-user']);
    } else {
      this.modalRefUser = this.modalService.show(updateUser, { class: 'modal-lg incareview-model' });
    }    
  }

  onDelete() {
    this.submitted = true;
    if (this.orgForm.invalid) {
      return;
    }
    let params: any = {};
    params = this.orgForm.getRawValue();
    params.uId = params.name.toString();
    params.nodeId = params.nodeId.toString();
    delete params.name;
    delete params.description;
    delete params.username;
    delete params.location;

    this.myOrgService.updatePermission(this.currentPermission, params).subscribe((data) => {
      this.currentPermission = 0;
      this.submitted = false;
      if (data.result.status) {
        this.modalRefUser.hide();
        this.toastr.error(data.result.err);
      } else {
        this.modalRefUser.hide();
        this.myOrgService.changeUserEvent.emit();
        this.toastr.success('User Permission Updated successfully');
        this.removeSplice();
        const index = _.findIndex(this.myOrgService.childNode, { id: this.childData });
        if (index === -1) {
          this.myOrgService.childNode.push({ id: this.childData });
        }
      }

    }, (err) => {
      this.modalRefUser.hide();
      this.toastr.error(err.error.message);
    });
  }

  closeUser() {
    this.currentPermission ? this.currentPermission = null : -1;
    this.modalRefUser.hide();
  }

  getOrgs() {
    this.myOrgService.getUserPermission().subscribe((data) => {
      let permissionList = data.result.permissionList;
       if(permissionList && permissionList.length > 0) {
         let mypermission = permissionList[0];
         let myActions = mypermission['myActions'];
         this.showCreate = (myActions['isCreate'] === true ||
          myActions['isCreate'] === 'true') ? true : false;
       }
    });
  }

}