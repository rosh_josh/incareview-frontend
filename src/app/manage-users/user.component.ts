import { Component, OnInit, Input, TemplateRef, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { TreeModel } from 'ng2-tree';
import { Observable } from 'rxjs/Observable';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MyOrgService } from '../services/my-org.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-users',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit,AfterViewChecked {
  isMulti = false;
  @Input() mobileView;
  modalRef: BsModalRef;
  modalRefUser: BsModalRef;
  myTree: any = [];
  orgForm: FormGroup;
  name: FormControl;
  email: FormControl;
  location: FormControl;
  description: FormControl;
  // nodeId: FormControl;
  // permissionId: FormControl;
  submitted = false;
  emptyOrg = true;
  searchMobileToggle = true;
  deleteIDs: any = [];
  isDeleteIds = false;
  hideMail = true;
  seletedNode;
  userType;
  isFirst = false;
  
  constructor(private modalService: BsModalService,
    private toastr: ToastrService,
    private cdRef : ChangeDetectorRef,
    private myOrgService: MyOrgService) {


  }

  toggleSearchMobile() {
    this.searchMobileToggle = !this.searchMobileToggle;
  }

  createOrgModal(createOrg: TemplateRef<any>) {
    this.orgForm.reset();
    this.modalRef = this.modalService.show(createOrg, { class: 'modal-lg incareview-model org-window' });
    let user: any = {};
    if (localStorage.getItem('userInfo')) {
      user = JSON.parse(localStorage.getItem('userInfo'));

      if (user.id !== 1) {
        this.orgForm.controls['email'].setValue(user.email);
        this.orgForm.controls['email'].disable();
        this.hideMail = false;


      }
    }
  }



  ngOnInit() {

    let user = JSON.parse(localStorage.getItem('userInfo'));
    this.userType = user.userType;

    this.createFormControls();
    this.createForm();
    this.getOrgs();

    this.myOrgService.changeUserEvent.subscribe((data) => {
  
      this.getOrgs();
    });
    this.myOrgService.multiDeleteEvent
      .subscribe((data) => {
        // console.log(data);
        if (data.length > 0) {

          this.deleteIDs = data;
          this.isDeleteIds = true;

        } else {
          this.deleteIDs = [];
          this.isDeleteIds = false;
        }
      });
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  createFormControls() {

    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
    ]);
    this.name = new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.pattern(/^[a-zA-Z_ ]{1,}$/)
    ]);
    this.location = new FormControl('', [Validators.required]);
    this.description = new FormControl('', [Validators.required]);
    // this.nodeId = new FormControl('1');

    // this.permissionId = new FormControl(['READ', 'CREATE', 'UPDATE', 'DELETE']);



  }

  createForm() {
    this.orgForm = new FormGroup({
      email: this.email,
      name: this.name,
      location: this.location,
      description: this.description,
      // nodeId: this.nodeId,
      // permissionId: this.permissionId
    });




  }
  get f() { return this.orgForm.controls; }

  getOrgs() {
    this.myOrgService.getPermission().subscribe((data) => {
      this.myTree = data.result.permissionList;
      if (this.myTree.length > 0) {
        this.emptyOrg = false;
        this.isFirst = true;

        if (this.myOrgService.seletedNode) {
          this.seletedNode = this.myOrgService.seletedNode;
          
        }

      } else {
        this.emptyOrg = true;
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    //   console.log('test')
    // stop here if form is invalid
    if (this.orgForm.invalid) {
      return;
    }
    this.myOrgService.createOrg(this.orgForm.value).subscribe((data) => {
      this.submitted = false;
      this.close();
      this.toastr.success('Organisation Created successfully');
      this.getOrgs();
    });
  }

  close() {
    this.orgForm.reset();
    this.modalRef.hide();
    this.submitted = false;
  }

  multiDelete(confirmMultiDelete: TemplateRef<any>) {
    console.log('MultiDelete');
    if (this.deleteIDs.length > 0) {
      this.modalRefUser = this.modalService.show(confirmMultiDelete, { class: 'modal-lg incareview-model incare-window' });
    } else if (this.deleteIDs.length === 0) {
      this.toastr.error('Plaese Select any One User !');
    }

  }


  deleteUser() {


    this.myOrgService.deleteUser({ permissionId: this.deleteIDs }).subscribe((org: any) => {
      this.deleteIDs = [];
      this.isDeleteIds = false;
      if (org.result.status) {
        this.modalRefUser.hide();
        this.toastr.error(org.result.err);
      } else {

        this.modalRefUser.hide();
        this.toastr.success(org.result.message);
        this.myOrgService.changeUserEvent.emit();
      }

    });
  }


  cancelDelete() {
    this.modalRefUser.hide();
    this.deleteIDs = [];
    this.getOrgs();
  }

}
