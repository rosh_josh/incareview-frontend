import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidMediaformComponent } from './covid-mediaform.component';

describe('CovidMediaformComponent', () => {
  let component: CovidMediaformComponent;
  let fixture: ComponentFixture<CovidMediaformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovidMediaformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidMediaformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
