import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { 
  PopoverModule, 
  ModalModule, 
  AccordionModule, 
  BsDropdownModule, 
  TabsModule,
  CarouselModule
 } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguCarouselModule } from '@ngu/carousel';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { NgxEditorModule } from 'ngx-editor';
import { GtagModule } from 'angular-gtag';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ForgotpasswordComponent } from './auth/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './auth/resetpassword/resetpassword.component';
import { LoaderService, JwtInterceptor, AuthenticationService, MyOrgService, VideoProcessingService } from './services/index';
import { AppConfig } from './app.config';
import { ToastrModule } from 'ngx-toastr';
import { MyDatePickerModule } from 'mydatepicker';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { ValidEmailComponent } from './auth/valid-email/valid-email.component';
import { LinkviaComponent } from './auth/linkvia/linkvia.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { SidemenuComponent } from './layout/sidemenu/sidemenu.component';
import { MedicationsComponent } from './medications/medications.component';
import { DosDontsComponent } from './dos-donts/dos-donts.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { MediaComponent } from './media/media.component';
import { ForumsComponent } from './forums/forums.component';
import { ContactComponent } from './contact/contact.component';
import { MyOrgComponent } from './my-org/my-org.component';
import { TreeViewComponent } from './my-org/tree-view.component';
// import { TreeModule } from 'ng2-tree';
import { TreeModule } from 'angular-tree-component';
import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UserComponent } from './manage-users/user.component';
import { UserViewComponent } from './manage-users/user-view.component';
import { NumbersOnlyDirective } from './directive/numbers-only.directive';
import { AuthGuard } from './services/auth-guard';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { PatientsComponent } from './patients/patients.component';
import { CareTeamComponent } from './care-team/care-team.component';
import { NotesComponent } from './notes/notes.component';
import { DrugInfoComponent } from './drug-info/drug-info.component';
import { PatientProfileComponent } from './patient-profile/patient-profile.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { InstructionsComponent } from './instructions/instructions.component';
import { MediaListComponent } from './media-list/media-list.component';
import { MediaFormComponent } from './media-form/media-form.component';
import { MediaSlidesComponent } from './media-slides/media-slides.component';
import { MediaVideofeedsComponent } from './media-videofeeds/media-videofeeds.component';
import { MediaIvideoComponent } from './media-ivideo/media-ivideo.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { SettingsComponent } from './settings/settings.component';
import { BottomnavComponent } from './layout/bottomnav/bottomnav.component';
import { AskYourDoctorComponent } from './ask-your-doctor/ask-your-doctor.component';
import { AddMedicationComponent } from './add-medication/add-medication.component';
import { AddMediaListComponent } from './add-media-list/add-media-list.component';
import { TagManagementComponent } from './tag-management/tag-management.component';
import { TagsComponent } from './tags/tags.component';
import { TextInputAutocompleteModule } from './input-autocomplete';
import { TagInputModule } from 'ngx-chips';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { ManageComponent } from './manage/manage.component';
import { AddCareteamComponent } from './add-careteam/add-careteam.component';
import { AddusersComponent } from './addusers/addusers.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { CeomanagementComponent } from './ceomanagement/ceomanagement.component';
import { AddceoComponent } from './addceo/addceo.component';
import { UnsupportpageComponent } from './unsupportpage/unsupportpage.component';
import { PatientsummaryComponent } from './patientsummary/patientsummary.component';
import { DownloadsummaryComponent } from './downloadsummary/downloadsummary.component';
import { OptloginComponent } from './auth/optlogin/optlogin.component';
import { CovidpatientsComponent } from './covidpatients/covidpatients.component';
import { CovidpatientpageComponent } from './covidpatientpage/covidpatientpage.component';
import { CovidpatientprofileComponent } from './covidpatientprofile/covidpatientprofile.component';
import { CovidtagsComponent } from './covidtags/covidtags.component';
import { CovidtagmanagementComponent } from './covidtagmanagement/covidtagmanagement.component';
import { CovidMediaformComponent } from './covid-mediaform/covid-mediaform.component';
import { CovidAddmedicationComponent } from './covid-addmedication/covid-addmedication.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderInterceptor } from './services/loader.interceptor';
import { UserListComponent } from './users/user-list.component'
import { MediaSlideshowComponent } from './media-slideshow/media-slideshow.component';
import { AssessmentManagementComponent } from './assessmentManagement/assessment-management.component';
import { DetailsComponent } from './assessment/details/details.component';
import { ViewSummaryComponent } from './assessment/view-summary/view-summary.component';
import { AddAssessmentComponent } from './assessment/add-assessment/add-assessment.component';
import { ViewCaseComponent } from './assessment/view-case/view-case.component';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('Google-OAuth-Client-Id')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('Facebook-App-Id')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    ValidEmailComponent,
    LinkviaComponent,
    MedicationsComponent,
    DosDontsComponent,
    AppointmentsComponent,
    MediaComponent,
    ForumsComponent,
    ContactComponent,
    AppLayoutComponent,
    HeaderComponent,
    SidemenuComponent,
    MyOrgComponent,
    TreeViewComponent,
    MobileMenuComponent,
    EditprofileComponent,
    NotificationsComponent,
    UserComponent,
    UserViewComponent,
    NumbersOnlyDirective,
    AddPatientComponent,
    PatientsComponent,
    CareTeamComponent,
    NotesComponent,
    DrugInfoComponent,
    PatientProfileComponent,
    InstructionsComponent,
    MediaListComponent,
    MediaFormComponent,
    MediaSlidesComponent,
    MediaVideofeedsComponent,
    MediaIvideoComponent,
    ChangePasswordComponent,
    SettingsComponent,
    BottomnavComponent,
    AskYourDoctorComponent,
    AddMedicationComponent,
    AddMediaListComponent,
    TagManagementComponent,
    TagsComponent,
    ManageComponent,
    AddCareteamComponent,
    AddusersComponent,
    CreateUserComponent,
    CreateDepartmentComponent,
    CeomanagementComponent,
    AddceoComponent,
    UnsupportpageComponent,
    PatientsummaryComponent,
    DownloadsummaryComponent,
    OptloginComponent,
    CovidpatientsComponent,
    CovidpatientpageComponent,
    CovidpatientprofileComponent,
    CovidtagsComponent,
    CovidtagmanagementComponent,
    CovidMediaformComponent,
    CovidAddmedicationComponent,
    LoaderComponent,
    UserListComponent,
    MediaSlideshowComponent,
    AssessmentManagementComponent,
    DetailsComponent,
    ViewSummaryComponent,
    AddAssessmentComponent,
    ViewCaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    PopoverModule.forRoot(),
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    InfiniteScrollModule,
    ToastrModule.forRoot({ maxOpened: 1, autoDismiss: true }),
    HttpClientModule,
    MyDatePickerModule,
    SocialLoginModule,
    BsDropdownModule.forRoot(),
    TreeModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    NguCarouselModule,
    TextInputAutocompleteModule,
    TagInputModule,
    FilterPipeModule,
    NgxDocViewerModule,
    NgxEditorModule,
    GtagModule.forRoot({ trackingId: 'UA-147574705-1', trackPageviews: true }),
    CarouselModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    VideoProcessingService,
    LoaderService,
    AppConfig,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
